---
layout: holidays
title: "Helsinki"
date: 2008-03-07 10:46:46 +0100
tags: [finland]
permalink: /holidays/kalmar/helsinki/
---
### Zondag 17 februari

![]({% link _holidays/kalmar/IMG_8798.jpg %}){: .reflect}  
De [M/S Gabriella](http://en.wikipedia.org/wiki/M/S%20Gabriella)
{: .center} 

Brak en moe komen we van de boot af. Bij mij zit de deining goed in de
benen en daar heb ik de hele zondag en zelfs een deel van de maandag nog
last van. We gaan naar het hostel om de bagage achter te laten en eerst
maar eens een wandeling te maken door de Finse hoofdstad.

![]({% link _holidays/kalmar/IMG_8699.jpg %}){: .reflect}  
Marlous geeft ons met verve
een rondleiding langs de standbeelden en bijzondere gebouwen van de stad.
{: .center} 

![]({% link _holidays/kalmar/IMG_8725.jpg %}){: .reflect}  
De [Russisch-Orthodoxe Uspenski-kathedraal](http://nl.wikipedia.org/wiki/Uspenski-kathedraal)  
(overigens *zonder* het bekende [kruis met drie balken](http://en.wikipedia.org/wiki/Image:Cross_of_the_Russian_Orthodox_Church_01.svg)!)
{: .center} 

![]({% link _holidays/kalmar/IMG_8741.jpg %}){: .reflect}  
De (Lutherse) [Domkerk van Helsinki](http://nl.wikipedia.org/wiki/Domkerk%20van%20Helsinki) 
{: .center} 

![]({% link _holidays/kalmar/IMG_8742.jpg %}){: .reflect}  
Het [centraal station](http://en.wikipedia.org/wiki/Helsinki_Central_railway_station) 
{: .center} 

![]({% link _holidays/kalmar/IMG_8790.jpg %}){: .reflect}  
Het [Nationaal Museum van Finland](http://en.wikipedia.org/wiki/Finnish_National_Museum) 
{: .center} 

### Maandag 18 februari

Nog steeds een beetje vermoeid gaan we vandaag naar de Finse tegenhanger van
TNO en SINTEF, [Valtion Teknillinen Tutkimuskeskus
(VTT)](http://www.vtt.fi/?lang=en) oftewel in goed Engels: Technical Research
Centre of Finland.

Een aantal presentaties, sommige beter te volgen dan andere en ten slotte een
interessante demo met [Augmented
Reality](http://en.wikipedia.org/wiki/Augmented_Reality) waarbij virtuele
objecten in videobeeld van reële omgevingen worden geprojecteerd. Het was mij
eigenlijk onbekend dat "we" al zo ver zijn in de industrie als wat ik daar zag.

Het probleem bij VTT lijkt de taalbarriere te zijn, want hoewel ze erg
interessante projecten aan het doen zijn, krijgen ze de groep niet enthousiast.
En dat terwijl de meeste studenten van de TU Helsinki het liefst na hun studie
bij VTT gaan werken.

Als we terug zijn in Helsinki besluit ik dat ik even wat rust om me heen nodig
heb en ga in m'n eentje wat door de stad wandelen.

### Dinsdag 19 februari

De dag begint wat later dan normaal, want er is overdag geen programma. Pas om
17u krijgen we een rondleiding door het [Nationaal Museum van
Finland](http://www.nba.fi/en/nmf). Tot die tijd gaan we in kleinere groepjes
door de stad zwerven, op zoek naar souvenirs, leuke fotomomenten en genieten
van het mooie weer. En de metro pakken, omdat het kan, één halte.
