---
layout: holidays
title: "Stockholm (2)"
date: 2008-02-29 21:09:53 +0100
tags: [zweden]
permalink: /holidays/kalmar/stockholm-2/
---
### Donderdag 14 februari

Zo, vandaag gaan we een bezoek brengen aan de technische universiteit van
Stockholm, [Kungliga Tekniska Högskolan](http://www.kth.se/). We krijgen een
uitgebreide rondleiding over de nogal grote campus en omdat de temperatuur
vandaag niet al te hoog is, is het erg fijn om af en toe even een gebouw binnen
te gaan.

![thumbnail]({% link _holidays/kalmar/IMG_8598.jpg %}){: .reflect} ![thumbnail]({% link _holidays/kalmar/IMG_8619.jpg %}){: .reflect}
{: .center}

Helaas is de student die ons rondleid niet echt sociaal vaardig en heeft hij
een nogal laag stemvolume, zodat de uitleg niet echt overkomt. Na de lunch
krijgen we een luchtig college combinatoriek en origami en gaan we aan de
koffie, waar ook direct de speelkaarten op tafel komen om een rikje te leggen.

![right thumbnail]({% link _holidays/kalmar/IMG_8650.jpg %}){: .reflect .left}  IJshockey is de nationale sport van de
Zweden, dus gaan we 's avonds met een deel van de groep naar een
ijshockeywedstrijd. Eigenlijk wilden we wel graag naar het gigantische
bolvormige [Globen Arena](http://nl.wikipedia.org/wiki/Globen_Arena), maar daar
blijkt de musical Mamma Mia te worden opgevoerd en ernaast, in Hovet wordt
daadwerkelijk ijshockey gespeeld.

De kaarten zetten ons in het vak van [Djurgårdens
Ishockeyförening](http://en.wikipedia.org/wiki/Djurg%E5rdens_IF), oftewel DIF,
die voor de wedstrijd op plaats 9 staat en tegen de nummer 4,
[MODO](http://en.wikipedia.org/wiki/MODO) uit Örnsköldsvik. Dat classementen
niet altijd iets zeggen liet deze wedstrijd wel zien, want na tweeënhalf uur
wint DIF met 3 - 0 van MODO.

### Vrijdag 15 februari

Het formele deel van het bezoek aan Stockholm is afgesloten, vandaag is het
tijd voor wat ontspanning in het [Skansen park](http://www.skansen.se/), een
openluchtmuseum annex dierentuin. Omdat het ook vandaag weer ijskoud is en het
zelfs een beetje gesneeuwd heeft, is de zon erg welkom om een beetje warmte te
geven.

![thumbnail]({% link _holidays/kalmar/IMG_8632.jpg %}){: .reflect} ![thumbnail]({% link _holidays/kalmar/IMG_8655.jpg %}){: .reflect}
{: .center}

Een afstammeling van een IJslandpaard staat een beetje lam tegen een boom te
leunen. We vermoeden dat het een zware nacht was... En we zagen nog een eland,
wat je natuurlijk gezien moet hebben als je in Zweden was, maar die je in de
stad natuurlijk normaalgesproken niet ziet.

### Zaterdag 16 februari

We slapen uit tot maarliefst 9 uur, gaan nog de stad in voor last-minute
souvenirs om rond 4 uur te gaan inschepen op de veerboot naar Helsinki.

![thumbnail]({% link _holidays/kalmar/IMG_8676.jpg %}){: .reflect .left}

Aan boord gaan we eerst de buitendekken verkennen, de café's aan boord, de
discotheek en om 6 uur is het dan tijd voor het dinerbuffet.  Een enorme keuze
aan allemaal even lekkere dingen, lokale specialiteiten, maar ook oosters en
mediterraan.

Wat hierna aan boord gebeurt, zal wijselijk onbeschreven blijven :P

![Vlag van Åland](https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Flag_of_Åland.svg/200px-Flag_of_Åland.svg.png){: .reflect .right}
Ik wil wel zeggen dat we onderweg rond middernacht de
[Ålandseilanden](http://nl.wikipedia.org/wiki/Ålandseilanden) aandoen, een
apart land in de Botnische golf tussen Zweden en Finland. Het is een speciale
provincie van Finland met zelfbestuur. De vlag lijkt op die van IJsland en
Zweden. We konden niet van boord, maar ik ben er dus een soort van wel geweest
;)

De volgende morgen komen we aan in Helsinki, waar ik net op tijd aan het
ontbijt zit om te zien hoe we de Suomenlinna eilandengroep doorvaren en
Helsinki voor ons uit de mist opdoemt. Een mooi en bijzonder gezicht.
