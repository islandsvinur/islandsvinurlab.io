---
layout: holidays
title: "Packed and ready to go..."
date: 2005-07-23 10:26:15 +0200
permalink: /holidays/leukerbad/packed/
---

Everything is packed. I went to Van Piere yesterday for a quick holiday
literature update and
got [Gödel, Escher, Bach: An Eternal Golden Braid,](http://en.wikipedia.org/wiki/Gödel,_Escher,_Bach)
supposedly a book one should have read if you're interested in the interaction
between logic and creativity (which I think every scientist should be). Of
course, The Little Book of Bathroom Philosofy
and [Murphy's Law,](http://en.wikipedia.org/wiki/Murphy's_law) both of which I
got for my birthday are also packed.

Tomorrow, early in the morning, my parents will pick me up to first go to a
hotel
in [Freiburg im Breisgau](https://en.wikipedia.org/wiki/Freiburg_im_Breisgau)
and then arrive in Leukerbad on Monday.
