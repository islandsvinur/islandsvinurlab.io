---
layout: holidays
title: "Day 5. Bad weather day"
date: 2005-07-30 11:17:06 +0200
permalink: /holidays/leukerbad/day-5-bad-weather/
---

The day started very nice, with sun and 25 °C on the balcony... But by 11
o'clock, the weather changed dramatically. The temperature sank to a mere 15 °C
and rain came over the valley.

Since we already knew this would happen from the weather forecast, we hadn't
planned anything for the day. This was a nice day for
watching [The 10th Kingdom](http://www.imdb.com/title/tt0207275/).
