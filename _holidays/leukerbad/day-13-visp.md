---
layout: holidays
title: "Day 13. Visp"
date: 2005-08-06 17:45:28 +0200
permalink: /holidays/leukerbad/day-13-visp/
---

Nothing special today, a trip to the
town [Visp](http://maps.google.com/maps?ll=46.293816,7.884922&#38;spn=0.051833,0.069064&#38;t=k&#38;hl=en), 
kind of shopping tour, but all shops close at 5. Went to Carrefour afterwards,
also closing at 5 and since we left home at around 2, we didn't really have time
to do anything (which suits me, I hate shopping).

Holiday's end is in sight, we're slowly getting everything ready for the way
back, tidying, packing, getting some specialities of the region for at home... I
don't know yet what we're doing tomorrow, but I suppose there still is a lot to
pack and move around. Monday morning at nine we are supposed to leave the
apartment.

It's sad to leave, but it is also good to return home, where I can finally post
these sixteen notes, get the photos, read e-mail (ugh, that's going to be a lot), 
read feeds (luckily, they are time-limited), listen new podcasts, update the
site to a new layout and... *aaaargh!* prepare for exams. I took all stuff with
me, but I didn't really have the time to read it.
