---
layout: holidays
title: "Day 2. Lac Souterrain & Sion"
date: 2005-07-26 16:59:53 +0200
permalink: /holidays/leukerbad/day-2-sion/
---

No Zermatt today because the view isn't very great, so we went to
the [subterranian lake in St. Léonard](http://www.lac-souterrain.com) and a
visit to the city of [Sion](http://www.sion.ch/).

*I was a bit stupid and somehow the original long post got overwritten, so this
is a short version.*

The lake is basically a cave with (originally) water until the ceiling. After an
earthquake about fifty years ago, cracks in the cave let half of the water out
and now there is a 1.5 km long, 25 m wide, 13 m high and 10 m deep underground
lake. Inside, it is 13 °C, the water being 9 °C. Quite a contrast to the 30 °C
outside!

![Looking down on the Rhône valley]({% link /image/holidays/leukerbad/looking-down-rhone-valley.jpg %}){: .reflect .width-75}
{: .center}

After the cave visit, we went to the near Sion in the valley. This is a
nice little city, built down from the hills to the Rhône. On top of the hills,
there are two chateaux, one is in ruins, the other is currently in restauration.
In the city center, the temperature differences between the valley and the
mountains is very well noticeable. I think I like the mountains more :-)
