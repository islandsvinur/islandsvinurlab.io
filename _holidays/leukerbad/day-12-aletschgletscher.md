---
layout: holidays
title: "Day 12. Aletschgletscher"
date: 2005-08-05 20:22:24 +0200
permalink: /holidays/leukerbad/day-12-aletschgletscher/
---

Ooff, this day was a bit exhaustive... We went up to see
the [Aletschgletscher](http://maps.google.com/maps?ll=46.441169,8.034782&#38;spn=.206774,.276255&#38;t=k&#38;hl=en)
, the largest in the Alps. A river of one single piece of ice of 23 kilometers
long, 2 km wide and the beginning of the Rhône river (now I have been at the
begin and the end).

The small town Mörel facilitates in three (!) cable cars up to the mountains,
one with two large carriages, and two with smaller 4-person cabines. You first
go to the town [Riederalp](http://www.riederalp.ch/) on 1900 m, with a station
half way. This little town is mainly a ski resort, with cafés, discos,
apres-skis, etcetera.

Then a one quarter walk to the next one which brings you further to 2333 m. From
there, you have a beautiful view on the gletscher, which is about a kilometer
beneath you.

On first impression, it may not appear that great, but if you realise that you
are really very far away from the ice, the enourmous measures of how big it
really is become clear. Actually reaching the ice is impossible if you're not
arriving there by 9 in the morning, so we didn't have the oppurtunity to walk on
the gletscher.

![Mountain lake]({% link /image/holidays/leukerbad/mountain-lake.jpg %}){: .left .width-50} So we took a walk on
the high road, forgetting time and place, being on another Lord of the Rings
set, coming along a beautiful little mountain lake. And then, when we wanted to
go back up to the cabin car... We noticed that they had stopped exactly at 5...
Great, we had missed the last descent. So, that meant a walk down to Riederalp.
40 minutes of steep downhill walking, that is *not* good for your knees.

Finally back in Riederalp, we were lucky to find out that the descent down to
Mörel is in service until 21:45, so we were in time. Otherwise we would have
another even longer walk down ahead of us.
