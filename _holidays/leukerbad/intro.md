---
layout: holidays
title: "Going to Leukerbad, Switzerland"
date: 2005-07-18 12:27:30 +0200
permalink: /holidays/leukerbad/intro/
---

This wasn't planned, but my parents rented an appartment in the Swiss village
of [Leukerbad](https://www.leukerbad.ch/) for two weeks. My cousin works there in
a hotel and many colleagues own an extra appartment to put it on for rent.

I don't know much about the place, only that it is in the
canton [Wallis (Valais)](https://en.wikipedia.org/wiki/Valais)
and at about 1500 meters and also has a hot water source of about 42 °C.

There is
a [satellite picture on Google maps](https://goo.gl/maps/XMCuxzyyDr4fWtx38)
which gives a rough idea where it is situated, but nothing more than that:
Details aren't yet available. _(Update 2021: Obviously Google now has very high
resolution imagery around the globe)_

My laptop travels with me, so I'll try to post something throughout the weeks
and if I can get an internet connection I'll even try to put the posts online.
If not, you'll have to read a lot of posts at once :-) We'll leave next Sunday
and will return somewhere around the 5th of August.
