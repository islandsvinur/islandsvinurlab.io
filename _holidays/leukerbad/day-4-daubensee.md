---
layout: holidays
title: "Day 4. Daubensee (Day 3. Dala canyon)"
date: 2005-07-28 17:01:22 +0200
permalink: /holidays/leukerbad/day-4-daubensee/
---

Right, yesterday I didn't write. Not because we didn't do anything, but because
I didn't find the time to do it.

So, yesterday we went on a walk along the Dala canyon. The Dala streams from the
mountain through Leukerbad with some quite spectacular waterfalls. The community
government has created a easily accessible bridge along this canyon, since many
visitors are elder people and it is too pretty not to visit it. Well, we took
some pictures and all, which I cannot show yet because they are all in the good
old 35 mm filing format (Yes, that is analog!). After the walk through the
schlucht, we went back through the forest which gave occasional views on the
town and the gletschers further in the mountains. We took a refreshment at the
Buljes restaurant... And a cheese and ham plate... And another refreshment... It
was really good sitting there in the sun, enjoying the view and the food and
life in general.

Today, the telpher carriage brought us from 1410 m to 2350 m to
the [Gemmi](http://www.gemmi.ch/) mountain, just behind our apartment. Since my
dad has sometimes some fears of heights, he had to fight some vertigo, but
luckily the trip is only a few minutes long, so things went smoothly. Up on the
Gemmi, one has a great view on the southern Swiss Alpes. Weisshorn, Matterhorn
and Dent Blanche were clearly visible, although a grey mist hid them later that
day.

We went on our walk around
the [Daubensee](http://maps.google.com/maps?ll=46.411766,7.619619&#38;spn=0.051721,0.069064&#38;t=k&#38;hl=en),
which is just behind the elevator station at about 2100 m. In the winter, this
is a langlauf stage. In the summer it is a nice walk through green fields, rock,
and all sorts of landscapes that look as if they were straight from the Lord of
the Rings movies.

The way up is nice, you gradually gain height and can get used to it slowly. The
way down however, is a nice confrontation with 500 meters of nothing below you
in the very first second. I must admit that although I have no fear of heights,
it is very uhm... confronting :-)

It's now 6 o'clock, so the sun has set behind the mountain and it's time for the
valley to cool down. It's currently 31 °C on the balcony, while it was only
about 20 °C up on the Gemmi, slightly warmer by the Daubensee. By the time it's
ten o'clock, it will be around 20 °C down here, the nights are relatively cold.
