---
layout: holidays
title: "Day 15. Leaving"
date: 2005-08-08 09:01:25 +0200
permalink: /holidays/leukerbad/day-15-leaving/
---

It's time to leave Leukerbad, everything is packed and we're aiming for a night
in Karlsruhe, Speyer or Koblenz. In Speyer it is a hotel of
the [Lindner](http://www.lindner-hotels.de/) group, the one DK works for, so
maybe we will get a discount ;-).

![Sheep on the Geissweg]({% link /image/holidays/leukerbad/sheep-geissweg.jpg %})
{: .center}

Yesterday we (dad and I) took a short walk on the Geissweg, a path along the
slope of the Gemmi mountain with a great view on the entire town — very unique
actually to see the town in whole. Mom stayed home and we would call her when we
get there so that we could take a picture of our home with her on the balcony :-)
