---
layout: holidays
title: "Day 8. Erster August-feier"
date: 2005-08-01 22:58:03 +0200
permalink: /holidays/leukerbad/day-8-national-holiday/
---

Another uncle joined us today, he is in Switzerland at friends and wanted to
spend 1 August with us, it's becoming a family party!

The Swiss of Leukerbad commemorate the birthday of their homeland with a large
barbecue in the town center, music and fireworks. We did it with a (typically
Swiss) cheese fondue, loads of Swiss wine and Toblerone chocolate. In the
afternoon I had an ice-coupe Suisse with a Rivella... Yes, Hop Schwyz! Hop
Schwyz!

The fireworks were impressive, not so much because of the visual effects (which
were great, some of them I've never seen before), but mainly because of the
enourmous growl which follows after each bang. Remember that Leukerbad lies
between two mountains and every sound is reflected multiple times before dimming
out, resulting in a massive wall of sound.
