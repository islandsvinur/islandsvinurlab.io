---
layout: holidays
title: "Day 16. Home!"
date: 2005-08-09 14:52:41 +0200
permalink: /holidays/leukerbad/day-16-home/
---

Woohoo! We made it! We spent the night in the
luxureous [Lindner Binshof hotel &amp; spa](http://www.lindner.de/de/LHB/index_html/complexdoc_view)
in [Speyer](http://maps.google.com/maps?ll=49.362980,8.451490&#38;spn=0.048855,0.069064&#38;t=k&#38;hl=en)
for a ridiculously low fare -- thank you for having family in the hotel business
;-)

Okay, so now, mail, feeds, uploading this, reading elfjes and pijlen
in [Utopia](http://utopiamoo.net/) and [Medieval](http://medievalmoo.nl/), get
the stuff here together, finding out who Marijke is who gave me money for using
the washing machine, and oh yeah, first a nice shower.
