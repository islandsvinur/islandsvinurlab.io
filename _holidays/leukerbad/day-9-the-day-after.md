---
layout: holidays
title: "Day 9. The day after"
date: 2005-08-02 15:08:01 +0200
permalink: /holidays/leukerbad/day-9-the-day-after/
---

It's the day after the day before and it seems that all the fireworks have
created holes in the clouds, because it is raining cats and dogs. Some people
have an undeclarable headache...

![Daube in the clouds]({% link /image/holidays/leukerbad/daube-in-clouds.jpg %}){: .reflect .width-75}
{: .center}

We're currently surrounded by clouds, the world ends in about 500 meters...
