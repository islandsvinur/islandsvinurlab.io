---
layout: holidays
title: "The Confederation Cross"
date: 2005-07-31 20:50:21 +0200
permalink: /holidays/leukerbad/confederation-cross/
---

![Confederation Cross](http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/100px-Flag_of_Switzerland.svg.png){: .left}
The [Swiss Confederation](https://en.wikipedia.org/wiki/Switzerland) Cross flag
looks great... It's symmetrical in every way, horizontal, vertical, diagonal,
point-symmetrical and rotation-symmetrical. Plus it is square and thus looks
good in every "orientation" if there is such a thing with this design. Oh, and
the colors don't hurt your eyes, very important if you're surrounded by them ;-)

![Wallis](https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Flag_of_Canton_of_Valais.svg/100px-Flag_of_Canton_of_Valais.svg.png){: .right}
The flags of the cantons are all squares too. The flag
of [Valais / Wallis](https://en.wikipedia.org/wiki/Valais) is on the left half
red, on the right half white, with tree columns of stars, two of four and one in
the middle of five stars. The left column of stars is white, the right column is
red and the stars in the middle are half red, half white. This most probably
indicates the bilinguality of the canton, with four Francophone cities, four
Deutschsprachige cities and five which are bilingual themselves.
