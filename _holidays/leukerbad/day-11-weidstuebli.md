---
layout: holidays
title: "Day 11. Weidstübli"
date: 2005-08-04 17:42:21 +0200
permalink: /holidays/leukerbad/day-11-weidstuebli/
---

A walk to the [Weidstübli](http://www.weidstuebli.ch/) today for a hot meal. One
hour and a half walking to, and an hour back.

The first new snow fell today above the 3 kilometers, I think I even saw Rudi
already rushing home. Indeed, this first snowfall introduces the winter,
according to D.
