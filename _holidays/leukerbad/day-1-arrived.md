---
layout: holidays
title: "Day 1. Arrival in Leukerbad"
date: 2005-07-25 18:40:15 +0200
permalink: /holidays/leukerbad/day-1-arrived/
---

One day and nine hours after we left Eindhoven, we arrived at the apartment in
Leukerbad.

The journey was quite good, not too much traffic, a few small jams though, but
all in all it went smoothly. Yesterday around 6 we arrived at a hotel in
Freiburg im Breisgau to spend the night. After we left our baggage there, we had
dinner in the city at an Italian restaurant which had a 50% discount on all
pizzas, pastas and antipasti, so that was good :-)

Then we took an after-dinner walk in the city, which has an old town center and
an old university.

22:00. It was time to return to the hotel to get some sleep. At night, a big
thunderstorm rolled over the Rhein-valley, which changed the weather completely
from sunny and warm to rainy and cold over night.

11:30. We left Freiburg and arrived in Basel, Switzerland around half past noon.
First surprise at the first gas station just 100 meters after the state border:
While Switzerland is known to be an expensive place, petrol is actually cheaper
in Switzerland than it is in the Netherlands! One liter of Super Bleifrei 95
costs CHF 1.60 (= € 1.06). Compare that to our price of around € 1.40.

Well, then it is time for the last few stages in our journey, from Basel to
Bern, from Bern to Kandersteg, then from Goppenstein to Leukerbad. The landscape
changes from river valley into steep hills with many smaller and larger tunnels.
After Bern, the first real mountains of the Alps show up. I've never seen the
Alps, so it was quite a disappointment to see (or actually *not* see) them
completely covered in a thick pack of clouds. Thun, Spiez and Frutigen we
passed, and the roads became smaller and smaller until we arrived in Kandersteg
were the road stops altogether.

![]({% link /image/holidays/leukerbad/kandersteg-train.jpg %}){: .left .width-50}
There we got on the "Autoverladezug" through
the [Lötschberg tunnel](http://www.bls.ch/) which transported us in about ten
minutes to the other side of the Berner Oberland to Goppenstein. This train
connection straight through the Lötschberg mountain range shortens the trip from
Bern to Leukerbad about 150 kilometers and thus is about 2.5 hours faster (plus
it is more fun). At half past four, we drove from the tunnel down into the sunny
and warm Rhône valley and another half hour later, we were ascending again,
driving from Leuk (925 m above sea level) in the valley all the way up to
Leukerbad (1410 m).

Just before our arrival in Leukerbad, a dark cloud fell over the mountains and
opened a rain shower with occasional thunderbolts and lightning you won't often
encounter. In this rain, we tried to find the hotel were my cousin works and by
pure coincidence, the same minute we drove by, she called us.

DK (my cousin) got her car and directed us up to our apartment which is very
nicely situated (with view on the mountains and the valley). The weather has
cleared up a bit now, but we won't see the sun anymore today: It already
disappeared behind the mountains.

We will now have dinner and then a nice quiet evening. Tomorrow, DK will have a
day off and if the weather allows it, we will go to Zermatt and the Matterhorn (
4478 m) near the Italian border. About 50 kilometers drive, but only 25
kilometers as the crow flies (which you won't find here, by the way, it is too
high for them).
