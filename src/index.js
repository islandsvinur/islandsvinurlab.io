import SimpleLightbox from './simple-lightbox'
import css from 'simplelightbox/dist/simple-lightbox.min.css'

const images = document.querySelectorAll('img.lightbox');
for (let image of images) {
    const a = document.createElement('a');
    image.replaceWith(a);
    a.appendChild(image);
    a.href = image.src;
    a.className = "lightbox";
    a.title = image.alt;
}

new SimpleLightbox('a.lightbox', { captionSelector: 'self' });
