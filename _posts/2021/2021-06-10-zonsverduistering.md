---
layout: post
title: Zonsverduistering
date: 2021-06-10 12:02:00 +02:00
lead_image: /image/2021-06-10-zonsverduistering-2.jpg
---
Vandaag was
een [gedeeltelijke zonsverduistering zichtbaar](http://hemel.waarnemen.com/zon/eclipsen/zonsverduistering_20210610.html)
boven Nederland. Met twee grote stukken karton, een prikpen en een vergrootglas
hebben we ons eigen zonneobservatorium geknutseld.

![Een groot vel karton met wat gaatjes en een vergrootglas]({% link /image/2021-06-10-zonsverduistering-1.jpg %})
{: .center}

Door in het grote vel een paar gaatjes te prikken zorg je ervoor dat je een
redelijk scherp beeld van de zon kan opvangen. Normaalgesproken is dat beeld
rond net als de zon, maar tijdens een zonsverduistering neemt de maan een hapje
uit de lichtgevende schijf en wordt het beeld een zonnesikkel!

Een vergrootglas maakt het beeld nog nét iets groter waardoor je echt goed kan
zien dat de zon een stukje mist.

![Zonnesikkels]({% link /image/2021-06-10-zonsverduistering-2.jpg %})
{: .center}
