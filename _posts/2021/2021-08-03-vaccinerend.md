---
title: Vaccinerend Vasovagaal...
layout: post
lead_image: /image/unsplash/zq-lee-aphQNheQXsc-unsplash.jpg
lead_image_credit:
    unsplash:
        name: ZQ Lee
        username: zqlee
---

Ik val flauw van naalden. Altijd al gehad. En die naald hoeft er niet eens bij
mij in te gaan; ik kan ook flauwvallen als er bij iemand anders een infuus wordt
gelegd of een buisje bloed wordt afgenomen. Bloed zelf heb ik geen problemen
mee, het zijn puur de naalden.

Maar het zijn niet alleen naalden. De diepe neus-swab van de coronatest was ook
voldoende om mij het bewustzijn te laten verliezen. Of ik ga van m'n stokje van
een simpel vingerprikje voor
een [CRP bloedtestje](https://nl.wikipedia.org/wiki/C-reactief_proteïne) bij de
huisarts. Allebei erg onhandig natuurlijk...

Het is me nog niet helemaal duidelijk wat het bij mij oorspronkelijk heeft
veroorzaakt, maar de DSM-IV omschrijft
de "[bloed-, letsel- en injectiefobie](https://en.wikipedia.org/wiki/Blood-injection-injury_type_phobia)"
wat aardig in de buurt komt van wat ik ervaar. Zo'n 3-4% van de populatie heeft
er last van. Specifiek op mij is
het [vasovagale type](https://nl.wikipedia.org/wiki/Vasovagale_syncope) van
toepassing waarbij de bloeddruk daalt op het moment dat ik *denk* aan wat er
allemaal gebeurt, of dat ik luister naar het verhaal van een ander. Het
resultaat is meestal flauwvallen.

Vanuit deze situatie kan je eigenlijk twee kanten op. Je kan elke naald en
andere medische behandeling uit de weg gaan, of je kan accepteren dat het
probleem bestaat en er vol tegenin gaan.

Ik koos voor de tweede optie. Na die episode met de coronatest was ik
vastberaden mij dit niet meer te laten overkomen. Ik heb inmiddels geleerd aan
te geven dat ik makkelijk flauwval en dat wordt erg gewaardeerd door het medisch
personeel, want dan hoeven zij me niet van de grond op te rapen en in stabiele
zijligging gaan draaien. Nog belangrijker, ze hoeven zich niet direct zorgen te
maken dat er iets ernstigers aan de hand is. Het zorgt er ook voor dat alles er
wat relaxter aan toe gaat. Dat heb ik ook echt wel nodig, want als ik bijkom is
dat meestal redelijk chaotisch en badend in koud zweet.

De eerste prik, op de gezondheidsverklaring ingevuld dat ik flauwval bij
vaccinaties. Ik word naar een apart hokje gebracht met een bed. We praten wat,
de arts bereidt de prik voor en ik installeer mij op het bed. De prik gaat goed,
voel hem amper en alles lijkt dan ook goed te gaan, dus al vrij snel ga ik
rechtop zitten. Dát had ik beter niet kunnen doen, want binnen vijf tellen voel
ik me slap worden en ga ik snel weer liggen. Maar nu is het eigenlijk al te
laat; de hersens zijn aan het malen over wat er zojuist is gebeurd. Doordat ik
lig is het iets makkelijker
de [syncope](https://nl.wikipedia.org/wiki/Syncope_(medisch)) tegen te houden,
maar ik zweet alsnog als een otter.

Afgelopen week kreeg ik de tweede prik; weer een bed, een babbeltje met de
dienstdoend arts, even tot rust komen en terwijl we aan het kletsen zijn meteen,
hup die naald erin. Ik voel hem, ik wéét dat hij erin zit. Dat zou
normaalgesproken genoeg zijn om mij te laten vertrekken naar mijn eigen
psychedelische privé-bioscoop. Niet vandaag, ik voel een heel lichte tinteling
in mijn hoofd, maar dat is alles.

Na een paar minuten durf ik het aan om rechtop te gaan zitten en nog geen minuut
later sta ik alweer op mijn benen. Helemaal super voel ik me dan nog niet, maar
dit is een enorme overwinning. Het kwartiertje in de nazorg kan ik gebruiken om
bij te komen, even nog wat extra suiker erin en dan ben ik weer helemaal de
oude.

Zo'n positieve ervaring geeft me goede hoop dat ik ooit zittend kan worden
behandeld en niet meer zo hoef op te zien tegen een bloed-onderzoek of een
vaccinatie.
