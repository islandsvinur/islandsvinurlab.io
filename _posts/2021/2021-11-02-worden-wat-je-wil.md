---
title: Worden wat je wil
layout: post
---

Ik mag in de klas van Noud komen vertellen over mijn beroep. Deze post is alleen
een verzameling linkjes die ik in de klas wil gebruiken.

* [Presentatie]({% link /files/computerprogrammeur.pdf %})
* [Scratch project](https://scratch.mit.edu/projects/592892586/)
