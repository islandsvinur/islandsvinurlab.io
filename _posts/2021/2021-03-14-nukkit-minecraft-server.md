---
layout: post
title: Een Minecraft server voor thuis
date: 2021-03-14 11:33:52
lead_image: /video/2021-03-14-creeper-love-explosion.png
---

[Minecraft](https://nl.wikipedia.org/wiki/Minecraft) is al jaren een populaire 
game bij jong en oud. Eén van de redenen daarvoor is dat het volledig aan te 
passen is; iets wat ook de Doom en Unreal engines geen windeieren heeft gelegd. 
Het is vrij eenvoudig om zelf een Minecraft-server te draaien en je eigen 
aanpassingen toe te voegen. Omdat ik wel houd van een beetje aanklooien met dit
soort dingen, heb ik dat dus ook gedaan.

## Lokale netwerkserver

Op het hoogste niveau zijn er twee edities van Minecraft; Java Edition en 
Bedrock Edition. Java Edition was de originele Minecraft en draait op alle 
platformen waar Java beschikbaar is (Windows, Mac, Linux). *Dit artikel gaat 
over Bedrock Edition*, de versie die je op Windows, Android, iOS en zo'n beetje 
alle consoles kunt spelen, maar dus niet op je Mac en ook niet in Linux.

Voor Java Edition is er officiële server software van Mojang, maar voor 
Bedrock Edition is deze er (nog) niet. Er is alleen een alpha-versie die sinds 
2018 niet meer is bijgewerkt. Waarschijnlijk wil Mojang voorkomen dat 
mensen hun eigen servers gaan draaien en dan niet meer betalen voor *Minecraft 
Realms*, hun private game hosting service.

Daardoor zijn er echter wel [verschillende server software 
projecten](https://minecraftservers.gamepedia.com/Bedrock_Edition_server_software), 
de meeste zijn Open Source. Het landschap is een beetje onoverzichtelijk door 
alle forks en specialisaties, maar uiteindelijk heb ik voor mijn eigen server 
heb ik voor [Cloudburst Nukkit](https://cloudburstmc.org/) gekozen, omdat deze 
op Java is gebaseerd en een plugin-systeem heeft waarmee je de functionaliteit 
dus kan uitbreiden zonder in de server zelf veranderingen te hoeven aanbrengen.

## Nukkit setup

Om te beginnen, volg eerst de [intallatie-instructies van Cloudburst 
Nukkit](https://cloudburstmc.org/wiki/nukkit-installation/). Nu heb je een lege 
server, waarop je kan inloggen en doen wat je wil. Enige probleem is... er zijn 
geen dieren en monsters. Dus installeer ook meteen de 
[MobPlugin](https://cloudburstmc.org/resources/mobplugin.3/).

Nu lijkt het al heel veel op het "echte" Minecraft. Sommige blokken zijn niet 
beschikbaar, dieren gedragen zich niet helemaal zoals op een officiële server 
en circuits op basis van Redstone werken niet. Desalniettemin heb je een leuke 
omgeving om te minen, farmen en ontdekken.

## Aanpassingen maken: lievere Creeper explosies

De jongens waren erg ongelukkig dat hun huis steeds weer kapot werd gemaakt door
ontploffende Creepers. Dus ging ik op zoek naar een oplossing.

Ik speurde wat rond in de MobPlugin code en vond [de `explode()` method in de 
`Creeper` class](https://github.com/Nukkit-coders/MobPlugin/blob/55129887575d0035d8ea515ba99b12772ea9ab42/src/main/java/nukkitcoders/mobplugin/entities/monster/walking/Creeper.java#L77-L96):

```java
@Override
public void explode() {
    if (this.closed) return;

    EntityExplosionPrimeEvent ev = new EntityExplosionPrimeEvent(this, this.isPowered() ? 6 : 3);
    this.server.getPluginManager().callEvent(ev);

    if (!ev.isCancelled()) {
        Explosion explosion = new Explosion(this, (float) ev.getForce(), this);

        if (ev.isBlockBreaking() && this.level.getGameRules().getBoolean(GameRule.MOB_GRIEFING)) {
            explosion.explodeA();
        }

        explosion.explodeB();
        this.level.addParticle(new HugeExplodeSeedParticle(this));
    }

    this.close();
}
```

Vóórdat een explosie echt wordt getriggerd, wordt er een 
`EntityExplosionPrimeEvent` event gestuurd naar alle plugins. Daarna wordt 
gekeken of het event gecanceld is, dus dat is het haakje waar we onze plugin aan 
gaan ophangen. Als we luisteren naar `EntityExplosionPrimeEvent`, en vervolgens 
het event cancelen, dan ontplot de Creeper niet meer.

```java
@EventHandler(ignoreCancelled = true)
public void EntityExplosionPrimeEvent(EntityExplosionPrimeEvent ev) {
    if (ev.getEntity() instanceof Creeper) {
        ev.setCancelled();
    }
}
```

Dat is wel een beetje saai, want nu verdwijnt de Creeper gewoon *"poef"* in het 
niets! Laten we in plaats van de explosie wat hartjes verspreiden:

```java
@EventHandler(ignoreCancelled = true)
public void EntityExplosionPrimeEvent(EntityExplosionPrimeEvent ev) {
    if (ev.getEntity() instanceof Creeper) {
        ev.setCancelled();
        Entity entity = ev.getEntity();
        Level level = entity.level;
        for (int i = 0; i < 10; i++) {
            level.addParticle(new HeartParticle(entity.add(Utils.rand(-1.0, 1.0), (entity.getHeight() * 0.75F) + Utils.rand(-1.0, 1.0), Utils.rand(-1.0, 1.0))));
        }
    }
}
```

Zo, dat is beter:

{% video /video/2021-03-14-creeper-love-explosion.mp4 /video/2021-03-14-creeper-love-explosion.png %}

## Code

De complete code voor de plugin vind je [op GitHub](https://gitlab.com/islandsvinur/NukkitCustomPlugin).

## Nieuwe serie?

Zou dit een nieuwe serie blogposts kunnen worden? Wie weet, ik wilde eigenlijk 
al wat gaan schrijven toen ik begon met de setup, dus er is wel wat materiaal.
