---
layout: series_post
series: geldingadalsgos
category: ijsland
date: 2021-05-23 20:17:49 +02:00
title: Geldingadalsgos (4)
lead_image: /image/unsplash/toby-elliott-17yY9Lyddrc-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Toby Elliott
        username: tobyelliot
---

De spectaculaire lava-geyser weet van geen ophouden. De vulkaan spuwt 
gloeiendheet gesteente tot wel 300 meter de lucht in. Zo hoog dat het nu zelfs
vanuit de hoofdstad Reykjavík te zien is. Dat levert dramatische beelden op, 
maar voorlopig is de vulkaan nog steeds ver genoeg van bewoond gebied af om een
echt gevaar te zijn.  

{% youtube "https://www.youtube.com/watch?v=ylRfrtk7yZE" %}

[![kaart Geldingadalsgos]({% link /image/iceland/location-geldingadalsgos.png %}){: .left}](https://www.openstreetmap.org/#map=13/63.8743/-22.2754) 
Door de voortdurende lavastroom loopt de vallei langzaam vol en inmiddels is de 
rand aan één kant bijna bereikt.

Zoals hiernaast op de kaart te zien is ligt weg nummer 427 (Suðurstrandarvegur)
op slechts een paar kilometer afstand van de vulkanen in het Fagradal-gebied.
Ingenieurs van Verkís hebben daarom een muur gebouwd die de lava voorlopig tegen
moet houden. Uiteindelijk gaat dat waarschijnlijk ook niet genoeg zijn, maar op
de vraag wat ze daaraan gingen doen reageerden ze koeltjes met "dan beginnen we
gewoon opnieuw". 

"Þetta reddast" in actie!

{% youtube "https://www.youtube.com/watch?v=jCaXTibBMOQ" %}
