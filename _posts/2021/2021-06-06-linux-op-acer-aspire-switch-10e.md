---
layout: post
title: Ubuntu op Acer Aspire Switch 10E
date: 2021-06-06 15:00:26 +02:00
tags: [ubuntu, linux, tablet]
lead_image: /image/ubuntu-op-acer-aspire-switch-10e.jpg
---

Een paar jaar geleden kocht Marly een
knalroze [Acer Aspire Switch 10E](https://web.archive.org/web/20160727225624/https://www.acer.com/ac/nl/NL/content/series/aspireswitch10e)
om mee te nemen naar de lerarenopleiding in Eindhoven. Het ding was niet heel
snel, maar het was klein en niet heel duur. Prima om mailtjes te versturen en
wat aantekeningen te maken, maar verder hoef je er niet veel van te verwachten.

Sinds ze met de opleiding klaar is, ligt dat ding eigenlijk te verstoffen in de
kast. Ik heb ooit nog geprobeerd iets van de Windows-installatie te maken, maar
dat was ook niet te doen. Toen besloot ik dat het volgende project was om hier
Linux op te zetten.

![Knalroze Acer Aspire Switch 10E]({% link /image/ubuntu-op-acer-aspire-switch-10e-knalroze.jpg %})  
_Knalroze is ook écht knalroze_
{: .center}

## Onderweg

Stap 1 is om een backup en recovery strategie te hebben voor het geval het
helemaal mis gaat. Dat is nog niet zo makkelijk, want hoewel er een USB poort in
het toetsenbord zit, is die tijdens het booten niet beschikbaar. De Micro-USB
poort in het scherm is echter een poort die in Client Mode staat en eigenlijk
alleen bedoeld is om de tablet op te laden. Dat betekent dat je die wel aan een
computer kan hangen net als je telefoon, maar er niet zomaar een USB-drive in
kan stoppen en de bestanden kan zien.

Je hebt eerst een [USB On-The-Go](https://en.wikipedia.org/wiki/USB_On-The-Go)
adapter/kabel nodig. Aangezien ik dat niet heel vaak nodig zal gaan hebben, heb
ik zo'n beetje het goedkoopste kabeltje besteld wat ik kon vinden en die werkt
prima.

## Schizofreen platform  

Volgende probleem wat ik tegenkwam was de bootloader zien op te starten. Hoewel
de [Intel Atom Z3735F](https://ark.intel.com/content/www/us/en/ark/products/80274/intel-atom-processor-z3735f-2m-cache-up-to-1-83-ghz.html)
die erin zit 64-bit is, moet de bootloader 32-bit zijn, want de UEFI is wél
32-bit. Ja, dit is een Frankenstein-apparaat, samengesteld uit allerlei
verschillende componenten. Dit bleek uit een vraag op
het [ArchLinux Forum](https://bbs.archlinux.org/viewtopic.php?id=198968) die
over deze specifieke laptop ging.

De images die je van Linux distributies kan downloaden zijn vaak 32 óf 64 bits,
maar geen combinatie van een 32-bits bootloader met 64-bits kernel, dus hier
moet wat geknutseld worden. Ik vond een vraag van iemand met hetzelfde probleem
met een andere tablet PC 
op [Ask Ubuntu](https://askubuntu.com/questions/775498/ubuntu-on-32-bit-uefi-only-based-tablet-pc)
en een [GitHub project](https://github.com/hirotakaster/baytail-bootia32.efi)
dat de benodigde bootloader heeft.

Met [unetbootin](https://unetbootin.org) maakte ik eerst een Ubuntu 20.04 LTS
live USB stick, en vervolgens plaatste ik de `bootia32.efi` die ik had gevonden
in `/EFI/BOOT` op de stick.

Zoals de meeste PCs tegenwoordig zijn de opstartbestanden beschermd door middel
van een proces genaamd "Secure Boot". Als een OS opstart onder Secure Boot
krijgt het toegang tot de beveiligde hardware waarin bijvoorbeeld
decryptie-sleutels worden opgeslagen. Aangezien ik niet van plan ben om te
Netflixen op dit apparaatje, en er volgens mij ook geen andere redenen zijn om
Secure Boot ingeschakeld te hebben, heb ik het uitgezet in de UEFI settings.
Misschien dat ik het op een later moment nog eens probeer mét Secure Boot.
Voorlopig mis ik het echter niet.

De vraag op Ask Ubuntu had twee links naar stappenplannen
om [Ubuntu 14.10](https://askubuntu.com/questions/619872/installing-ubuntu-14-10-64-bit-on-a-windows-8-bay-trail-atom-tablet#619873)
en [Ubuntu 15.10](https://askubuntu.com/questions/727664/how-can-i-install-64-bit-ubuntu-15-10-on-my-uefi-ia32-bay-trail-tablet/727665#727665)
te installeren. Ik heb ze gevolgd, maar blijkbaar gaat ook de ontwikkeling van
Ubuntu verder, want behalve de 32-bit EFI bootloader hoefde ik in 20.04 niets
meer te doen om de boel helemaal tot een login-scherm te laten komen. Het mag
ook eens mee zitten!

![Login scherm staat dwars]({% link /image/ubuntu-op-acer-aspire-switch-10e-dwars.jpg %})  
_Hmm, dat klopt niet helemaal..._
{: .center}

## Richtingsgevoel

Uit
de [verschillende](https://askubuntu.com/questions/1196076/iio-sensor-proxy-not-working-correctly) [topics](https://askubuntu.com/questions/1072645/screen-rotation-on-ubuntu-18-04-lts-lenovo-ideapad-miix-510#1119438)
die ik vind concludeer ik dat Ubuntu heeft herkend dat het is geïnstalleerd op
een systeem met een accelerometer die de oriëntatie van het scherm kan bepalen.
In elk apparaat kan die chip echter op een andere manier geplaatst zijn ten
opzichte van het scherm. Daarom moet je als je systeem niet herkend wordt en de
oriëntatie niet goed is, zelf een transformatiematrix opgeven die de coordinaten
van de sensor omzet naar schermcoordinaten.

De tool die zorgt voor het uitlezen van de sensor data en het doorgeven naar de
rest van het systeem
heet [iio-sensor-proxy](https://gitlab.freedesktop.org/hadess/iio-sensor-proxy/)
. De data voor een heleboel systemen staat in
[`/lib/hwdb.d/60-sensor.hwdb`](https://github.com/systemd/systemd/blob/main/hwdb.d/60-sensor.hwdb)
. Daar staan ook de instructies hoe je je eigen systeem kan toevoegen aan de
database. Die waren voor mij echter niet helemaal duidelijk, want dit heeft me
wat hoofdbrekens gekost.

De sensor data blijkt bij mij niet uit `/dev/iio:device0` te komen, maar
uit `/dev/iio:device1`. Daardoor was de sensor-identificatie voor systemd niet
correct en werd mijn transformatiematrix niet opgepikt. Uiteindelijk vond ik
een [issue op iio-sensor-proxy](https://gitlab.freedesktop.org/hadess/iio-sensor-proxy/-/issues/244)
die het een en ander duidelijk maakte voor mij. De tool `monitor-sensor`
wordt genoemd die je kan gebruiken om te testen of je de juiste matrix hebt
gebruikt. Ook belangrijk was de opmerking dat `iio-sensor-proxy` handmatig moet
worden herstart als je de matrix in de database aanpast.

```
$ cat /sys/class/dmi/id/modalias
dmi:bvnINSYDECorp.:bvrV1.08:bd08/17/2015:br1.8:efr0.0:svnAcer:pnAspireSW3-013:pvrV1.08:rvnAcer:rnGummi:rvrV1.08:cvnAcer:ct10:cvrChassisVersion:
$ cat /sys/`udevadm info -q path -n /dev/iio:device0`/../modalias
acpi:CPLM3218:CPLM3218:
$ cat /sys/`udevadm info -q path -n /dev/iio:device1`/../modalias
acpi:SMO8500:SMO8500:
```

Met deze informatie kwam ik uiteindelijk tot mijn eigen `/etc/udev/hwdb.d/61-sensor-local.hwdb`:

```shell
# SMO8500 / Acer / AspireSW3-013
sensor:modalias:acpi:SMO8500*:dmi:*:svnAcer:pnAspireSW3-013:*
  ACCEL_MOUNT_MATRIX=0, -1, 0; -1, 0, 0; 0, 0, 1
```

Die kan ik vervolgens testen met de volgende commando's:

```
$ systemhwdb-update
$ udevadm trigger -v -p DEVNAME=/dev/iio:device1
$ systemctl restart iio-sensor-proxy
$ monitor-sensor
```

## Gelukt!

Het meeste werkt nu prima. Er zijn een paar probleempjes die nog optreden en
waarvan ik nog niet weet waarom. Soms besluit het touchpad opeens te stoppen met
werken en dan kan ik alleen nog maar het touchscreen gebruiken. Het scherm
loskoppelen en weer terugplaatsen op het toetsenbord wil soms helpen, maar niet
altijd.

Als de tablet is losgekoppeld en je kunt tekst invoeren, dan krijg je een
schermtoetsenbord te zien, behalve in Firefox. Dat is natuurlijk nogal onhandig,
maar heeft niets met deze specifieke hardware te maken. In Firefox (en alleen in
Firefox) kan ik ook niet scrollen door de pagina omhoog te trekken zoals gewend
op mobiele apparaten. Lijkt er dus op dat ze daar nog wat werk te doen hebben 
voor touchscreens.

![Ubuntu op Acer Aspire Switch 10E]({% link /image/ubuntu-op-acer-aspire-switch-10e.jpg %})  
_Voila! Ubuntu 20.04 LTS op een Acer Aspire Switch 10E in tablet mode!_
{: .center}

Deze post is vooral bedoeld als aantekening voor mezelf om het ooit opnieuw te
kunnen installeren, mocht dat nodig zijn. Dat is ook de reden dat ik voor Ubuntu
20.04 LTS heb gekozen; die wordt nog ondersteund tot 2025 en is daarmee
gegarandeerd langer beschikbaar dan de verwachte resterende levensduur van het
apparaat.
