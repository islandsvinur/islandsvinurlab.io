---
layout: series_post
series: geldingadalsgos
category: ijsland
title: Geldingadalsgos (5)
lead_image: /image/unsplash/asa-steinarsdottir-_xmAPHUXXiU-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Ása Steinarsdóttir
        username: asast
---

De [laatste update]({% post_url 2021/2021-05-22-geldingadalsgos-4 %}) was alweer
bijna dan twee maanden geleden. Toen dachten we nog dat we met een muurtje de
lava wel konden tegenhouden. Dat bleek al vrij snel -- zeg maar gerust vrijwel
meteen -- onrealistisch, want lava kruipt waar het niet gaan kan.

Op 22 mei, nog geen week nadat de berg zand was opgeworpen -- en op dezelfde dag
als mijn vorige bericht -- stroomde het gemaakte reservoir over, zó in het
naastgeleden dal op weg naar weg 427. Dat is natuurlijk vervelend, want het is
een belangrijke verbindingsweg in het zuiden van Reykjanes. Als het gaat om een
afsluiting van een paar dagen of weken denk ik dat iedereen er wel mee zou
kunnen leven, maar deze uitbarsting heeft de potentie om meer dan een jaar door
te gaan en gedurende die tijd zou de route onbegaanbaar zijn. Behalve de weg
liggen er bovendien ook nog stroomleidingen en glasvezelverbindingen in het
gebied die beschermd moeten worden.

{% youtube "https://www.youtube.com/watch?v=hG3LtMexwiY" %}

Met alle infrastructurele problemen is het makkelijk om te vergeten om gewoon te
genieten van wat de vulkaan zoal brengt. Het is nog steeds een vuurspuwende
lava-geiser met inmiddels een gigantisch veld eromheen. De heuvel van waaruit je
een mooi uitzicht had op de krater is inmiddels van het pad afgesneden en de
vuurspuwende krater die eerder nog relatief klein was is nu de grootste van de
hele reeks.

Wat ook zichtbaar is, is dat de structuur van de lava is veranderd. In het begin
had het een erg brokkelig en breekbare structuur, nu is het veel vloeibaarder en
compacter.

{% youtube "https://www.youtube.com/watch?v=X5HTHBNdMo0" %}

In de eerste video van deze post staat Valur Grettisson van Reykjavík Grapevine
in een dal bij een groot rotsblok met ergens in de verte de lava-stroom die over
de bergen kwam. In deze video van twee weken later is dat hele dal al volgelopen
en is het rotsblok door de lava verzwolgen.

IJsland staat naast geisers natuurlijk ook bekend om de prachtige watervallen.
Geldingadalsgos moet hebben gedacht dat als het een geiser kan nadoen, het ook
net zo goed een waterval kan zijn, dus dat is het nu geworden; een lava-val.

Er is nog steeds veel onduidelijk over de aard van de vulkaan. Fimmvörðuháls en
Eyjafjallajökull waren na een paar weken klaar; Geldingadalsgos heeft er volgens
de vulkanologen alle schijn van een schildvulkaan te zijn en gaat waarschijnlijk
nog jaren door. IJsland heeft er een toeristische trekpleister bij.

{% youtube "https://www.youtube.com/watch?v=idCV34yFwOQ" %}
