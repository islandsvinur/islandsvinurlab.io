---
layout: series_post
series: geldingadalsgos
category: ijsland
date: 2021-05-11 22:50:43
title: Geldingadalsgos (3)
lead_image: /image/unsplash/jonatan-pie-b9Sn-kymBhY-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Jonatan Pie
        username: r3dmax
---

Geldingadalsgos gaat maar door en door en zowat elke week verandert er weer 
iets.

Waar al sinds het begin op werd geanticipeerd, gebeurde tussen 5 en 10 april,
toen verschillende scheuren en een aantal nieuwe kraters ontstonden. De 
lava-rivier die daaruit voortvloeide liep in het naastgelegen dal genaamd 
Meradalur. 

{% youtube "https://www.youtube.com/watch?v=4vposX4Xkro" %}

De [eerste dag van de zomer](https://nl.wikipedia.org/wiki/Eerste_dag_van_de_zomer) 
is een feestdag in IJsland waarmee het begin van het zomerseizoen wordt gevierd.
IJsland kent traditioneel slechts zomer en winter en "Sumardagurinn fyrsti" valt 
dit jaar op 22 april.

{% youtube "https://www.youtube.com/watch?v=120EFYoS5eo" %}

Als de vulkaan inderdaad meerdere jaren lava blijft geven, zal uiteindelijk het 
hele dal gevuld zijn en overstromen. Dat kan gevaarlijk zijn voor verschillende 
zaken in de buurt, zoals de [Blue Lagoon](https://nl.wikipedia.org/wiki/Blue_Lagoon_(zwembad)),
of de [geothermische centrale in Svartsengi](https://en.wikipedia.org/wiki/Svartsengi_Power_Station).

Ingenieurs zijn daarom aan het onderzoeken of en hoe het mogelijk is een 
lava-stroom om te leiden. 

{% youtube "https://www.youtube.com/watch?v=xE7BmFlsUMg" %}

Rond 3 mei nam de uitbarsting opeens een heel dramatische wending. IJsland staat
natuurlijk bekend om zijn geisers, met 
[Geysir](https://nl.wikipedia.org/wiki/Geysir) en 
[Strokkur](https://nl.wikipedia.org/wiki/Strokkur) als bekendste voorbeelden. Nu 
kunnen ze daar ook een vulkaan-geiser aan toevoegen, want "Krater Nummer 5" 
gooit opeens regelmatig lava honderden meters de lucht in. 

{% youtube "https://www.youtube.com/watch?v=c_vnsLQBVlY" %}

Na bijna twee maanden heeft het lava-veld eindelijk een naam gekregen. Niet de 
meest spannende naam, maar de lava zal voortaan Fagradalshraun heten, naar het 
"mooie dal" Fagradalur waar het allemaal begon.

{% youtube "https://www.youtube.com/watch?v=HsBY9vIMxqs" %}
