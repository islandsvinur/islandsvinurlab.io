---
layout: series_post
series: geldingadalsgos
category: ijsland
date: 2021-05-10 19:50:43
title: Geldingadalsgos (2)
lead_image: /image/unsplash/toby-elliott-7Jr70SSY-II-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Toby Elliott
        username: tobyelliot
---

De [vulkaanuitbarsting in Geldingardalur]({% post_url 2021/2021-03-21-geldingadalsgos %}) 
op Reykjanes in IJsland is nog steeds in volle gang, hoewel na bijna 60 dagen de
activiteit een klein beetje lijkt af te nemen.

Het uitstekende Engelstalige cultuurmagazine [Reykjavík Grapevine](https://www.grapevine.is/) 
was er snel bij om regelmatig verslag te doen van dit natuurgeweld, aangezien 
buitenlandse toeristen dit keer grotendeels zouden thuisblijven.

Op de avond van de uitbarsting waren alle wegen nog afgesloten en kon niemand 
in de buurt van de vulkaan komen, omdat niemand wist hoe gevaarlijk het zou zijn.

{% youtube "https://www.youtube.com/watch?v=jnh0gV37oI8" %}

De volgende ochtend hebben ze een flinke wandeltocht van een paar uur ondernomen
om bij het gebied te komen dat inmiddels was vrijgegeven, omdat de uitbarsting 
relatief rustig bleek te zijn. Onderstaande video stond ook al op de vorige post,
maar ik heb hem opgenomen voor compleetheid.

{% youtube "https://www.youtube.com/watch?v=f3BD8vqYTho" %}

Op 24 maart werd de omvang van de uitbraak langzaam duidelijk en werden bepaalde
gebieden rondom de grote krater afgesloten, met name de heuvelrug die erheen 
liep waar ze in de vorige video nog op stonden. Er was een verhoogde kans dat er
een spontane scheur in die heuvelrug zou ontstaan en dat er dan dus heel snel 
veel lava uit zou kunnen komen.  

{% youtube "https://www.youtube.com/watch?v=GjjL2mCf2j0" %}

Op 26 maart was het kleine mini-kratertje groter gegroeid dan z'n grotere broer. 
Het werd inmiddels ook niet onmogelijk geacht dat de uitbarsting erg lang zou 
gaan duren, misschien jarenlang en wellicht zelfs decennia.

{% youtube "https://www.youtube.com/watch?v=ldN8H5Tz76g" %}

De video van 31 maart was van een avondbezoek, waar het goed zichtbaar is dat 
het gehele lava-veld nog steeds gloeiend heet is, ook al lijkt het overdag alsof
de lava al helemaal is uitgehard. Het is dan ook volledig onmogelijk om het 
lava-veld op te gaan. Je schoenen zullen doorgesneden worden door de scherpe 
randen van het gesteente en vervolgens smelten door de hitte.

{% youtube "https://www.youtube.com/watch?v=M5_sWSG5nKU" %}

Morgen nog meer video's!
