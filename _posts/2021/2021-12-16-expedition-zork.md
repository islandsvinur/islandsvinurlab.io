---
title: Toverland upgrade Expedition Zork
layout: post
tags: [toverland]
lead_image: /image/2021-12-16-expedition-zork/BH_Zork_Birdseye_High-resolution.jpg
---

[Toverland](https://www.toverland.com/) heeft
een [grote upgrade aangekondigd](https://www.toverland.com/zork/gigantische-upgrade-voor-wildwaterbaan-expedition-zork)
van het buitengebied rondom Expedition Zork. Het verhaal wordt gekoppeld aan de
andere attracties in themagebied Wunderwald, waarbij de excentrieke uitvinder
Maximus Müller wordt gepresenteerd als de ontwikkelaar van dit
boomstammen-transportsysteem. Hiermee wil hij de lokale bevolking van het
Wunderwald met eigen ogen laten zien en geruststellen dat het beruchte
moerasmonster Zork *niet* bestaat (leuke wending!). Natuurlijk bestaat Zork wél
echt, maar of dat zo erg is...?

![Expedition Zork upgrade artists impression]({% link image/2021-12-16-expedition-zork/BH_Zork_Birdseye_High-resolution.jpg %}){: .width-75}
{: .center}

Ooit, lang geleden, toen Toverland nog slechts twee hallen was en het enige
buiten de Booster Bike, de Woudracer (nu Maximus' Blitz Bahn) en de splash drop
van (toen nog) de Backstroke waren, was thematisering nog geen groot issue,
zoals je hieronder kan zien.

![Booster Bike vanaf de Backstroke]({% link image/2021-12-16-expedition-zork/Booster_Bike_at_Toverland.jpg %}){: .width-75}
{: .center}

De komst van Troy veranderde daar nog weinig aan, maar toen daar themagebied
Ithaka omheen werd gelegd begonnen die kale loodsen al wat af te steken. De
opening van de sterk gethematiseerde Magische Vallei in 2013 maakte dit nog
erger en nadat Avalon en Port Laguna in 2018 gereed waren, kon die kale vlakte
écht niet meer.

![Magische Vallei]({% link image/2021-12-16-expedition-zork/IMG_2526.jpg %}){: .width-75}  
Waterpartijen in de _Magische Vallei_
{: .center}

![The Flaming Feather in Avalon, Toverland]({% link image/2021-12-16-expedition-zork/IMG_2190.jpg %}){: .width-75}  
Restaurant _the Flaming Feather_ in _Avalon_
{: .center}

Het aankleden van een bestaande attractie levert misschien voor de gemiddelde
bezoeker geen heel andere ervaring op, maar ik denk dat dit heel goed laat zien
aan welke standaard Toverland zichzelf wil gaan houden. Het park heeft namelijk
ook nog uitbreidingsplannen, die weliswaar wat
[vertraging hebben opgelopen](https://www.1limburg.nl/uitbreidingsplannen-toverland-lopen-vertraging-op), 
maar dat moet echt geweldig worden.

Ik ben in ieder geval erg enthousiast over dit nieuws! We komen sinds deze zomer
regelmatig in het park als "Magic Member" en het geeft een heel klein tipje van
de sluier van wat we nog kunnen gaan verwachten in de komende jaren. Het park
heet nu officieel Attractiepark Toverland en de eerste foto hierboven omschrijft
dat prima, maar het is inmiddels zo veel meer geworden; het zou mij niet
verbazen als ze dat voorvoegsel binnenkort laten vallen.
