---
layout: series_index
series_slug: geldingadalsgos
category: ijsland
date: 2021-03-21 09:50:43
title: Geldingadalsgos
lead_image: /image/unsplash/jonatan-pie-g6tqHx0ME1o-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Jonatan Pie
        username: r3dmax
---

Na weken van aardbevingen barstte op 19 maart de vulkaan Fagradalsfjall 
(Fag-ra-dals-fjall) op Reykjanes uit in de vallei Geldingardalur. De uitbarsting 
wordt daarom Geldingardalsgos (Gel-ding-ar-dals-gos) genoemd. De afgelegen 
locatie en relatief rustige uitbarsting zorgt voor weinig gevaar voor mensen en 
mooie beelden.

{% youtube "http://www.youtube.com/v/f3BD8vqYTho" %}

In de aanloop [verheugden IJslandse nieuws-sites](https://grapevine.is/news/2021/03/04/icelands-possible-volcano-titled-thrainsskjaldarhraun-a-news-anchors-nightmare/) zich al op de naam van de
aanstaande uitbarsting en de tongenbrekers die het voor nieuws-lezers op de hele
wereld zou opleveren. Het was toen nog niet helemaal duidelijk waar de lava aan de
oppervlakte zou komen, dus werd ingezet op Sundhnjúkagígaröð (Sund-hnjúka-gí-ga-röð) en
Þráinsskjaldarhraun (Þrá-ins-skjal-dar-hraun).

{% youtube "https://www.youtube.com/v/fDll0rBRhDM" %}

Helaas, IJsland had medelijden en besloot open te barsten in het veel
eenvoudigere Geldingardalur. Gemiste kans, als je het mij vraagt!

Om het toch goed te maken is de uitbarsting heel rustig, en is het relatief
veilig om de plek te bezoeken (als je een wandeling van een paar uur ervoor over
hebt).

Locatie van [Geldingardalur][1].

[1]: https://goo.gl/maps/6jdwaBE4eb95FaxZA
