---
layout: series_post
title: "19 december: Skyrjarmur"
date: 2019-12-19 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Skyrjarmur]({% link /image/iceland/jolasveinarnir/skyr.jpg %})
> {: .right}
> 
> **Skyrschrokker**, de achtste,  
> was een enge bul.  
> Hij beukte op het skyr-vat,  
> totdat het deksel erop brak.
> 
> Toen boog hij zich erover  
> en begon te schrokken,  
> totdat kreunend en zuchtend  
> alles schoon op was.
