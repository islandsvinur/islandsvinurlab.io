---
layout: series_post
title: "18 december: Hurðaskellir"
date: 2019-12-18 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Hurðaskellir]({% link /image/iceland/jolasveinarnir/hurda.jpg %})
> {: .right}
> 
> De zevende was **Deurenknaller**.  
> Zo was behoorlijk duidelijk,  
> dat wanneer in de schemering   
> de mensen een dutje wilden doen,
> 
> hij er niet bijzonder  
> overstuur van was,  
> hoe hard de deur kraakte  
> in de scharnieren.
