---
layout: series_post
title: "17 december: Askasleikir"
date: 2019-12-17 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Askasleikir]({% link /image/iceland/jolasveinarnir/aska.jpg %})
> {: .right}
> 
> Nummer zes, **Schalenlikker**  
> was altijd opgefokt.  
> Vanonder de bedden  
> stak hij zijn lelijke hoofd.
> 
> En wanneer de schalen neergezet werden  
> voor kat en hond,  
> slingerde hij zich erheen om ze te vangen  
> en ze leeg te likken op de grond.
