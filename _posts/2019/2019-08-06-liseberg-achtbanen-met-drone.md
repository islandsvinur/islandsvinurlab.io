---
layout: post
title: Lisebergs achtbanen vanuit een ander perspectief
date: 2019-08-06 20:58:16 +0200
tags: [youtube, video, liseberg, drone]
lead_image: /image/2019-08-06-liseberg-achtbanen-met-drone.jpg
---
Wow, daar is iemand verrekte handig met een drone! Bij 
[Liseberg](https://nl.wikipedia.org/wiki/Liseberg) in Göteborg moeten ze gedacht hebben dat we de 
in-rides nu wel gezien hebben, dus laten we een professionele drone-piloot racen met de twee 
grootste achtbanen van het park.

[Valkyria](https://en.wikipedia.org/wiki/Valkyria_(roller_coaster)), het Zweedse antwoord op 
[Baron 1898](https://nl.wikipedia.org/wiki/Baron_1898) en eveneens van 
[B&M](https://nl.wikipedia.org/wiki/Bolliger_%26_Mabillard):

{% youtube "https://www.youtube.com/watch?v=VBKhtiD8Orw" %}

[Helix](https://en.wikipedia.org/wiki/Helix_(roller_coaster)), de langste achtbaan van Scandinavië. 
De baan is heel mooi geïntegreerd in de heuvel waar Liseberg op ligt en is deels vervlochten met 
nog een derde achtbaan, [Lisebergsbanan](https://nl.wikipedia.org/wiki/Lisebergbanan).

{% youtube "https://www.youtube.com/watch?v=1I61B9FUlp0" %}

De video's werden gemaakt door Viggo Koch, die op zijn 
[YouTube-kanaal](https://www.youtube.com/channel/UCBhzQHQy0Zkey32DPalWTKQ/videos) nog veel meer 
stunts uithaalt met z'n drone.
