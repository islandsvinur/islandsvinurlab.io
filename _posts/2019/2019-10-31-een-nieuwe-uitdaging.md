---
layout: post
title: Een nieuwe uitdaging bij IXON
date: 2019-10-31 22:12:12 +0200
---

Vandaag heb ik na bijna 3,5 jaar afscheid genomen van mijn collega's bij 
[GX Software in Nijmegen](https://gxsoftware.com/), want vanaf morgen start ik 
bij [IXON in Overloon](https://ixon.cloud/)!

![]({% link /image/ixon-logo.png %}) IXON laat machinebouwers verbinden met de 
machines die bij hun klanten opgesteld staan. Hierdoor kunnen die machinebouwers 
op afstand problemen verhelpen (remote service), maar ook de huidige toestand 
bekijken (monitoring) en uiteindelijk kunnen ze zelfs op basis van die 
monitoring voorspellingen doen wanneer bepaalde onderdelen zullen gaan falen 
([predictive maintenance](https://en.wikipedia.org/wiki/Predictive_maintenance)). 

Het is een compleet andere branche dan marketing en communicatie waar ik me 
voorheen vooral in bevond en dat is precies wat me er zo enthousiast over maakt. 
"De industrie" wordt in Nederland vooral als een vies woord gezien, terwijl we 
diezelfde industrie keihard nodig hebben voor onze welvaart. Ik hoop daarom dat 
ik zo mijn bescheiden steentje kan bijdragen aan een efficiëntere industrie, 
zodat zij zich kunnen gaan bezighouden met de verduurzaming ervan.

Ik heb er heel veel zin in en ga weer een hoop mooie dingen maken!
