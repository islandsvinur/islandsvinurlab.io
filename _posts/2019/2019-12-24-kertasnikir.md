---
layout: series_post
title: "24 december: Kertasníkir"
date: 2019-12-24 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Kertasníkir]({% link /image/iceland/jolasveinarnir/kerta.jpg %})
> {: .right}
> 
> De dertiende was **Kaarsenbietser**,  
> \- het was koud,  
> als hij niet de laatste was  
> op kerstavond.
> 
> Hij achtervolgde de kleintjes  
> lachend zo blij en fijntjes,  
> terwijl ze door de tuin  
> dartelden met hun kaarsjes.
> 
> In de kerstnacht zelf,  
> \- zo zegt een wijs man, -  
> hielden de jongens zich in  
> en staarden ze naar de lichtjes.
>
> Daarna, een voor een, vertrokken ze weer,  
> dwars door vrieskou en sneeuw.  
> Met Driekoningen[^dertiende] ging  
> de laatste van het stel. 
>
> Al lang geleden heeft de sneeuw  
> hun voetafdrukken laten vervagen.  
> Maar uit de herinneringen  
> ontstaan beelden en liederen.

Fijne kerstdagen en zorg ervoor dat je nieuwe kleren krijgt vóór het eind van Kerstavond, want anders komt 
[**Jólakötturinn**](https://en.wikipedia.org/wiki/Icelandic_Christmas_folklore#The_Yule_Cat) de Kerstkat je opeten!

[^dertiende]: Driekoningen heet Þrettándin - "de Dertiende" - in het IJslands.