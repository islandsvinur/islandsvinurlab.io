---
layout: series_index
title: GOTO Amsterdam 2019
date: 2019-06-18 21:03:16 +0200
series_slug: goto-amsterdam-2019
---
Deze week ben ik op de [GOTO Amsterdam](https://gotoams.nl/2019) conferentie.
Een conferentie over softwareontwikkeling in de breedste zin van het woord. Ik
ben dol op deze conferentie, omdat dit nu juist *niet* gaat over welke "stack"
je dit jaar moet gebruiken, of wat de leuke nieuwe features van het hipste
platform van het moment zijn. 

Daarnaast wordt het gehouden in de Beurs van Berlage, wat op zichzelf al een
goede reden is om te gaan.

Ik ben het verslag nog aan het uitwerken, dus deze pagina zal nog wel een paar
keer worden bijgewerkt voordat ik er helemaal tevreden over ben.

