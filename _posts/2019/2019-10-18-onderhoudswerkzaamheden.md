---
layout: post
title: Onderhoudswerkzaamheden
date: 2019-10-18 22:45:34 +0200
---
{% raw %}De afgelopen weken ben ik bezig geweest om alle posts in dit blog 
volledig te converteren naar Markdown en alle links om te zetten naar de Jekyll 
[`{% link %}`](https://jekyllrb.com/docs/liquid/tags/#link) en 
[`{% post_url %}`](https://jekyllrb.com/docs/liquid/tags/#linking-to-posts) 
tags, waar relevant. Ik wil namelijk heel graag dit blog wat nieuw leven 
inblazen, maar daarvoor moet de keuken natuurlijk wel een beetje opgeruimd 
zijn.{% endraw %}

In 2015 heb ik [de hele blog geconverteerd naar Jekyll]({% post_url 2015/2015-04-07-migrated-to-jekyll %}) 
vanuit Hobix. Omdat [Jekyll](https://jekyllrb.com) prima kan omgaan met 
[Textile](https://textile-lang.com) content -- het standaardformaat in Hobix -- heb ik 
toen niet de de moeite genomen om de hele boel om te zetten naar Markdown, maar 
dat leverde wel een hoop brakke pagina's op met veel kapotte links en plaatjes.

Midden 2016 ben ik van [GitHub Pages](https://pages.github.com/) naar 
[GitLab Pages](https://about.gitlab.com/product/pages/) geswitched, waarmee ik 
de volledige vrijheid kreeg om mijn Jekyll in te richten naar mijn eigen smaak, 
in plaats van te gebruiken wat GitHub aanbiedt.

Het project is [publiekelijk beschikbaar](https://gitlab.com/islandsvinur/islandsvinur.gitlab.io), 
dus neem vooral eens een kijkje als je geïnteresseerd bent in hoe het allemaal 
achter de schermen werkt.
