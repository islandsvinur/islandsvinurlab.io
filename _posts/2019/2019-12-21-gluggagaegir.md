---
layout: series_post
title: "21 december: Gluggagægir"
date: 2019-12-21 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Gluggagægir]({% link /image/iceland/jolasveinarnir/glugga.jpg %})
> {: .right}
> 
> De tiende was **Glazengluurder**,  
> een grijze gluiperd,  
> die naar de ramen sloop  
> en naar binnen loert.
> 
> Als er binnen iets was  
> wat hem beviel,  
> kwam hij meestal later  
> terug om het te halen.
