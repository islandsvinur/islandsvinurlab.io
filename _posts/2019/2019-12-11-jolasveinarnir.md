---
layout: series_index
title: "Jólasveinarnir: de 13 kerstmannen van IJsland"
date: 2019-12-11 05:00:00 +0200
series_slug: jolasveinarnir
category: [ijsland]
---
> Luister naar mijn verhaal  
> van een stel kwajongens,  
> die ons vroeger bezochten  
> op onze boerderijen thuis.
> 
> Ze kwamen uit de bergen,  
> zoals jullie vast wel weten.  
> in een lange stoet   
> naar de erven in het dal beneden.
> 
> **Grýla** was hun moeder  
> zij gaf hen trollenmelk.  
> Hun vader was **Leppalúði**,   
> Een werkelijk walgelijk stel.
> 
> Ze werden de **Joeltijdjongens** genoemd,  
> met het [joelfeest](https://nl.wikipedia.org/wiki/Joelfeest) kwamen ze.  
> Altijd een voor een,  
> nooit twee of drie.
> 
> Met dertienen waren ze,  
> Deze kleine heren.  
> Ze wilden niet komen vervelen,  
> Althans, niet tegelijk.
> 
> Langs de deuren sluipen ze,  
> halen ze stilletjes van het slot.  
> Daarna hebben ze maar een doel,  
> in de keuken of voorraadkast.
> 
> Met een sluwe blik  
> verstoppen ze zich hier en daar,  
> voor onbekende blikken,  
> en of er niemand in de buurt was.
> 
> En zelfs als iemand ze zag,  
> aarzelden ze niet  
> om mensen te foppen - het verstoren  
> van de vrede in huis.

© Jóhannes úr Kötlum, vertaling © Christian Luijten
{: .small}

Met deze woorden begint het gedicht "Jólasveinarnir" van *Jóhannes úr Kötlum* (Johannes uit Katla) uit 1932. Hebben 
Sinterklaas en Coca-Cola ervoor gezorgd dat zowat over de hele wereld Kerst wordt gevierd met de Kerstman; in IJsland 
hebben ze wel *dertien* kerstmannen! Elke dag in de aanloop naar Kerst komt er één uit de bergen naar de mensen en in 
tegenstelling tot onze kindervriend zijn het allesbehalve aangename kerels.

Het gedicht gaat nog verder met een omschrijving van elk van de "joeltijdjongens". De komende dagen volgt daarom **tot 
aan Kerst elke dag een post** telkens over een van deze merkwaardige figuren, terwijl ze ons komen lastigvallen!
