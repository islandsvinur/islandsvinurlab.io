---
layout: series_post
title: "23 december: Ketkrókur"
date: 2019-12-23 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Ketkrókur]({% link /image/iceland/jolasveinarnir/kjet.jpg %})
> {: .right}
> 
> **Vleeshaak**, zo heette de twaalfde,  
> had vele talenten.   
> Hij stampte door het veld  
> op [Thorlaksmis](https://en.wikipedia.org/wiki/Thorlac%27s_mass).
> 
> Hij gapte vaak wat vlees,   
> maakte niet uit wat.  
> Maar soms was zijn haak  
> nét iets te kort.  
