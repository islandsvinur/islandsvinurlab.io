---
layout: series_post
title: "22 december: Gáttaþefur"
date: 2019-12-22 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Gáttaþefur]({% link /image/iceland/jolasveinarnir/gatta.jpg %})
> {: .right}
> 
> De elfde was **Deursnuffelaar**  
> \- nooit was hij verkouden,  
> ook al had hij zo'n gevoelige  
> en lachwekkend grote neus.
>
> Hij ving al van verre  
> de geur op van [bladbrood](https://en.wikipedia.org/wiki/Laufabrauð),  
> en licht als rook,  
> rende hij erheen.
