---
layout: series_post
title: "GOTO Amsterdam 2019: dag 3"
date: 2019-06-20 21:56:16 +0200
series: goto-amsterdam-2019
---
Dag drie van GOTO is nieuw. Voor het eerst is de conferentie driedaags en dat is even wennen, want dat betekent ook een stuk meer te verwerken!

## Keynote: AI/ML, Quantum Computing and 5G – Opportunities, Challenges and the Impact on Society

Marco Gercke

[[Meer info](https://gotoams.nl/2019/sessions/1035)] | [[Video](https://www.youtube.com/watch?v=TgHh6I3GMvM&feature=youtu.be&list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## Building Evolutionary Infrastructure

Kief Morris

[[Meer info](https://gotoams.nl/2019/sessions/759)]

## Build Agility with Design Sprints

Gary Crawford

[[Meer info](https://gotoams.nl/2019/sessions/754)] | [[Video](https://youtu.be/23OmHRXAuzU)]

## The Happy Movement: How Business Is The Key To Making The World A Happier Place

Evan Sutter

[[Meer info](https://gotoams.nl/2019/sessions/772)] | [[Video](https://youtu.be/Z_e5hwZAeIM)]

## Life After Java 8

Trisha Gee

[[Meer info](https://gotoams.nl/2019/sessions/771)] | [[Video](https://youtu.be/eBuFzQeiGe0?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## It's Getting Faster

Liz Keogh

[[Meer info](https://gotoams.nl/2019/sessions/747)] | [[Video](https://youtu.be/9Cy3Dn3C8jk?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]
