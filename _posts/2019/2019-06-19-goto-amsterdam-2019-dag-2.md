---
layout: series_post
title: "GOTO Amsterdam 2019: dag 2"
date: 2019-06-19 22:12:16 +0200
series: goto-amsterdam-2019
lead_image: /image/2019-06-19_pingpong-goto.jpg
---
Op dag twee van GOTO zien we vliegende auto's, vliegensvlugge acceptatietests, testen met objecten, kwantumcomputers, monitoring, buzzwords bingo bij "Cloud Native" "Developer Experience", sneller opleveren en we sluiten afgelegen dorpen in India aan op hun eigen electriciteitsnetwerk.

## Keynote: From Flying Cars to Humans on Mars - The Future of Transportation

Dr. Anita Sengupta is wat je noemt een "rocket scientist". Ze was de hoofdingenieur van het team dat het parachutesysteem van het [Mars Science Laboratory](https://nl.wikipedia.org/wiki/Mars_Science_Laboratory) -- beter bekend als Curiosity -- veilig op Mars deed landen. Na haar leven bij NASA is ze in de private sector begonnen aan het ontwikkelen van nieuwe transportmethodes, waaronder de implementatie van een [hyperloop](https://nl.wikipedia.org/wiki/Hyperloop).

Naast haar werk als ingenieur is ze ook piloot in de algemene luchtvaart. Het nieuwste concept valt dan ook in de categorie "vliegende auto's", maar dan niet zoals we kennen uit de Jetsons. Het idee is een vierpersoons vliegtuigje wat verticaal kan opstijgen en landen als een helicopter, maar ook kan starten en landen op een normale landingsbaan. Hierdoor kan het gebruikt worden om vanuit kleinere vliegveldjes buiten een stad in een directe vlucht bovenop een gebouw in de door verkeersopstoppingen geplaagde stad. Het toestel is elektrisch aangedreven, aangezien fossiele brandstoffen echt niet meer passen in een nieuw vervoersconcept.

Het doel is om de vliegtuigen zo goedkoop te maken dat ze in dezelfde prijsklasse als taxi's kunnen worden ingezet. Klinkt leuk, maar volgens mij zorgt dat er gewoon voor dat de drukte zich verplaatst van de grond naar de lucht. Natuurlijk is er in de derde dimensie een stuk meer ruimte, maar ik krijg toch het idee dat de oplossing niet méér vervoer is maar juist minder.

Niettemin een technisch interessant onderwerp waar we in de toekomst zeker meer van gaan horen.

[[Meer info](https://gotoams.nl/2019/sessions/756)] | [[Video](https://www.youtube.com/watch?v=UzwE-TbcIj4)]

## Millisecond Full Stack Acceptance Tests

Aslak Hellesøy begon in 2013 met [Cucumber](https://cucumber.io/), een combinatie van specificeren, documenteren en testen op basis van een enkel bronbestand. Het probleem van specificatie is normaalgesproken dat software engineers, business analysten en testers niet dezelfde taal spreken en elkaar daardoor niet begrijpen. Het resultaat is dat de verkeerde software wordt opgeleverd met niet-gevonden fouten. Het verenigen van de talen van de drie rollen en het voorzien in de mogelijkheid om die taal te gebruiken voor specicatie, documentatie en tests is de kern van Cucumber.

Over de jaren heen is Cucumber [vele malen verkeerd begrepen](https://cucumber.io/blog/the-worlds-most-misunderstood-collaboration-tool/). Steeds kwam dat voort omdat slechts één van de drie rollen volledig investeerde en de anderen de geschreven tekst volgden. Het belangrijke aspect van samenwerking werd vaak vergeten.

Een van de misverstanden is dat een Cucumber-bestand de volledige stack moet omschrijven om nuttig te zijn. Alleen, als je de bestanden dan gebruikt om te testen krijg je lange, onstabiele tests, die ook nog eens veel herhaling bevatten. Door bewust delen van het systeem te vervangen door test-implementaties kan een test veel stabieler én sneller worden. Stel je applicatie voor als een stapel blokken met elk zijn eigen verbindingen. Als je schijf- en netwerktoegang weet te beperken, zullen je tests veel sneller worden.

![Zekerheid versus Tijd]({% link /image/2019-06-19-goto-amsterdam-2019-dag-2-aslak.png %})
{: .center}

Uiteindelijk is *zekerheid* het enige doel van testen. De zekerheid dat wat je live zet op vrijdagmiddag het hele weekend goed zal werken. De zekerheid dat de facturen op de juiste manier worden opgemaakt. De zekerheid dat het systeem werkt op macOS én Windows. Elk van voorgaande voorbeelden kan worden bereikt met een end-to-end-test, maar het is de vraag of je daar meer zekerheid mee bereikt dan met een meer specifieke test.

Dezelfde set Cucumber-bestanden kan een volledige stack testen, of slechts twee componenten, of zelfs allebei. Dat is alleen afhankelijk van of de stappen in de bestanden een betekenisvolle invulling hebben voor het gewenste zekerheidsniveau.

[[Meer info](https://gotoams.nl/2019/sessions/813)] | [[Video](https://www.youtube.com/watch?v=sUclXYMDI94&feature=youtu.be&list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## Selenium Tests, the Object Oriented Way

Corina Pip presenteert een manier om je Selenium tests meer object-georienteerd op te zetten en zo de asserties leesbaarder en herbruikbaar te maken. Ik vond het zelf een iets minder geslaagde presentatie, omdat er veel herhaling in zat en de concepten van OO wel erg diep werden geïntroduceerd. Software engineers met een informatica-achtergrond waren duidelijk niet de doelgroep; die hadden het met een introductie van 5 minuten ook begrepen en het kunnen repliceren.

[[Meer info](https://gotoams.nl/2019/sessions/762)]

## Keynote: The Grand Challenge and Promise of Quantum Computing

Lieven Vandersypen geeft een bloemlezing langs de huidige stand van zaken rondom kwantumcomputers en de uitdagingen die nog komen.

[[Meer info](https://gotoams.nl/2019/sessions/776)] | [[Video](https://youtu.be/fyndSsMxzEc?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## An Introduction to Systems and Service Monitoring with Prometheus

Julius Volz is een van de oprichtes van Prometheus, een monitoring-oplossing. 

[[Meer info](https://gotoams.nl/2019/sessions/844)] | [[Video](https://youtu.be/5O1djJ13gRU?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## In Search of the Perfect Cloud Native Developer Experience

Daniel Bryant

[[Meer info](https://gotoams.nl/2019/sessions/786)]

## Continuous Delivery and the Theory of Constraints

Steve Smith

[[Meer info](https://gotoams.nl/2019/sessions/758)]

## Keynote: Energy and Education Access for Remote Communities

Jaideep Bansal

[[Meer info](https://gotoams.nl/2019/sessions/757)] | [[Video](https://youtu.be/AwS42muvKQ0)]

## GOTO Gathering

De GOTO Gathering is het traditionele social event. Vorig jaar in een café onder een brug aan het IJ met prachtig zicht op de ondergaande zon; dit jaar in de grote zaal van de Beurs van Berlage. Voor eten en drinken is gezorgd, en ook aan gekke games en bonkende beats is gedacht.

![](/image/2019-06-19_pingpong-goto.jpg)
{: .center}

Sam Aaron trekt nog eens alle registers open van zijn Sonic Pi en bouwt een feestje.

{% video /video/2019-06-19_sonic-pi-goto.mp4 /video/2019-06-19_sonic-pi-goto.jpg %}

[[Meer info](https://gotoams.nl/2019/sessions/1055)]

