---
layout: series_post
title: "20 december: Bjúgnakrækir"
date: 2019-12-20 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Bjúgnakrækir]({% link /image/iceland/jolasveinarnir/bjugna.jpg %})
> {: .right}
> 
> De negende was **Worstensnaaier**,  
> een sluw en snel.  
> Hij klom op de spanten  
> en begon z'n plundering.
> 
> Op een dwarsbalk in de keuken,  
> in roet en rook  
> at hij de hangende worsten,  
> alsof er niets aan de hand is. 
