---
layout: series_post
title: "12 december: Stekkjastaur"
date: 2019-12-12 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Stekkjastaur]({% link /image/iceland/jolasveinarnir/staur.jpg %})
> {: .right}
> 
> De eerste van hen was **Springstok**.  
> Hij kwam stijf als een plank,  
> jagend op de schapen van de herders,  
> zo veel hij maar kon.
> 
> Hij wilde de ooien melken,  
> maar dat viel nog niet mee.  
> Zijn stijve knieen  
> \- ze verhinderden het.
