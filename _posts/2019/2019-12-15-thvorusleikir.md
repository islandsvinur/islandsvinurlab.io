---
layout: series_post
title: "15 december: Þvörusleikir"
date: 2019-12-15 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Þvörusleikir]({% link /image/iceland/jolasveinarnir/tvoru.jpg %})
> {: .right}
> 
> De vierde, **Lepellikker**  
> was vreselijk dun.  
> En oh, wat was hij blij,  
> als de kok even vetrok.
> 
> Dan haastte hij zich als de bliksem  
> om de lepel de grijpen,  
> en hield haar met beide handen,  
> omdat ze soms glad was.
