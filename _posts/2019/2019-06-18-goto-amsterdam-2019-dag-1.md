---
layout: series_post
title: "GOTO Amsterdam 2019: dag 1"
date: 2019-06-18 22:06:16 +0200
series: goto-amsterdam-2019
tags: [vimeo]
---
De eerste dag van GOTO begint met een rockster. Voor mij gaat het vervolgens 
verder in de mindfulness, het Spotify model, leren programmeren met 
electronische muziek, het modelleren van tijd, augmented reality, het leven van 
een ondernemer en tenslotte de ethiek van machines.

## Keynote: Turning your customers into fans

Bruce Dickinson is schrijver, piloot, ondernemer, maar vooral is hij de
leadzanger van Iron Maiden. Hij vond dat klanten maar vervelend zijn, omdat ze
een hele nare eigenschap hebben: ze kunnen weglopen. Daarom is het volgens hem
veel waardevoller te investeren in een fan-relatie in plaats van de
klant-relatie.

> In een wereld waar muziek bijna gratis is, willen fans best 50 euro neertellen
voor een t-shirt.

Hij sluit af met de herinnering dat de platenlabels het zwaar hadden in het
afgelopen decennium en zelfs de allergrootsten de digitalisering niet allemaal
overleefden, maar dat de bands er nog gewoon zijn. De muziekliefhebbers waren
klanten van de labels, maar zijn nog steeds fans van de muziek.

[[Meer info](https://gotoams.nl/2019/sessions/764)]

## Mind as code: Mindfulness for developers and knowledge workers

Toen Markus Wittwer voor het eerst in aanraking met mindfulness kon hij er vrij
weinig mee aanvangen. Het was te vaag, te zweverig en te esotherisch. Hij was
dan ook blij om te zien dat in de decennia die erop volgden er steeds meer
aandacht vanuit de wetenschap was naar wat het is dat mindfulness zo effectief
maakt.  Het onderzoek zet het wat meer met beide benen op de grond, maar de
aard van de onderzoeken maakt het ook mogelijk om de praktijk op een meer
westerse manier aan te pakken.

Wat betreft mindfulness zijn de drie belangrijkste vaardigheden:

* Concentratie: Focus op wat je nu belangrijk vindt, want *aandacht* is de
  meest schaarse "resource" die we hebben als kenniswerker.
* Helderheid: Ervaar het hier en nu met hogere resolutie en minder vertraging,
  want daardoor kun je beter reageren op de situatie.
* Gelatenheid: Verzet je niet tegen ongemak, want zo reageer je beter op
  onzekerheid en verandering. 

Deze vaardigheden worden ontwikkeld door *oefening*, wat een iets minder
zweverig woord is voor *meditatie*. We hebben een gezamenlijke meditatie-sessie
gehouden waarin we probeerden onze aandacht op een enkel punt te focussen en
daardoor dat punt met verhoogde resolutie te kunnen waarnemen. Door steeds de
focus los te laten en weer terug te vinden, oefen je je hersens om dit sneller
en beter te doen. 

Hiermee kan je jezelf oefenen om sneller in diepe concentratie te komen;
alleszins geen onnodige vaardigheid in de gemiddelde kantoortuin. 

Voor mij was dit de eerste kennismaking met mindfulness en het heeft mijn
interesse gewekt. Ik zal de komende tijd er zeker nog wat meer in gaan duiken.

[[Meer info](https://gotoams.nl/2019/sessions/774)] | [[Video](https://youtu.be/5UgpoaM5670)]

## Death of the IT manager: Being a tech lead in modern IT organizations

Han Markslag & Marcin Pakulnicki werken als tech leads voor ING. Ze leggen uit
dat ING sinds een aantal jaren het Spotify model volgt wat betreft
software-ontwikkeling en wat dat betekende voor de rollen die verdwenen en de
nieuwe rollen die verschenen.

De silo's van de projectorganisatie van afdelingen met aan de hoofd een IT
manager met diverse, tegenstrijdige verantwoordelijkheden (persoonlijke
ontwikkeling, projectsucces, technologieontwikkeling) maakten plaats voor een
aantal grote teams met aan het hoofd een manager die voor de persoonlijke
ontwikkeling is, en daarbinnen projectgroepen met projectleiders en
expertisegroepen met technologisch leiders.

Dat [het Spotify model eigenlijk niks meer is dan de aloude
matrixorganisatie](http://www.christiaanlam.nl/2016/05/18/het-spotify-model-rijnlands-denken-in-een-ander-jasje/)
wordt door velen vergeten als ze het hebben over hippe termen als Tribes,
Squads, Chapters en Guilds. Het klinkt gewoon lekkerder dan Afdeling,
Projectgroep, Expertisegroep en Interessegroep en toch zijn er in Nederland
bedrijven die al veel langer dan Spotify zo functioneren, zoals Océ.

[[Meer info](https://gotoams.nl/2019/sessions/970)]

## Keynote: Get ready to rock with Sonic Pi: The live coding music synth for everyone

De boodschap van Sam Aaron, ontwikkelaar van [Sonic Pi](https://sonic-pi.net/),
is dat programmeeronderwijs onmisbaar is in het huidige onderwijs, maar dat het
huidige curriculum zó saai is dat er natúúrlijk niemand het graag wil leren.

Programmeren is de taal van de 21e eeuw. Lang niet alle kinderen zullen
professioneel programmeur worden, net zomin als dat ze in de 20e eeuw allemaal
professioneel schrijver werden omdat ze hebben leren schrijven. Het is echter
wel verrekte handig dat we (bijna) allemaal kunnen schrijven!

Kinderen zien op tv "hackers" die voor flitsende schermen op een toetsenbord
aan het hameren zijn en binnen tien seconden ingebroken zijn, maar als ze zelf
in de stoel van Neo, Zero Cool of Crash Override gaan zitten mogen ze alleen
maar "Hello World" oneindig vaak op hun scherm laten verschijnen.

Zijn betoog is dat als we leerlingen al op de basisschool willen laten
kennismaken met programmeren en programmeurs, we ze iets moeten bieden wat
aanspreekt. Op "neem je papa of mama naar school"-dag kan ook een programmeur
iets aansprekends laten zien, wat misschien niet direct met het werk te maken
heeft, maar wat wel inspirerend kan zijn voor nieuwsgierige kinderen.

Zijn Sonic Pi produceert muziek op basis van code, echte code:

```ruby
live_loop :melody do
  notes = (scale :e4, :minor_pentatonic)
  16.times do
    play notes.choose, cutoff: rrand(90, 110)
    sleep 0.25
  end
end

live_loop :bass do
  use_synth :prophet
  play 30, release: 0.5, cutoff: rrand(50, 90)
  sleep 0.25
end
```

Het meest toffe is dat het systeem live-editing ondersteunt, waardoor je
terwijl je programma in een loop draait, de code kan wijzigen en zo dus de
muziek verandert. Op deze manier wordt live coding hetzelfde als een
muziekinstrument bespelen, want ook dit vereist heel veel oefenen!

{% vimeo "https://vimeo.com/112079774" %}

De software integreert met traditionele (electronische) muziekinstrumenten en
andere randapparatuur via [MIDI](https://en.wikipedia.org/wiki/MIDI) en
[OSC](https://en.wikipedia.org/wiki/Open_Sound_Control) en heeft een flinke
verzameling aan effect-pedalen, software synthesizers en samples ingebouwd om
snel resultaat te bereiken. Ik wilde Jasper al introduceren aan mijn oude
[Technics KN-901](https://www.youtube.com/watch?v=-nEKTVv04KM) en nu wil ik dat
ding ook nog met mijn MacBook aansturen.

[[Meer info](https://gotoams.nl/2019/sessions/800)] | [[Video](https://youtu.be/OLLwG_SN8oo)]

## Temporal modelling

Matthias Verraes

[[Meer info](https://gotoams.nl/2019/sessions/817)] | [[Video](https://youtu.be/KNqOWT0lOYY?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## Augmented Reality, beyond virtual objects floating in physical space

Morten Birk is een beetje teleurgesteld over waar augmented reality (AR) lijkt
te blijven hangen. Geinige filtertjes die ons in een panda veranderen en dino's
over tafel laten lopen. Allemaal leuk, maar het zijn allemaal kostenposten en
leveren geen directe inkomsten op. 

De echte toepassingen laten intussen op zich wachten, terwijl de technologie al
ver genoeg is. In de industrie wordt geprobeerd AR in te zetten om het
personeel te helpen bij complexe taken, zoals montage en onderhoud, maar het
blijft bij knullige simulaties die uit elkaar vallen als de omgeving er nét
iets anders uitziet dan de maker had bedacht.

[[Meer info](https://gotoams.nl/2019/sessions/785)] | [[Video](https://youtu.be/cl9A1wWjFEA)]

## Journey of an entrepreneur: the story of a product startup from a founders perspective

Koen Bos is mede-oprichter van Relay42, die zich bezighouden met het
onderhouden en verstevigen van de klant-relaties van hun eigen klanten door het
inzetten van slimme technologiën die losse interacties omzetten in
betekenisvolle dialogen. Dat lijkt extreem veel op
[BlueConic](https://blueconic.com/), een spin-off van [GX
Software](https://gxsoftware.com).

Hij schetste zijn beeld van het leven in een startup.

[[Meer info](https://gotoams.nl/2019/sessions/1065)] | [[Video](https://youtu.be/gCVFEFAfl_E?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

## Keynote: Machine ethics

Nell Watson

[[Meer info](https://gotoams.nl/2019/sessions/782)] | [[Video](https://youtu.be/o8I0M5vjdyE?list=PLEx5khR4g7PKT9RvuVyQxJLO8CZUJzNMy)]

