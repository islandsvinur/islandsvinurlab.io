---
layout: series_post
title: "16 december: Pottaskefill"
date: 2019-12-16 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Pottaskefill]({% link /image/iceland/jolasveinarnir/potta.jpg %})
> {: .right}
> 
> De vijfde **Pottenschraper**,  
> was een merkwaardige kerel.  
> Wanneer de kinderen de korstjes kregen,  
> klopte hij op de deur.
> 
> Ze renden om te kijken  
> of de gast er al vandoor was.  
> Dan haastte hij zich naar de potten  
> en haalde zijn goede prijs.
