---
layout: series_post
title: "13 december: Giljagaur"
date: 2019-12-13 05:00:00 +0200
series: jolasveinarnir
category: [ijsland]
---
> ![Giljagaur]({% link /image/iceland/jolasveinarnir/gilja.jpg %})
> {: .right}
> 
> **Gulzige Gijs** was de tweede,  
> met zijn grijze hoofd.  
> Hij gleed door een kloof naar beneden,  
> en schoot zo de koeienstal in.
> 
> Hij verstopte zich in de hokken,  
> en stal het schuim van de melk,  
> terwijl de boerin  
> aan het praten was met de boer.
