---
title: Maak de Raspberry Pi PoE-hat ventilator stiller
tags: [ notes ]
layout: post
lead_image: /image/stable-difussion/raspberry-pie-ethernet.png
---

Ik heb een Raspberry Pi met Power-over-Ethernet (PoE) hat in de meterkast hangen (met onder andere een P1 kabel om de
slimme meter uit te lezen). Het nadeel is dat de PoE hat een kleine ventilator heeft die standaard erg agressief staat
afgesteld. Gelukkig is die te configureren zodat de temperatuur in het veilige bereik blijft en je niet gek wordt van
het gejank van die kleine herrieschopper.

In `/boot/firmware/config.txt`:

```
# PoE Hat Fan Speeds
dtoverlay=rpi-poe-plus
dtparam=poe_fan_temp0=65000,poe_fan_temp0_hyst=5000
dtparam=poe_fan_temp1=67000,poe_fan_temp1_hyst=2000
dtparam=poe_fan_temp2=69000,poe_fan_temp2_hyst=2000
dtparam=poe_fan_temp3=71000,poe_fan_temp3_hyst=2000
```

Reboot je Pi en check of het naar wens is:

```
$ od -An --endian=big -td4 /proc/device-tree/thermal-zones/cpu-thermal/trips/trip?/temperature /proc/device-tree/thermal-zones/cpu-thermal/trips/trip?/hysteresis
       65000       67000       69000       71000
        5000        2000        2000        2000
```

Let erop dat er _vier_ temperaturen zijn die je kan instellen, als ze niet expliciet zijn opgegeven in `config.txt`
hebben ze een standaardwaarde die waarschijnlijk niet is die je wil.

## Links

- <https://github.com/raspberrypi/firmware/blob/1.20240529/boot/overlays/README#L4062-L4082>
- <https://github.com/raspberrypi/firmware/issues/1689#issuecomment-1053403166>
- <https://jjj.blog/2020/02/raspberry-pi-poe-hat-fan-control/>
- <https://pimylifeup.com/raspberry-pi-temperature/>
- <https://www.jeffgeerling.com/blog/2021/taking-control-pi-poe-hats-overly-aggressive-fan>
