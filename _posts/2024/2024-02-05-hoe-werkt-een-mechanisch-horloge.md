---
layout: post
title: Hoe werkt een mechanisch horloge?
date: 2024-02-05 22:15:00 +01:00
lead_image: /image/unsplash/brooke-campbell-Rw2-Y0nSIKQ-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Brooke Campbell
        username: bcampbell
---

In navolging van [mijn vorige post]({% link _posts/2024/2024-02-05-waar-zijn-alle-websites-heen.md %}), waarin ik zei
dat we onze rol van ontdekkingsreizigers op het wereldwijde web weer moesten herpakken, vond ik een prachtig voorbeeld
van zo'n website die er gewoon is _om te zijn_. De site van [Bartosz Ciechanowski](https://ciechanow.ski/) bevat niet
heel veel artikelen, maar het zijn stuk voor stuk juweeltjes. Met werkende 3D modellen legt hij in jouw eigen webbrowser
uiteenlopende principes stap voor stap uit.

Een kleine greep uit het aanbod: Een gedetailleerde omschrijving van de
verschillende [bewegingen in een mechanisch horloge](https://ciechanow.ski/mechanical-watch/), van de grote veer tot aan
de datumaanwijzer, de kroon en het automatisch opwindmechanisme. Een uiteenzetting van de principes
achter het [Global Positioning System (GPS)](https://ciechanow.ski/gps/), waaruit ook meteen af te leiden is
dat zowat elke televisieserie die GPS-signalen beweert te hacken volledige onzin is. Een hogerdimensionale reis in de
wereld van de [tesseract](https://ciechanow.ski/tesseract/) die je doet duizelen. Een technische uitleg hoe een digitale
computer toch met [floating point getallen](https://ciechanow.ski/exposing-floating-point/) kan rekenen.

Elke pagina is weer een ijzersterk didactisch verhaal gecombineerd met een vernuftig stukje techniek.
