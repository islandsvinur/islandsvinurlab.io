---
layout: post
title: Logging the Switch bestaat 20 jaar!
---

Twintig jaar geleden begon ik een weblog genaamd "Logging the Switch". Ik had destijds met genereuze hulp van de
Belastingdienst (met de pc-privéregeling
die [diezelfde maand nog werd afgeschaft](https://tweakers.net/nieuws/34019))
de [Apple Power Mac G5](https://en.wikipedia.org/wiki/Power_Mac_G5) gekocht en ging
van Linux op de Intel PC naar Linux op de PowerPC. Mijn ervaringen zou ik delen op dit weblog. Of althans, dat was de
bedoeling. De praktijk was anders.

Het was al vrij snel duidelijk dat zulke nieuwe hardware echt niet meteen ging werken op Linux. De CPU zelf werd in
eerste instantie al niet eens herkend, laat staan dat het systeem kon opstarten. Ik moest dus een andere "Switch" gaan
maken, naar Mac OS X. Maar dat was helemaal niet zo'n interessant onderwerp, dus al vrij snel kwamen andere dingen
voorbij, zoals [welke CDs ik kocht]({% link _posts/2005/2005-03-16-new-media-20050316.md %}),
welke [foto's ik had gemaakt]({% link _posts/2005/2005-08-22-harley-day-2005.md %}) en
welke [tijden ik op het ijs reed]({% link _posts/2007/2007-11-08-first-times.md %}). Soms in het Nederlands, soms in het
Engels.

In 2006 maakte ik een tijdelijk uitstapje naar een tweede weblog, [Christian in IJsland](https://ijsland.luijten.org),
omdat ik merkte dat die twee niet lekker samen gingen op hetzelfde adres. Twee totaal verschillende doelgroepen.

Na de studie zakte het aantal posts flink in. Ik heb meerdere keren besloten te gaan stoppen en de boel af te sluiten,
maar telkens kwam er toch weer een berichtje. Het leven stond even in de weg, we trouwden, kochten een huis en kregen
kinderen. Andere dingen werden even belangrijker. En toch, op een gegeven moment begon het weer te kriebelen en begonnen
de posts langzaam weer te komen, vooral over hobbyprojecten zoals Arduino en VR.

### Techniek door de jaren heen

De techniek is in de loop van de jaren flink veranderd, maar ik ben altijd op de achtergrond blijven knutselen en
schaven aan het weblog.

![De site met t-shirt-layout]({% link /image/screenshot-site-2001.png %}){: .left} Begin 2004 experimenteerde ik al wat
met [Movable Type](https://movabletype.org) embedded in mijn toenmalige website. Daarvan is nog een klein beetje te zien
op the [Internet Archive](https://web.archive.org/web/20040410185652/http://luijten.org/). Ik beschouw dat echter nog
niet echt als het begin van Logging the Switch, het was meer een probeersel.

Later dat jaar ging ik over naar [Instiki](https://golem.ph.utexas.edu/wiki/instiki/) (
van [David Heinemeier Hansson](https://world.hey.com/dhh) die later [Ruby on Rails](https://rubyonrails.org), Basecamp
en de e-maildienst Hey zou gaan ontwikkelen), waar Logging the Switch dan echt werd geboren. Website en weblog waren
toen nog gescheiden. Minder dan een jaar later zou ik de hele boel overzetten naar [Hobix](https://hobix.com/) en toen
waren ook website en weblog samengevoegd.

De jaren daarna is de weblog regelmatig verhuisd van hosting. Eerst heb ik bij [Spacelabs](https://www.spacelabs.nl/)
nog verschillende servers gebruikt en daarna kwam het op een eigen VPS te staan. In 2015 introduceerde GitHub
hun [Pages](https://pages.github.com), waarbij je elk repository (gratis) een website kon geven mét eigen domein. Het
bleek erg lastig om ouwe Hobix op zulk nieuw spul te draaien, want in de tussentijd was
de [ontwikkelaar van Hobix van de aardbodem verdwenen]({% link _posts/2009/2009-08-19-_why-is-kwijt.md %}) en hij had al
zijn code meegenomen. Hobix was al vijf jaar niet meer onderhouden en werkte niet meer samen met de nieuwste Ruby-versies
die GitHub Pages vereiste. Ik moest nogmaals de blog migreren naar een ander
systeem, [dat werd Jekyll]({% link _posts/2015/2015-04-07-migrated-to-jekyll.md %}).

De afgelopen acht jaar gebruik ik [GitLab Pages]({% link _posts/2019/2019-10-18-onderhoudswerkzaamheden.md %}), omdat
zij nét iets flexibeler zijn in hun automatisering en meer mogelijkheden bieden voor het genereren van statische
websites. De [site op GitHub](https://islandsvinur.github.io) staat blijkbaar wel nog steeds online, maar daar heeft de
tijd stilgestaan.

Het is ook niet zo dat ik de afgelopen acht jaar niets meer aan onderhoud heb gedaan, integendeel! Ik ben eigenlijk best
regelmatig aan het rommelen in de code die de site genereert om hier en daar wat te verbeteren, de navigatie te
stroomlijnen, het er nét iets mooier uit te laten zien. Eigenlijk doe ik dat meer dan nieuwe stukjes schrijven...

**Dit waren de eerste twintig jaar, wat gaan de volgende twintig brengen?**

*[VR]: Virtual Reality
