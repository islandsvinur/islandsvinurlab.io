---
layout: post
title: Waar zijn alle websites heen?
date: 2024-02-05 21:34:00 +01:00
lead_image: /image/unsplash/thomas-ashlock-7G5dkthFyxA-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Thomas Ashlock
        username: thomas_ashlock
---

Ik zag laatst
een [artikel op Slashdot](https://tech.slashdot.org/story/24/01/19/0835245/where-have-all-the-websites-gone) langskomen,
waar de schrijver zich
afvroeg [waar alle websites gebleven waren](https://www.fromjason.xyz/p/notebook/where-have-all-the-websites-gone/).

Het verhaal begint in 2009, toen het internet nog een digitale speeltuin was vol verrassingen. Facebook en Instagram
waren als de coole kids op het schoolplein die de laatste roddels verspreidden, en surfen op het web betekende het
intikken van webadressen met de hoop op een avontuurlijke ontdekkingsreis bij elke klik.

Fast forward naar 2024 en het internet lijkt een totale make-over te hebben ondergaan. De "Voor Jou Pagina" levert nu op
maat gemaakte content van je favoriete "creators", speciaal afgestemd op verschillende platforms. De ooit gevarieerde
wereld van websites lijkt echter te zijn verdwenen, alles is in hetzelfde format gedwongen, en dat roept vragen op over
de huidige toestand van het web en waarom we er helemaal niet zo blij meer mee zijn.

De echte verandering zit hem echter in onszelf. De vreugde van ontdekking en "curatie", ooit essentieel voor de
internetervaring, lijkt te zijn uitbesteed aan bedrijfsalgoritmen. We scrollen nu eindeloos passief door content,
vertrouwend op algoritmes voor onze digitale avonturen. Algoritmes die echter hele andere belangen dienen...

![CoderDojo: bouw je eigen website]({% link image/2024-02-05-coderdojo-bouw-je-eigen-website.jpg %}){: height="250"
.left}

De oplossing? Laten we teruggaan naar onze rol als ontdekkingsreizigers! Iedereen kan een curator worden door open
webportals te creëren. Gebruik platforms zoals Linktree.com om je favoriete blogposts, artiesten of coole websites te
delen. Heb je meer te melden, open je eigen website op WordPress.com en link ernaar vanuit je socials in plaats van jouw
ideeën op te sluiten in de platforms van anderen. Laten we de interconnectiviteit nieuw leven inblazen en het web weer
leuk maken om te ontdekken, zonder dat je een computernerd hoeft te zijn.

En als je nog een stapje verder wilt gaan hoeft het helemaal niet ingewikkeld te
zijn. [Het boek hiernaast](http://meisenmaas.nl/boeken/306/coderdojo-creeren-met-code-bouw-je-eigen-website/) heb ik bij
ons in de bibliotheek gevonden voor Jasper die op school als verrijkingsproject "Een eigen website" heeft gekozen. Bouw
je eigen website, stap voor stap, van de grond op. Je eigen virtuele volkstuintje op het internet.
