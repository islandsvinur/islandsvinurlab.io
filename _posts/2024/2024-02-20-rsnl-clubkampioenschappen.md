---
layout: post
title: RSNL Clubkampioenschappen 2024
modified_date: 2024-02-25
categories: [ sport ]
lead_image: /image/unsplash/vidar-nordli-mathisen-QckdmETcdXs-unsplash.jpg
lead_image_credit:
  unsplash:
    username: vidarnm
    name: Vidar Nordli-Mathisen
---

Na [meer dan veertien jaar]({% link _posts/2009/2009-10-11-schaatsseizoen-2009-2010.md %}) heb ik eindelijk weer eens
een wedstrijd gereden. Het was afwachten wat de overhand zou hebben; de verbetering van de techniek of de veroudering
van het gestel 😜. Het bleek het laatste te zijn, want de PRs van toen zijn nu ver buiten bereik gebleken. Ik moest me
tevredenstellen met een 01:03.220 op de 500 meter en 02:09.900 op de 1000 meter. Ter vergelijking, dit waren
de [PRs die ik bij Isis](https://essvisis.nl/results/user/619) heb gereden:

| Afstand | Datum                  | PR        |
|---------|------------------------|-----------|
| 300 m   | 2009-01-14 (Eindhoven) | 00:37.920 |
| 500 m   | 2009-02-18 (Eindhoven) | 00:57.720 |
| 1000 m  | 2009-02-18 (Eindhoven) | 01:58.710 |
| 1500 m  | 2009-02-19 (Eindhoven) | 02:57.660 |
| 3000 m  | 2008-03-11 (Thialf)    | 06:16.750 |

![Noud aan de start bij de NRW Pokal]({% link image/photos/2023-12-16-nrw-pokal-noud.jpeg %}){: .width-75 .lightbox}  
_2023-12-16: Noud aan de start bij de NRW Pokal_
{: .center}

Inmiddels heb ik wel opvolging gekregen, want Noud rijdt sinds dit jaar ook fanatiek op de schaats en heeft al een
aantal wedstrijden verreden. Met de sprongen die hij dit jaar heeft gemaakt duurt het niet lang voordat hij mijn tijden
verpulvert. Ik kijk er nu al naar uit, hij heeft er in ieder geval heel veel plezier in; op naar de laatste wedstrijd
van het seizoen!

| Datum      | Wedstrijd                 | 100 meter | 300 meter | 500 meter | 1000 meter |
|------------|---------------------------|-----------|-----------|-----------|------------|
| 2023-12-09 | Förderkreispokal          |           | 01:06.870 | 01:46.330 |            | 
| 2023-12-16 | NRW pokal (2)             | 00:22.370 |           | 01:44.070 |            |
| 2024-01-07 | Super Sprint              | 00:20.470 | 00:57.090 |           |            | 
| 2024-02-17 | RSNL Clubkampioenschappen |           |           | 01:31.660 | 02:56.190  |
| 2024-02-24 | NRW pokal (3)             | 00:18.140 | 00:51.760 |           |            | 

**Update 25 februari**: Tijden _NRW pokal (3)_ toegevoegd.
