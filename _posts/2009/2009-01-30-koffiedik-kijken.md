---
layout: post
title: "Koffiedik kijken met CoffeeDregs"
categories: [studie]
date: 2009-01-30 12:02:35 +0100
---

In augustus had ik [hier al
geschreven]({% post_url 2008/2008-08-15-alle-vakken-af %}) dat ik koffiedik zou
gaan kijken. Nu, vijf maanden later is het werk aan
[CoffeeDregs](https://svn.win.tue.nl/trac/CoffeeDregs/) grotendeels klaar.  Ik
ben nu nog een beetje aan het schaven om de laatste probleempjes eruit te
werken, maar het grote werk is af.

![Een screenshot van CoffeeDregs in actie]({% link /image/software/coffeedregs-in-actie.png %}){: .width-75}  
_Een screenshot van CoffeeDregs in actie_
{: .center}

De gebruiker kan nu een Java-broncode inladen en deze met behulp van de tool
uitvoeren. Je ziet dan de objecten gecreeerd en verwijderd worden, referenties
gelegd, methoden aangeroepen worden, hoe waardes in variabelen veranderen. We
(m'n begeleiders en ik) hopen over een paar weken een aantal studenten met de
tool te laten spelen en kijken hoe ze ermee omgaan, om te zien of het bruikbaar
zou zijn om vanaf september in het programmeer-onderwijs in te gaan zetten. 

Helaas maak ik die lancering natuurlijk niet meer mee, maar interessant gaat
het zeker worden; ik heb dan toch een klein beetje een stempel weten te drukken
op het onderwijs van de [TU/e](http://www.tue.nl/)!

De komende twee maanden ga ik m'n verslag schrijven en de experimenten
uitvoeren en dan ben ik hopelijk afgestudeerd!
