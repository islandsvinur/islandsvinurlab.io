---
layout: post
title: "We hebben weer natuurijs!"
date: 2009-01-02 14:02:57 +0100
tags: [vimeo]
---

Er ligt alweer even een flinke laag ijs op de Nederlandse meren. Net in het
oude jaar zijn we nog eens naar het [Oud Meer in
Son](https://www.google.nl/maps/place/Oud+Meer/@51.5153892,5.4577301,17z/data=!4m13!1m7!3m6!1s0x47c6de8a87c6338d:0xc85f5438d5f03239!2sOud+Meer,+Son!3b1!8m2!3d51.5134396!4d5.469328!3m4!1s0x47c6de7d415ba1a5:0xe0b1d5ebdd8b9f55!8m2!3d51.5155155!4d5.4584911) getrokken om het uit te
proberen. Videos na de klik/enter/whatever.

## 30 december

{% vimeo https://vimeo.com/2696294 %}

## 3 januari

{% vimeo https://vimeo.com/2708319 %}
