---
layout: post
title: "Ons nieuwe hutje: Woningjacht voorbij!"
date: 2009-03-30 21:09:20 +0200
tags: [vimeo]
---

In november schreef ik over [m'n jacht naar een
woning]({% post_url 2008/2008-11-07-op-woningjacht %}) in Eindhoven. Aan die jacht is
bijna een einde gekomen, want in april krijgen we de sleutels voor ons eigen
appartementje!

{% vimeo https://vimeo.com/3926735 %}

Op 30 maart werden we door Woonbedrijf uitgenodigd een kijkje te komen nemen in
[ons nieuwe appartement]({% link files/plattegrond-de-toeloop.pdf %}).
Wat vinden jullie ervan? Laat een reactie achter!
