---
layout: post
title: Arduino ✕ PlatformIO
tags: [ Arduino, PlatformIO ]
lead_image: /image/unsplash/claudio-schwarz-294j9hG1N3w-unsplash.jpg
lead_image_credit:
  unsplash:
    username: purzlbaum
    name: Claudio Schwarz
---

De electronica en code voor de Rheinturm zijn al een hele tijd af, maar ik moet nog steeds een "behuizing" voor het
geheel bedenken. Ik twijfel nog tussen een eenvoudige houten plaat of een uitgesneden vorm, maar
een [3D geprint model](https://www.thingiverse.com/thing:5927614) zou ook heel tof zijn. Blijkbaar zijn 3D-printers
tegenwoordig ook niet meer super duur als je van ~~prutsen~~ [tinkeren](https://duckduckgo.com/?q=tinkeren) houdt...
Anyway, dat is weer een heel andere hobby!

Voorlopig ligt de boel dus veilig in een doos opgeborgen te wachten op de afwerking. Tot die tijd wil ik de code wel een
beetje netjes achterlaten, zodat het later makkelijk weer op te pakken is. Daarbij heb ik al gemerkt dat de Arduino IDE
niet het beste stuk software is om dat mee te doen. Arduino-Rheinturm gebruikt een aantal software-bibliotheken die
ingeladen moeten worden en de vraag is of die over twee of drie jaar nog werken in de Arduino IDE.

Daarnaast is het prototype wat ik nu heb gebouwd weliswaar gebaseerd op een Arduino UNO, maar heb ik tegen de tijd dat
ik de definitieve versie maak, misschien een ander type controller waar de Arduino IDE helemaal niet mee samenwerkt. Wat
ik nodig heb, is een manier om te zeggen welke bibliotheken ik nodig heb en hoe de broncode moet worden gebouwd voor de
controller die ik op dat moment heb. Als je ooit een Java-project hebt gemaakt, zul je denken aan Maven of Gradle. Voor
Ruby heb je Gemspecs en Bundler, voor Python is er `setup.py` (of tegenwoordig `project.toml`) en `pip` en in de
JavaScript-wereld gebruik je `package.json` en `npm`.

## PlatformIO

Het van oorsprong Oekraïense project [PlatformIO][platformio] is zo'n tool voor embedded software development. Een
enkele `platformio.ini` in je project en de sources op een aangewezen
plek ([Convention over Configuration](https://nl.wikipedia.org/wiki/Convention_over_configuration)), meer heb je niet
nodig om snel weer op weg te zijn als je een tijd weg bent geweest van je project.

```ini
[env:uno]
platform = atmelavr
board = uno
framework = arduino
lib_deps =
    bxparks/AceTime @ ^2.0.1
    adafruit/Adafruit NeoPixel @ ^1.11.0
    northernwidget/DS3231 @ ^1.1.2
    rlogiacco/CircularBuffer @ ^1.3.3
```

Doordat spullen op voor PlatformIO bekende plaatsen staan, kan die ook de benodigde projectbestanden genereren om het te
openen in je IDE, zonder dat die bestanden in Git hoeven te worden gezet. Vandaag de dag werk ik met JetBrains CLion,
maar misschien is dat morgen Visual Studio Code. Die snappen elkaars projectstructuur niet, dus dan is het fijn dat je
niet helemaal opnieuw hoeft te beginnen met de inrichting, maar gewoon `platformio -c clion init`
of `platformio -c vscode init` kan doen en alles staat weer recht.

Daar houdt het echter niet op, want als ik een andere microcontroller gebruik, dan kan ik eenvoudig een nieuwe minimale
configuratie aanmaken en ben ik ook weer op weg. Met de juiste hardware zou het zelfs mogelijk moeten zijn om unit
testen te schrijven die de code kan verifiëren.

## CLion

De overstap van Arduino IDE via PlatformIO naar CLion was wel fundamenteel, want plotseling had ik een hele krachtige
omgeving tot mijn beschikking waarmee het programmeren in C++ daadwerkelijk ondersteund wordt. Het nodigde uit tot
opschonen en opsplitsen van de code, waardoor ik nu een heel nette structuur heb die ik over een jaar of twee ook nog
begrijp. En ja, dan kan je de [code ook maar beter publiek maken][arduino-rheinturm], want misschien heeft iemand er
ooit nog iets aan.

## Links

- [Arduino Rheinturm op GitLab][arduino-rheinturm]
- [PlatformIO][platformio]

[arduino-rheinturm]: https://gitlab.com/islandsvinur/arduino-rheinturm

[platformio]: https://platformio.org/
