---
layout: post
title: Djöflaterta
tags: [ijsland, recept]
lead_image: /image/2023-07-24-djoflaterta.jpeg
---

Lang geleden [leefde ik een paar maanden in IJsland](https://ijsland.luijten.org/), waar ik kennismaakte met de perfecte
IJslandse variant van de Devil's Pie: [Djöflaterta](https://ijsland.luijten.org/2006/11/30/duivelse-taart/). Nu was ik
niet helemaal kapot van het recept wat ik destijds had en ben ik op zoek gegaan naar een nieuwe samenstelling.

Ik heb denk ik de perfecte receptuur nu
gevonden: [Djöflaterta sem bráðnar í munninum](https://www.hun.is/djoflaterta-sem-bradnar-i-munninum-uppskrift/) oftewel
"Duivelse taart die smelt in je mond".

## Voor het beslag

Voeg de ingrediënten een voor een toe en blijf mixen op hoge snelheid.

> 150 gr margarine  
> 1 1/2 cup suiker (355 ml)  
> 3 eieren  
> 2 cups bloem (473 ml)  
> 1 tl bakpoeder  
> 1/2 tl zout  
> 1 tl baking soda  
> 2 tl vanilledruppels  
> 1 cup melk (237 ml)  
> 2 el cacao

> Meng alles door elkaar en doe in 2 springvormen en bak 20-30 min op 180°. Houd het in de gaten, zodat het bovenop niet
> te donker wordt.

Met een enkele springvorm lukt het ook, dan duurt het ongeveer 40-50 minuten en moet je het uiteindelijk horizontaal
snijden. Dit recept gebruikt zowel bakpoeder als baking soda; het deeg is dan ook heel erg "fluffy" als het is gebakken.

## Voor de crème

> 50 gr boter  
> 300 gr poedersuiker  
> 1 ei  
> 1 tl vanilledruppels  
> 2-6 el melk  
> 2 el cacao  
> of 2 eieren en sla de melk over

Laat de cake volledig afkoelen voordat je de creme erop smeert, want anders loopt het er zo weer uit.

## Voor de ganache

Hiervoor heb ik een ander "[recept](https://rutgerbakt.nl/basisrecepten/ganache-maken/)" gevonden wat goed werkt.
Gebruik pure chocolade en slagroom in 1:1 verhouding. Smelt de chocolade au bain-marie en laat het een beetje afkoelen.
Breng de slagroom aan de kook en voeg het bij de chocolade. Goed roeren en laten afkoelen tot het een stevige pasta is,
dat kan prima in de koelkast. Wel regelmatig doorroeren zodat je niet een grote dikke klont krijgt.

Nu kan je de pasta eenvoudigweg eroverheen smeren en ziet het er zalig uit, en zo smaakt het ook...

![Djöflaterta]({% link /image/2023-07-24-djoflaterta.jpeg %})
