---
layout: post
title: Lichtzeitpegel in je woonkamer
tags: [WS2812B, Arduino]
lead_image: /image/unsplash/mohammed-alorabi-xCtFTKAvlm0-unsplash.jpg
lead_image_credit:
    unsplash:
        username: i_alorabi
        name: mohammed alorabi
---

Naast programmeren was ik altijd al geïnteresseerd in de hardware-kant, wat zelfs in een vlaag van
verstandsverbijstering leidde tot een studiejaar elektrotechniek. Wat me altijd een beetje er vanaf hield om er meer
mee te doen was de enorme verscheidenheid aan componenten en het grillige gedrag van analoge schakelingen wat ik niet
altijd goed begreep. Er was eigenlijk gewoon teveel wat ik niet wist om goed te beginnen.

Sinds een paar maanden rommel ik soms in de avonduren wat met de
[Arduino Starter Kit](https://store.arduino.cc/products/arduino-starter-kit-multi-language) die ik een tijd geleden heb
aangeschaft. Eigenlijk zijn hiermee beide problemen opgelost, want ik hoefde nu niet zelf te bepalen welke componenten
ik nodig zou gaan hebben en de voorbeelden zijn van een niveau dat analoog gedoe geen roet in het eten gooit. Hierdoor
begon ik beter te begrijpen wat de verschillende componenten doen; in ieder geval beter dan tijdens het college
Netwerken.

![]({% link image/2023-01-21-lichtzeitpegel/starterkit_02.unbox_1000x750.webp %}){: .width-50 .lightbox}
{: .center}

## Rheinturm in Düsseldorf

![]({% link image/2023-01-21-lichtzeitpegel/440px-Düsseldorf_Rheinturm_-_Lichtzeitpegel.jpg %}){: .right width="200"}

Van het een kwam het ander en zo kwam ik uiteindelijk terecht op een ander verloren project uit de jaren 90; het
nabouwen van de decimale klok in de [Rheinturm in Düsseldorf](https://nl.wikipedia.org/wiki/Rheinturm). Dit vereist
wellicht wat meer uitleg, want de [Lichtzeitpegel](https://de.wikipedia.org/wiki/Lichtzeitpegel) zoals de installatie
heet is nét iets minder bekend dan pakweg de verlichting van de Eiffeltoren of de melodie van Big Ben.

Langs de toren zijn 62 lampen aangebracht die van boven naar onder de decimalen van een 24-uurs klok voorstellen. De
eerste twee lampen geven de tientallen uren aan, de volgende tien lampen de enkele uren, daaronder zes lampen die
tientallen minuten aangeven en tien lampen de enkele minuten. Ten slotte zijn er nog zes lampen voor de tientallen
seconden en tien lampen voor de enkele seconden.

De klok hiernaast geeft 16:56:39 aan, omdat van boven naar onder 1, 6, 5, 6, 3 en 9 lampen branden. Er zijn nog wat
extra's, zoals scheidingslampen tussen de groepen die op elk volle uur een minuut lang branden, maar dit zijn de pure
regels voor de klok.

### Het Elektor project

De toren met klok staat al sinds 1981, maar eind jaren 90 werd in het
blad [Elektor 05-1998](https://www.elektormagazine.de/magazine/elektor-199805/31489) een elektrisch schakelschema
gepubliceerd waarmee de klok na te bouwen was. Omdat het een erg populair artikel was, werd het
in [Elektor 01-2000](https://www.elektormagazine.de/magazine/elektor-200001/851) nog eens dunnetjes overgedaan.

In onderstaande video laat Roger Leifert zijn versie zien die nog steeds voldoende aandacht krijgt op
tentoonstellingen. Vanaf 6:20 zie je de ingewikkelde schakeling en lijmblob die alle leds bij elkaar houdt.

{% youtube "https://www.youtube.com/watch?v=nEt6M_76v3Y&t=380s" %}

### ... en toen kwam de WS2812B

De WS28xx serie RGB LED modules veroorzaakten een revolutie in aanstuurbare verlichting. Voeding en aansturing werd
gescheiden, waardoor je schakeling een stuk eenvoudiger kan worden. Daarnaast biedt de WS28xx ook een _uitgangssignaal_,
wat alle inkomende berichten doorstuurt _minus het bericht wat voor de huidige LED is bedoeld_! Dat betekent dat je er
ketens van kan bouwen die je met een enkele data-lijn kan aansturen!

![Schakelschema van drie WS2812B in serie]({% link image/2023-01-21-lichtzeitpegel/app201911271500538.png %}){: .width-75 .lightbox}  
_Drie maal WS2812B in serie_
{: .center}

Je kan zelf de losse WS2812Bs aan elkaar koppelen, maar er zijn ook kant-en-klare LED-strips die zelf op lengte te maken
zijn. Ze zijn er met 30, 60 en zelfs 144 LEDs per meter. Ze zijn er trouwens ook in de vorm van ringen, arrays (meerdere
strips naast elkaar), ze zijn waterdicht en stofdicht verkrijgbaar. Voor mijn toepassing is 60 / meter optimaal; het
model van de Lichtzeitpegel wordt dan ongeveer een meter hoog.

![]({% link image/2023-01-21-lichtzeitpegel/IMG_7746.jpeg %}){: .width-75 .lightbox}  
_Belachelijk simpele schakeling_
{: .center}

Het resultaat is in de basis een belachelijk simpele schakeling met alleen twee draadjes voor de power en een
weerstandje tussen de I/O poort van de Arduino en de data-lijn van de WS2812B strip. De Arduino in combinatie met de
[NeoPixel library](http://reference.arduino.cc/reference/en/libraries/adafruit-neopixel/) doet de rest. In de Arduino
IDE kan je voorbeeld-code voor de NeoPixel library inladen. Deze laat onder andere onderstaande bewegende
regenboog zien. Mooi! De schakeling werkt!

![]({% link image/2023-01-21-lichtzeitpegel/neopixel-strandtest.gif %}){: .width-75 .lightbox}  
_NeoPixel 'strandtest' demonstratie_
{: .center}

De enige uitdaging die er nu nog overblijft, is de code voor de klok schrijven. Dát is voor een volgende post!
