---
layout: post
title: "Hemelrijken Gemist"
date: 2008-10-11 15:52:01 +0200
---
De website [Hemelrijken Gemist](http://hemelrijkengemist.nl/) bestaat al wat
langer, maar ik had het hier nog helemaal niet weggezet. Welnu, ik heb dus
een nieuwe website opgezet die het doel heeft het leven in Hemelrijken weer
te geven voor de oud-bewoners en tijdelijk elders wonenden. Het is een
nogal eenzijdige blik natuurlijk, van een enkel groepje bewoners, maar het
geeft toch een idee.

Ik heb trouwens op Eindhoven-in-Beeld.nl wat [foto's uit de
buurt](http://www.theoldhometown.com/eindhoven/list.asp?field=groups&crit=251)
gevonden.  Als je erop klikt krijg je een grote versie, met de
pijltjestoetsen kan je er doorheen bladeren.

[![]({% link photos/2008-10-11-hemelrijken-gemist/5341.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=5341)
{: .center}

Foto uit 1929. De tegenwoordige dagopvang voor daklozen (oorspronkelijk de rectorswoning) is nog niet gebouwd.
{: .center} 

[![]({% link photos/2008-10-11-hemelrijken-gemist/5343.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=5343)
{: .center}

Foto uit 1970. Het begroeide gebouw is tegenwoordig volledig weg, ook de stenen muur die de gebouwen met elkaar verbond is weg. Overblijfselen hiervan zijn tegenwoordig nog wel zichtbaar.
{: .center} 

[![]({% link photos/2008-10-11-hemelrijken-gemist/5344.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=5344)
{: .center}

Foto uit 1931. Wasplaatsen, onbekend waar deze zich bevonden.
{: .center} 

[![]({% link photos/2008-10-11-hemelrijken-gemist/5345.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=5345)
{: .center}

Foto uit 1931. Een keuken, aan de vloer en de plafondconstructie te zien op de begane grond.
{: .center} 

[![]({% link photos/2008-10-11-hemelrijken-gemist/5346.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=5346)
{: .center}

Foto uit 1948. Een kamer in het pensionaat.
{: .center}

[![]({% link photos/2008-10-11-hemelrijken-gemist/9895.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=9895)  
Datum onbekend, maar in de jaren 70 en 80 zat hier een wijkcentrum.
{: .center} 

[![]({% link photos/2008-10-11-hemelrijken-gemist/11050.jpg %})](https://www.eindhoveninbeeld.com/photodetail.php?id=11050)
{: .center}

Foto uit 1982. Tegenover het klooster stonden deze huizen, nu staat er basisschool de Fellenoord.
{: .center}

![]({% link photos/2008-10-11-hemelrijken-gemist/6d211e441812be6abbcf5acd3d042c36.JPG %})
{: .center}

De gang op de begane grond. De hoekige boogconstructie is nog steeds zichtbaar aanwezig.
{: .center}

![]({% link photos/2008-10-11-hemelrijken-gemist/4efcbbda422e6273085d43499077e233.JPG %})
{: .center}

De Kapel. De blokken die in onze keuken uit de muur steken zijn [kraagstenen](https://nl.wikipedia.org/wiki/Console_(bouwkunde)), bedoeld om de dakconstructie van de kapel te dragen. Zeer waarschijnlijk zijn de kraagstenen op deze foto precies dezelfde die je in ons huis nog steeds ziet. Hier zie je ook dat er dus een verdieping is toegevoegd bij de grote verbouwing; onze keuken zweeft ergens in het midden van de kapel.
{: .center}
