---
layout: post
title: "Skeuveltoernooi 2008"
categories: [sport]
date: 2008-03-11 19:45:59 +0100
---

Wauw, dat was gaaf, een toernooi rijden in Thialf. Zoals het [vorig jaar]({%
post_url 2007/2007-07-02-summer-ice %}) gaaf was om überhaupt op het
Heerenveense ijs te staan, was het nu gaaf om geconcentreerd aan de start te
staan en weg te knallen bij het horen van een piepje (mja, want een echt
startpistool met patronen gebruiken is veel te duur :P)

Als je op zulk goed ijs staat, dan moet dat ook in de tijden te zien zijn, en
inderdaad, met wat aanloopprobleempjes op de 500 kreeg ik het voor mekaar om de
drie overige afstanden goed te verbeteren.

| Afstand | PR      | Skeuveltoernooi | WR                          |
| ------- | ------- | --------------- | --------------------------- |
| 500m    | 0:58.73 | 1:00.20         | 0:34.03 (Wotherspoon)       |
| 3000m   | 6:39.96 | 6:16.75         | 3:37.28 (Ervik)             |
| 1000m   | 2:04.43 | 2:02.98         | 1:07.00 (Koskela)           |
| 1500m   | 3:14.31 | 3:00.40         | 1:42.32 (Davis & Wennemars) |
 
Per saldo dus ruim 35 seconden verbetering! In de toernooi-uitslag sta ik
weliswaar laatste, maar dat is omdat ik mezelf in de B-categorie had geplaatst
zodat ik een 3000 kon rijden (ook in de C-categorie had ik trouwens niet veel
hoger gekomen).

Op zondag reden we over een uitgestorven A50 naar het noorden, waar we rond 6
uur aankwamen. Luchtbed opblazen, eten koken, eten en dan een rustig avondje
zonder teveel opwindingen buiten toepen en Regenwormen (ja zeg, kennen meer
mensen dat spel!?). We gaan redelijk op tijd slapen, want om half 7 gaat de
wekker alweer.

Om 8 uur staan we dan in het heiligdom van de Nederlandse schaatsliefhebber.
Het ijs ziet er prachtig uit en dan is het nog niet eens verzorgd! Nadat de
Zamboni (jawel, een echte!) het ijs heeft verlaten, is het tijd om het eens te
verkennen... Fijn, geen hobbels, geen geultjes, geen krassen, geen rare harde
stukken halverwege de bocht... Maar ook bijna geen grip! Dat zou ik merken in
de 500, waarvan ik de bochten extra voorzichtig doorreed en dus ook geen PR
wist te rijden.

Later op de ochtend gaan we voor de 3000, een afstand die me een beetje
verrastte, want ik zou weliswaar meedoen aan het allround toernooi, maar wel
als Heren C (500-500-1000-1500) en niet Heren B (500-1000-1500-3000). Maar,
geen probleem, rijd ik die ook eens en het was goed dat ik net twee weken
ervoor ook al een 3 had gereden op het Isistoernooi, want ik had dus een
richttijd!

Nou, die 3000 was lekker knallen op de rechte stukken, heerlijk glijden en lang
afzetten, maar dan kwam toch weer die onvermijdelijke bocht waar de snelheid er
weer uit moest om er niet uit te vliegen. Alles bij elkaar toch 23 seconden
verbeterd en da's zeker niet niks!

Die avond gingen we uit eten bij een pizzaria in Heerenveen, waar ik enorme
buikpijn van kreeg en de eerder zo lekkere pizza in iets minder lekkere vorm
aan de WC moest doneren. Helaas bleef de buikpijn wel een beetje en ik
verwachtte niet dat de 1000 en 1500 iets zouden worden.

En inderdaad, de 1000 ging voor geen meter, voor mijn gevoel slechter dan ik
ooit had gereden. De tijd van 2:02 was ook niks, want m'n PR stond op 2:00...
Toch? Bij thuiskomst bleek die alleen op 2:04 te staan en was dit dus mijn
nieuwe beste race!

De 1500 ging dan weer een stuk beter en dat had alles te maken met de vele
Isisianen die strategisch rond de baan stonden opgesteld om mij (en natuurlijk
ook de andere Isis-rijders) over de streep te brullen. Geweldig! Het waren er
geen 12.000, maar het geeft toch een goed gevoel dat het nooit helemaal stil is
om je heen, ook al versta je geen moer wat ze brullen.

Goed, woensdag is de allerlaatste training van het seizoen en dan mag ik Bob
gaan uitleggen waarom ik het hele seizoen zo langzaam reed als ik nu opeens wel
sneller kan rijden :P
