---
layout: post
title: "Under Byen in Ekko"
categories: [muziek, vimeo]
date: 2008-10-10 23:39:24 +0200
---
Zo, nu het gedoe met de kapotte harde schijf achter de rug is weer eens een gewoon stukkie tekst. Vorige week woensdag zijn we weer eens een bandje gaan bekijken. Ditmaal was het opnieuw een Deense groep, Under Byen (spreek ongeveer uit als 'onne buuen') en dat was zeker leuk.

Met de [Flip](http://theflip.com/) heb ik een filmpje gemaakt:

{% vimeo https://vimeo.com/1880538 %}
