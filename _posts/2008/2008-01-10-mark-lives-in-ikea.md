---
layout: post
title: "Mark lives in IKEA"
categories: [web-stuff]
date: 2008-01-10 09:41:51 +0100
tags: [youtube]
---
Mark Malkoff, die ooit alle (toen) [171 filialen van Starbucks in *Manhattan*](http://www.171starbucks.com/) (dank voor de verbetering, [sherco](http://sherco.livejournal.com/)) heeft bezocht in 1 dag, heeft weer een nieuwe uitdaging.

{% youtube "https://www.youtube.com/watch?v=z3S5s3EITcQ" %}

Terwijl zijn woning groot onderhoud krijgt, zal hij [leven in de IKEA](http://www.marklivesinikea.com/). Aangezien een groot deel van z'n inrichting van IKEA was, zou hij zich best wel eens thuis kunnen voelen. Malkoffs grootste verrassing was misschien wel dat IKEA hem toestemming gaf voor het project. Hij is er afgelopen maandag ingetrokken en blijft tot zaterdag (dat klinkt niet als een week, maar goed).
