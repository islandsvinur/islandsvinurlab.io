---
layout: post
title: "20 Jaar Internet in Nederland"
categories: [web-stuff, vimeo]
date: 2008-11-17 14:07:33 +0100
---
Precies 20 jaar geleden kwamen er voor het eerst Internet-bitjes door de kabelverbinding tussen Amsterdam en Amerika. Het waren ook meteen de eerste bitjes die ooit via het Internet de VS hadden verlaten.

Reden voor een feestje, toch? Zeker nu bekend is geworden dat ruim de helft van de Nederlanders  [niet meer dan twee dagen zonder Internet kan](http://www.depers.nl/opmerkelijk/261952/Eerst-internet-dan-het-toilet.html)

Tot slot nog een filmpje uit de oude doos.

{% vimeo http://vimeo.com/2266474 %}
