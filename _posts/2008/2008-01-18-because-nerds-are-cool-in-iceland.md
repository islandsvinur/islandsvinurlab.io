---
layout: post
title: "...Because Nerds Are Cool In Iceland"
categories: [ijsland]
date: 2008-01-18 11:37:14 +0100
---
Gelezen op [How do you like Iceland?](http://blog.icelandexpress.com/iceland/2008/01/18/because-nerds-are-cool-in-iceland/):
 
> <h2>Because Nerds are Cool in Iceland</h2> A laptop in Reykjavik is like a guitar-case on the continent: it doesn't matter what you're doing with it, just carrying one around increases your chances of getting lucky.
> 
> Imagine using your computer in a pub back home; everybody would look at you like you're barmy... before turning back to the television that's blaring away in the corner, that is. Nearly all bars in Reykjavik are TV-free, thankfully, but what's even better is that being a nerd is actually considered cool in Iceland, and you can surf the net in the pub without anybody thinking any the less of you for it.

Zo heel erg waar, lees nog maar eens wat ik op [9 augustus](http://ijsland.luijten.org/2006/08/09/daar-zit-je-dan/) en [30 oktober](http://ijsland.luijten.org/2006/10/30/aboutblank-een-stage-in-ijsland-in-weblogs/) 2006 op m'n IJslandblog schreef.
