---
layout: post
title: "Das Wunder Von Berlin"
categories: [film]
date: 2008-01-27 03:37:19 +0100
---
Op 27 januari zendt de ZDF de film [Das Wunder von Berlin](http://www.ufa.de/index.php/Produktionen/Detail/id/1012946) uit. Waarom viel de Muur op 9 november 1989? Waarom liep dit niet zo uit de hand als de [demonstratie](http://nl.wikipedia.org/wiki/Tiannanmen-protest) op het [Plein van de Hemelse Vrede](http://nl.wikipedia.org/wiki/Plein_van_de_Hemelse_Vrede) in Beijing?

De [val van de Muur](http://nl.wikipedia.org/wiki/Berlijnse_Muur#Val_van_de_muur_in_1989) verenigde twee landen die al bij elkaar hoorden ("Jetzt wächst zusammen, was zusammen gehört" -- Willy Brandt), maar scheurde in vrede families uiteen. Moeder is overtuigd socialist, de kinderen vertrekken naar de "vrijheid" aan de andere kant en vader blijkt al jaren bij de Stasi te zitten en wordt nu opeens als crimineel bestempeld.

De reden dat ik me voor dit stukje wereldgeschiedenis interesseer komt denk ik vooral doordat het de eerste grote gebeurtenis is die ik me kan herinneren. Voor Tsjernobil was ik nog nét iets te klein en weet ik alleen dat daarna ik niet in de zandbak mocht spelen.

Van de val van de Muur weet ik nog dat we naar de familie in Duitsland gingen: Toen we vertrokken was alles zoals het al jaren was -- en ik vond het eigenlijk ook niet vreemd, maarja, heb je wel een politieke mening als zevenjarige? -- toen we aankwamen kwamen net de beelden op TV van feestende mensen bovenop de muur. Ook de beelden van vakantiegangers die niet meer thuiskwamen en van mensen die de ambassades in vluchtten maakten indruk.

Dus vanavond om kwart over 8 op ZDF, das Wunder von Berlin.
