---
layout: post
title: "Studiereisplanning"
categories: [reizen]
date: 2008-01-30 12:08:56 +0100
---
![]({% link /image/kalmar-logo.png %}) Zo, volgende week donderdag is het zover, dan vertrek ik voor twee weken naar het hoge noorden.

Voor mezelf en ook voor de thuisblijvers heb ik het reisschema in m'n agenda gedumpt en [gepubliceerd](webcal://icalx.com/public/AntiChris/Kalmar.ics) ([suffere HTML versie](http://www.icalx.com/html/AntiChris/month.php?cal=Kalmar&#38;getdate=20080229)).

Grofweg ben ik van 7 tot en met 10 februari in [Trondheim](http://maps.google.com/maps?q=Trondheim), van de 11e tot en met de 16 in [Stockholm](http://maps.google.com/maps?q=Stockholm) en dan nog tot de 22e in [Helsinki](http://maps.google.com/maps?q=Helsinki).
