---
layout: post
title: "Is IJsland echt de beste plaats om te leven?"
categories: [ijsland]
date: 2008-01-11 12:54:24 +0100
---
De Verenigde Naties kwamen afgelopen november met het Human Development Report 2007/2008, waar IJsland als "de beste plaats om te leven" uit kwam.

Het [kunstenaarsduo Wouter Osterholt en Elke Uitentuis](http://www.osterholtuitentuis.nl/) woont in Reykjavík en proberen middels citaten die ze op allerlei plaatsen vinden deze uitspraak te relativeren.

[Een van die citaten](http://www.icelandbestplacetolive.com/?p=26) komt van de [Reykjavík Harbour Watch](http://reykjavikharbor.blogspot.com/2007/11/our-green-footprint.html) die ik al lang volg en inderdaad alleen maar kan bevestigen; het bussysteem is hopeloos.

![]({% link /photos/iceland/public-transit-system-broken.jpg %})

Intussen is "ECS" trouwens overstag gegaan en [heeft na anderhalf jaar een auto gekocht](http://reykjavikharbor.blogspot.com/2008/01/january-shifting.html).
