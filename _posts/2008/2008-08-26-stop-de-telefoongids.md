---
layout: post
title: "Stop De Telefoongids"
categories: [rants]
date: 2008-08-26 09:32:26 +0200
---
![rants]({% link /image/rants.gif %})

> <big>Gebruik jij ook de papieren telefoongids niet?</big>
> 
> Anno 2008 ploft er nog steeds elk jaar een ge-update Telefoongids van zo
> ongeveer een halve kilo tot 1 kilo bij elk huishouden en elk bedrijf in de
> bus, hetzelfde gebeurt met de Gouden Gids.
> 
> Een hele hoop mensen gooien die zware boeken gelijk weer in de oud-papier bak
> of de vuilnisbak omdat ze hem geen seconde gaan inkijken, of hij ligt het
> hele jaar naast de telefoon te verstoffen. Er zijn al jaren de altijd
> up-to-date sites van De Telefoongids en de Gouden Gids zelf, en bijna
> iedereen weet dat. 

Wij hebben al een Nee-Nee-sticker, dus waarom komt dat ding toch nog elk jaar
binnen? Dat komt omdat de bedrijven achter de gidsen zich dus niet aan de
afspraken over huis-aan-huisbladen houden.

Dus, ga naar [StopDeTelefoongids.nl](http://www.stopdetelefoongids.nl/) en
bespaar een paar bomen, want "ook niet-boomknuffelaars vinden het zonde van het
papier..."

**Update oktober 2018:** [Eindelijk zijn we van dat onding af!](https://nos.nl/artikel/2211385-papieren-telefoongids-stopt-na-137-jaar-einde-aan-iconisch-boek.html)
