---
layout: post
title: "Walibi World"
date: 2008-10-20 22:41:23 +0200
tags: [vimeo]
---
Weer eens in ~~Flevohof~~ ~~Walibi Flevo~~ ~~Six Flags Holland~~ [Walibi
World](http://www.rcdb.com/pd266.htm) geweest. Dit keer met Flip.

{% vimeo http://vimeo.com/2019304 %}

In de video: ~~Superman The Ride~~ [Xpress](http://www.rcdb.com/id769.htm,) 
[El Condor](http://www.rcdb.com/id770.htm) en
[Goliath](http://www.rcdb.com/id1565.htm)

Grappig trouwens, een van de eerste posts op dit weblog ging [over een
bezoek aan Six Flags]({% post_url 2004/2004-09-07-sixflags %}).
