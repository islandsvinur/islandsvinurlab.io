---
layout: post
title: "Efterklang in Ekko"
categories: [muziek]
date: 2008-04-14 15:34:49 +0200
---

Zo kom je nog eens ergens! Van [Tutus](http://tutus.hyves.nl/) kreeg ik
vorige week een mailtje doorgestuurd,

> 3×2 TICKETS: EFTERKLANG, 13/4 @ EKKO, UTRECHT (NL) De
> sprookjesachtige muziek van Efterklang brengt iedereen in vervoering. Jij
> kunt ze live zien als je mailt wie de extra violen speelden op "Under Giant
> Trees" naar prijsvraag@kindamuzik.net met als onderwerp Efterklang. ===>
> [http://www.kindamuzik.net/artiest/efterklang](http://www.kindamuzik.net/artiest/efterklang.html)

Ik doe dat gewoon, krijg ik een mailtje van
[Kindamuzik](http://www.kindamuzik.net/) terug...

> Je staat op de gastenlijst met een +1.

Waaat, zo droog? Dus gingen Marly en ik gisteren lekker naar Utrecht voor
[Efterklang]({% post_url 2005/2005-07-06-efterklang-tripper %}}) naar het
gezellig kleine zaaltje genaamd [Ekko](http://www.ekko.nl/) aan de Bemuurde
Weerd, wat naar het blijkt toen ik daar aankwam wel heel dicht bij Frank en
Helie blijkt te zijn... Krijg ik deze
tweet
binnen als reactie op <a
href="http://www.mobypicture.com/index.php?cmd=image&action=full&imgid=20328"
rel="lightbox[efterklang]" title="Efterklang in Ekko, Utrecht">een live naar Twitter geuploade foto</a>.

[![]({% link /image/frankmeeuwsen-ekko.png %})](http://twitter.com/frankmeeuwsen/statuses/788417801)
{: .center}

Ideaal, dat Twitter, maar nee sorry, was dan een beetje laat geworden, gok
ik. Volgende keer beter ;)

![Efterklang]({% link /image/photos/efterklang-by-nan_na_hvass_b-web.jpg %})  
{: .center}

Het was leuk. Erg leuk, knus en gezellig. Oh, en ik heb ook nog een <a
href="http://www.mobypicture.com/index.php?cmd=image&action=full&imgid=20321"
rel="lightbox[efterklang]" title="Peter Broderick in Ekko, Utrecht">onherkenbaar fotootje</a> gemaakt en live geupload van de
support act [Peter Broderick](https://www.peterbroderick.net), van
wiens muziek ik zo onder de indruk was dat ik gelijk z'n CD heb gekocht.
