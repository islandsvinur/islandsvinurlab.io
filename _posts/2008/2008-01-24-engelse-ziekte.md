---
layout: post
title: "Engelse ziekte"
categories: [taal]
date: 2008-01-24 13:13:23 +0100
---
Ohhh, de [Verkiezing van de Spatie van 2007](http://www.spatiegebruik.nl/despatievan2007.html)! Met juweeltjes als 'systeem fout geconstateerd', 'software matig' en 'naakt model tekenen'.

Zie je hier niets bijzonders aan, dan heb je waarschijnlijk de mysterieuze, maar erg besmettelijke [Engelse ziekte](http://nl.wikipedia.org/wiki/Engelse_ziekte_%28taal%29) te pakken.

Het gebruik van dit [Deppenleerzeichen](http://de.wikipedia.org/wiki/Deppenleerzeichen) zoals onze oosterburen hetzelfde verschijnsel noemen slaat door de anglicalisering van onze taal wild om zich heen en levert naast een hoop vermakelijke taalblunders ook een hoop onbegrip op.

Want is het nou een zware doos (al dan niet) gevuld met spullen of is het een stevige doos, speciaal gemaakt voor zware spullen?

<a href="http://www.spatiegebruik.nl/popup.php?id=1620"><img src="http://www.spatiegebruik.nl/imagedb/groot/img01620.jpg" alt="" /></a>
