---
layout: post
title: "Vierde IIW"
categories: [sport]
date: 2008-01-31 00:09:21 +0100
---
Vanavond stond alweer de 4<sup>e</sup> IIW (Interne Isiswedstrijd) op het
programma. Na een marathon die ik aan me heb voorbij laten gaan en
een [rampzalige 1500 meter](http://www.essvisis.nl/nieuw/wedstrijden/wedstrijden.php?item=tijdendatabase&#38;wedid=1057) 
(op m'n toen splinternieuwe schaatsen, dus daar kwam het wel door), was het weer
tijd voor een 500/1000m combinatie.

PRs werden aan gort gereden en het was ook mooi om te zien dat er weer een paar
beginners meer meededen. De wedstrijden beginnen echt van de grond te komen.

M'n richttijd voor de 500 was om onder 1 minuut te gaan, dat is helaas niet
gelukt, maar de flinke verbetering op de 1000 maakte een boel goed, en nou ben
ik niet meer twee keer zo traag als de snelste ter wereld :P

| Afstand | PR      | 4e IIW  | [WR]                         |
| ------- | ------- | ------- | ---------------------------- |
| 500 m   | 1:02.83 | 1:01.73 | 0:34.03 (Jeremy Wotherspoon) |
| 1000m   | 2:15.06 | 2:04.43 | 1:07.00 (Pekka Koskela)      |

Beide tijden staan dus nu op scherp om de volgende keer hopelijk wel onder de 1
resp. 2 minuten te duiken :)

[WR]: http://nl.wikipedia.org/wiki/Lijst_van_schaatsrecords_%28langebaan%29
