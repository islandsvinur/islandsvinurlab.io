---
title: Papa, wat staat er in China op de muur?
layout: post
tags: [efteling]
lead_image: /image/2020-09-21-carnaval-festival-china.jpg
---

Een tijdje terug ben ik tijdens een studiedag met de jongens naar
de [Efteling](https://www.efteling.com) geweest. Nu ben ik moeilijke vragen over
de Efteling inmiddels wel gewend en houd
ik [Eftepedia](https://www.eftepedia.nl) altijd binnen handbereik voor de echt
lastige kwesties. Vandaag echter kon ik de vraag van Jasper niet beantwoorden en
wist ook de compleetste encyclopedie over de Efteling mij geen bevredigend
antwoord te geven!

> Papa, wat staat er in China op de muur?

Uitstekende vraag, en met wat meer context snap je dat het hem gaat om de
betekenis van de karakters aan de achterwand van het China-deel van Carnaval
Festival

![無任歡迎 in Carnaval Festival in de Efteling]({% link image/2020-09-21-carnaval-festival-china.jpg %}){: .width-50}
{: .center}

Nu is mijn kennis van Chinese talen beperkt, dus dit wordt een uitdaging.
[Eftepedia meldt alleen dat Google Translate dit vertaalt met "niet welkom"](https://www.eftepedia.nl/lemma/Carnaval_Festival#China), 
wat ik me niet kan voorstellen dat er staat.

Eerst ga ik de karakters maar eens digitaliseren, want dat zoekt daarna
handiger. Veel weet ik er niet van, maar ik weet dat karakters zijn opgebouwd
uit een of meerdere delen, radicalen genaamd. Ik
vond [een site](https://www.yellowbridge.com/chinese/adv-character-dictionary.php)
waar je op basis van die deelkarakters kan zoeken tussen de duizenden karakters
in het [Chinees schrift (hanzi)](https://nl.wikipedia.org/wiki/Chinees_schrift).

## 無任歡迎?
{: style="text-align: center"}

Zo kom ik op 無任歡迎, waarbij ik een hele tijd bezig ben geweest het laatste
karakter te reproduceren en uiteindelijk dus op 迎 uitkwam. Dat mist echter een
"haakje" links. Het lijkt erop dat ze bij de Efteling een foutje hebben gemaakt,
want die specifieke combinatie van lijnen bestaat helemaal niet. Wat ook opvalt
is dat 無 en 歡 traditioneel Chinese karakters zijn, terwijl 任 en 迎 vereenvoudigde
karakters zijn. Normaalgesproken worden die niet gemengd gebruikt; de Chinese
Volksrepubliek introduceerde in de jaren 50 van de 20e eeuw de vereenvoudigde
Chinese karakters, maar andere gebruikers van het Chinees bleven de traditionele
karakters gebruiken. De keuze is daarmee dus politiek gekleurd. Het kan zijn dat
de Efteling heeft willen voorkomen voor een kant te kiezen en daarom maar beide
door elkaar gebruikt.

De vier karakters invoeren in Google Translate levert inderdaad "niet welkom"
op, maar dat kon ik nog steeds niet geloven. De losse karakters betekenen 
"niet" (無), "aanstellen" (任), "vreugdevol" (歡), "welkom" (迎).

## "niet aanstellen vreugdevol welkom"
{: style="text-align: center"}

Juist. Het is minder negatief dan dat we helemaal niet welkom zijn, maar het
slaat nog steeds nergens op. Tijd om een andere tool te zoeken, want Google
Translate helpt niet echt.

[Wiktionary](https://en.wiktionary.org) is het woordenboek-project van
Wikipedia, en biedt de volgende vertalingen:
"not, nothing, nil" ([無](https://en.wiktionary.org/wiki/無)),
"duty, responsibility" ([任](https://en.wiktionary.org/wiki/任)),
"joy, delight" ([歡](https://en.wiktionary.org/wiki/歡)),
"welcome" ([迎](https://en.wiktionary.org/wiki/迎)). Ik begin te vermoeden dat we
met samengestelde woorden te maken hebben, en inderdaad:
"extremely, immensely" ([無任](https://en.wiktionary.org/wiki/無任)),
"welcome" ([歡迎](https://en.wiktionary.org/wiki/歡迎)).

Gelukkig maar, we zijn "extreem welkom"!

## 無任歡迎! wúrèn huānyíng!
{: style="text-align: center"}

Na die hele zoektocht ontdek ik dat de site waar ik de karakters had opgezocht
óók een woordenboek heeft. Daar kan je
gewoon [de karakters invullen](https://www.yellowbridge.com/chinese/dictionary.php?word=無任歡迎)
en krijg je de samengestelde woorden op een presenteerblaadje.

Zo, nu ga ik Eftepedia een mailtje sturen zodat ze hun Carnaval Festival lemma
bij kunnen werken.
