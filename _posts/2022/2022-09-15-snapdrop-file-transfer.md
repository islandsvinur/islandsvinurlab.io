---
title: "Snapdrop: file transfers van iPhone naar Linux"
layout: post
tags: [ios, linux]
lead_image: /image/unsplash/lubo-minar-JCUcCpptkz8-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Lubo Minar
        username: bubo
---

Sinds een tijdje gebruik ik op m'n werk geen MacBook meer, maar heb ik een Linux laptop (uiteraard met Debian erop, dat
is al sinds 1998 "mijn" distributie). Werkt super fijn, behalve als ik even snel een foto van bijvoorbeeld een
whiteboard vanaf m'n iPhone naar dat ding wil krijgen. Alles waar de Linux-wereld een oplossing voor heeft, werkt niet
in de Apple-wereld en andersom.

Dan zeg je misschien, "gebruik dan Dropbox ofzo", maar ik vind het een beetje onzinnig dat ik eerst mijn foto moet gaan
uploaden naar een server in de VS, om het dan vervolgens weer te downloaden naar de computer die er nog geen halve meter
vanaf staat. In tijden van energiecrisis is dat niet echt de way-to-go. Om nog maar te zwijgen over alle privacy-issues.

Waarom dan niet een SFTP-verbinding maken met je laptop en het zo uploaden? Kan, en dat zou best wel eens kunnen werken,
maar echt gebruiksvriendelijk is dat ook niet. Ik wil natuurlijk ook bestanden van iemand anders kunnen ontvangen zonder
veel gedoe.

![Snapdrop]({% link image/2022-09-15-snapdrop-file-transfer.png %}){: .no-box-shadow}

En toen vond ik Snapdrop; een "simpele" webapplicatie die lokale file transfers tussen
Android, iPhone, Windows, Mac _en_ Linux gewoon makkelijk maakt. Het beste is dat het bijna helemaal lokaal draait; er
is alleen een WebRTC verbinding met een server nodig zodat apparaten elkaar kunnen vinden. Nog beter, omdat de code
op GitHub staat zou je, als je dat zou willen, het helemaal self-hosted maken!

**Update februari 2025**: Snapdrop is "overgenomen" door LimeWire, tegenwoordig een NFT scam ongerelateerd aan het 
originele illegale-muziek-deel-platform. Ik heb alle links naar Snapdrop daarom verwijderd.

De ontwikkeling van Snapdrop lag al bijna een jaar stil en er was ook al een fork actief 
genaamd [PairDrop](https://pairdrop.net/). De code hiervan staat ook 
op [GitHub](https://github.com/schlagmichdoch/pairdrop).
