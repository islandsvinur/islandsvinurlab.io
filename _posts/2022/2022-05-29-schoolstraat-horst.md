---
title: Schoolstraat Horst
layout: post
lead_image: /files/2022-05-29-schoolstraat-horst/2022-06-05-IMG_6361.jpeg
tags: [horst]
---

Ruim twintig jaar geleden schreef wielerjournalist Frans van Schoonderwalt in _de Volkskrant_ een column over de
Schoolstraten van Nederland. Op 13 januari 2001 was Horst aan de beurt. Ik kwam in september 2016 bij toeval terecht op
de pagina
van [de column op de site van de Volkskrant](https://www.volkskrant.nl/nieuws-achtergrond/schoolstraat~b18e48c0/) en heb
er destijds [een PDF van gemaakt]({% link
files/2022-05-29-schoolstraat-horst/2001-01-13-schoolstraat-horst-archief-de-volkskrant.pdf %}). Dat was maar goed ook,
want inmiddels zijn we zes jaar verder en heeft de krant zowat alles achter de betaalmuur gezet.

Maar goed, nu zes jaar later moest ik er opeens weer aan denken en bedacht me dat het misschien leuk is om het verhaal
van toen te vergelijken met nu; 21 jaar later.

In 2001 bestond de Westsingel nog niet, die is rond 2008 aangelegd. Hierdoor kwam <q>tuin- dier- en steencentrum
Severens op nummer 49</q> direct aan die nieuwe rondweg te liggen. Severens heeft zich inmiddels toegelegd op
bedrijfsbeveiliging en ze zijn inmiddels vertrokken naar Melderslo. Nummer 49 is zelfs niet langer in gebruik; van 47b
binnen de Westsingel gaat het aan de overkant direct verder met 51. Een recentelijk afgesplitst perceel maakt een nieuwe
49 echter weer mogelijk...

<q>Een wetering kruist ter hoogte van nummer 49 de straat.</q> Precies bij die wetering loopt nu de Westsingel. Toen we
in 2013 in Horst kwamen wonen lag hij binnen de singel, kort daarna is deze buiten de singel verlegd, precies over het
oude perceel van nummer 49. <q>Een bordje met het cijfer 5, een pijl en een fiets verwijst naar Peel en Maas.</q>
Blijkbaar is het fietsnetwerk intussen opnieuw opgebouwd, want tegenwoordig is het nummer 87 waar de pijl heen wijst en
die maakt ook eens een klein ommetje door de nieuwe Grad Roosenstraat en Van den Bekeromstraat.

![Hier was nummer 49. Links is nummer 47, rechts nummer 51]({% link
files/2022-05-29-schoolstraat-horst/2022-06-05-144908_292_pano.jpeg %}){: .lightbox}  
_Hier was nummer 49. Links is nummer 47, rechts nummer 51._
{: .center}

<q>Tegenover het tuincentrum staan rijen kerstbomen in het gelid</q>, maar twintig kerstmissen hebben duidelijk hun tol
geeist, want daar is niets meer van te zien.

![De overkant van nummer 49]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-153656_787_pano.jpeg %}){: .lightbox}  
_De overkant van nummer 49_
{: .center}

<q>Van hem is vast het stenen paartje onder een doorzichtig paraplu afkomstig, dat de tuin van nummer 53 opluistert, en
vermoedelijk ook de twee stenen ganzen in diezelfde tuin en de kruik waaruit water in een vijver vloeit.</q> Na ruim
twintig jaar staat het stenen paartje -- met nieuwe blauwe paraplu -- nog steeds in de tuin van nummer 53! De vijver is
inmiddels gedempt, inclusief de stenen ganzen en de kruik en er prijkt een nieuw natuurstenen fontein-ornament. <q>En
het stenen paartje onder een stenen paraplu in de voortuin van nummer 30 lijkt ook de signatuur van Severens (<q>uw
tuinvakman</q>) te dragen.</q> Die staan er inmiddels niet meer, maar ik zag op Google Streetview dat ze er in 2008 nog
stonden.

![]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-144652_638.jpeg %}){: .width-50 .lightbox}  
_Het stenen paartje onder een blauwe paraplu dat de tuin van nummer 53 opluistert_
{: .center}

<q><q>Babouche gasten, willen jullie je fiets zoveel mogelijk aan de overkant zetten, dit in verband met de beschadiging
van de muur</q>, vraagt nummer 11. Babouche is een rockcafé op nummer 9 met twee deuren. Op de ene deur staat: andere
deur.</q> Inmiddels zijn beide deuren voorgoed
gesloten. [Eind 2018 zijn de uitbaters er na dertig jaar mee gestopt.](https://www.limburger.nl/cnt/dmf20180120_00054414/)
Een opvolger is er nog niet.

![Babouche]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-145826_449.jpeg %}){: .width-50 .lightbox}  
_Babouche_
{: .center}

<q>Ik ben in het Land van de Champignons. Eenmaal buiten de dorpskom -- want dit is een Schoolstraat die de natuur
opzoekt -- stuit ik op de ene kwekerij na de andere. Horst Champignoncentrum.</q> Nummer 49 waar we al eerder waren, was
in 2001 al buiten de bebouwde kom. Met de aanleg van de Westsingel kwam de grens precies daar te liggen, en toen
uitbreidingsplan De Afhang in verre staat van uitvoering was, is het hele gebied opgenomen in de bebouwde kom. De weg
ging van 80 km/h naar 50 km/h en inmiddels 30 km/h. De Schoolstraat zoekt inmiddels de natuur niet meer op en ook de
champignonkwekerijen hebben dit gebied al enige tijd verlaten.

<q>Verboden met vuur te spelen, waarschuwen de gaskastjes in de tuinen van de nummers 51 en 55. Pas op schrikdraad,
hangt
aan het weiland ter hoogte van nummer 53.</q> De gaskastjes zijn er nog, inclusief vuur-verbod, maar de vraag is hoe
lang
nog, nu we met z'n allen van het gas moeten. Een weiland is er hier allang niet meer; hier is nu het kruispunt naar
alweer de volgende nieuwbouwwijk, precies ingepast tussen nummers 53 en 42.

![Overzijde Schoolstraat 55a in september 2013]({% link
files/2022-05-29-schoolstraat-horst/2013-09-04-schoolstraat-tegenover-55a.jpeg %}){: .width-50 .lightbox}  
_Overzijde Schoolstraat 55a in september 2013_
{: .center}

![Overzijde Schoolstraat 55a in juni 2022]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-144322_378_pano.jpeg
%}){: .lightbox}  
_Overzijde Schoolstraat 55a in juni 2022_
{: .center}

Er is er nog één enkele kweker te vinden iets verderop aan de Afhangweg. Weggestopt achter de huizen, want wat niet
veranderd is: <q>de kwekers lopen in tegenstelling tot Severens niet erg met zichzelf te koop. Een minuscuul
tekeningetje
van drie champignons (nummer 55a), een dito naamstickertje (nummer 53), volstrekte anonimiteit (nummer 90). Een
vrachtwagen achter het ABC-hekwerk verraadt het eveneens anonieme nummer 47.</q>

![Schoolstraat 90 in juni 2013]({% link files/2022-05-29-schoolstraat-horst/2013-06-08-schoolstraat-90.jpeg %}){:
.width-50 .lightbox}  
_Schoolstraat 90 in juni 2013_
{: .center}

Een interessante historie heeft nummer 1. <q>Voor verse mosselen en Ware Witte van het vat beveelt café-restaurant
Soestdijk op nummer 1 zich aan.</q> In de loop der jaren zijn hier verschillende uitbaters geweest, met als toppunt het
[Italiaanse restaurant "La Vita" wat als witwas-operatie van de Italiaanse maffia](https://www.misdaadjournalist.nl/2018/12/06/operatie-kippetje-een-klap-of-een-tikje-voor-de-maffia/)
zou functioneren. Na de inval was het restaurant "om technische redenen" lange tijd gesloten. Nadat de rust was
wedergekeerd opende er muziek-café Anja ván de smid. De Ware Witte heeft plaats gemaakt voor het gerstenat uit Enschede.

![Anja ván de smid]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-150026_357.jpeg %}){: .width-50 .lightbox}  
_Anja ván de smid_
{: .center}

<q>Bij 't Schippertje op nummer 6 zijn kindervestjes van fl 87,50 te koop voor fl 43,75 en een set breibollen van Beach
-- in rood of zalmroze -- kost slechts zestien gulden.</q> Een echte specialist zoals deze vind je nog maar weinig, ook
buiten Horst. Ze zijn er nog steeds en drie bollen "Raw linen" kosten nu in de aanbieding € 20,00.

!['t Schippertje]({% link files/2022-05-29-schoolstraat-horst/2016-07-08-schippertje.jpeg %}){: .width-50 .lightbox}  
_Bankje voor 't Schippertje in juli 2016_
{: .center}

<q>Ook de speksteen is in de aanbieding, bij De Triangel op nummer 5: fl 3,95 de kilo.</q> Op nummer 5 verkopen ze
inmiddels
liever verzekeringen dan speksteen; en die zijn vast een stuk duurder per kilo papierwerk!

<q>Vanaf het terras zijn schuin aan de overkant de uitgezaagde figuren te zien die zich aan het hekwerk van de school op
nummer 8 vastklampen. Een meisje in een tuinbroek en twee voetballende jongens.</q> Kinderen buiten laten spelen is een
inmiddels verloren strijd. Tegenwoordig maakt OBS de Weisterbeek zich als gezonde school sterk voor een rookvrije
generatie.

![OBS De Weisterbeek gaat voor een rookvrije generatie]({% link
files/2022-05-29-schoolstraat-horst/2022-06-05-145855_987.jpeg %}){: .width-50 .lightbox}  
_OBS De Weisterbeek gaat voor een rookvrije generatie_
{: .center}

Sowieso zal het aanzicht van dit stuk Schoolstraat de komende jaren flink veranderen, want de school gaat tegen de
vlakte en met de herbouw krijgt dit hele gebied meteen een facelift.

![OBS De Weisterbeek]({% link files/2022-05-29-schoolstraat-horst/2022-06-05-145810_056.jpeg %}){: .width-50 .lightbox}  
_Binnen een paar jaar zal hier een compleet nieuw schoolgebouw verrijzen_
{: .center}

### Naschrift

Tijdens het maken van deze post kwam ik erachter dat Wim "Horst Sweet Horst" Moorman al in 2011 een
["Schoolstraat Revisited"](https://horstsweethorst.blogspot.com/2011/01/intermezzo-de-schoolstraat-revisited.html) heeft
geschreven. Erg leuk! Hij heeft zelfs ook een foto van het originele krantenartikel erbij geplaatst.
