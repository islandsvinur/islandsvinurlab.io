---
layout: post
title: Rosetta Code
lead_image: /image/unsplash/matteo-vistocco-KOUvMTHK64I-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Matteo Vistocco
        username: mrsunflower94
---

Elk jaar een nieuwe programmeertaal leren. Het is iets wat ik de afgelopen jaren met wisselend succes heb geprobeerd.
Het is leerzaam om inzichten die taalontwerpers hadden voor de ene taal te vertalen naar een andere. Het nadeel is dat
documentatie voor programmeertalen geschreven wordt voor een breed publiek, dus je moet vaak eerst door een hoop
basismateriaal heen lezen voordat je bij "the good stuff" komt. Dat is jammer, want als je een andere natuurlijke taal
leert, hoef je (meestal) ook niet eerst het alfabet en het concept van "woorden" opnieuw te leren.

Nu werd ik deze week gewezen op [Rosetta Code](https://rosettacode.org/), waar complexere problemen en constructies
worden gedeeld voor veel verschillende programmeertalen, als een
soort [Steen van Rosetta](https://nl.wikipedia.org/wiki/Steen_van_Rosetta) voor de informatica dus. Ook handig als je
bijvoorbeeld even een [CRC-32](https://rosettacode.org/wiki/CRC-32) wil berekenen zonder de
volledige [wiskunde op Wikipedia](https://en.wikipedia.org/wiki/Cyclic_redundancy_check) te hoeven lezen en doorgronden.
