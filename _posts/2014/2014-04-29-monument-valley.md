---
title: Monument Valley
layout: post
lead_image: /photos/2014-04-29-monument-valley/IMG_7750.jpeg
---
Ken je het spelletje Monument Valley? Bij de makers -- **[ustwo](https://www.ustwogames.co.uk)** -- heb ik de komende
drie dagen cursus, erg leuk kantoor!

En als je dan toch een paar dagen in Londen bent, dan kan je er ook maar een beetje rondkijken, toch?

![Trafalgar Square]({% link photos/2014-04-29-monument-valley/2014-04-30 18.45.38.jpeg %}){: .lightbox}  
_Loopt een [kip](https://en.wikipedia.org/wiki/Hahn/Cock) over het [plein](https://nl.wikipedia.org/wiki/Trafalgar_Square), "tok"_
{: .center}

![Tower Bridge]({% link photos/2014-04-29-monument-valley/IMG_7726.jpeg %}){: .lightbox}  
_[Tower Bridge](https://nl.wikipedia.org/wiki/Tower_Bridge)_
{: .center}

![Tower of London]({% link photos/2014-04-29-monument-valley/IMG_7728.jpeg %}){: .lightbox}  
_[Tower of London](https://nl.wikipedia.org/wiki/Tower_of_London_(gebouw))_
{: .center}

![20 Fenchurch Street]({% link photos/2014-04-29-monument-valley/IMG_7730.jpeg %}){: .lightbox}  
_[20 Fenchurch Street](https://en.wikipedia.org/wiki/20_Fenchurch_Street) met [beschermende doeken tegen het zonnelens-probleem](https://en.wikipedia.org/wiki/20_Fenchurch_Street#Solar_glare_problem)_
{: .center}

![The Shard]({% link photos/2014-04-29-monument-valley/IMG_7731.jpeg %}){: .lightbox}  
_[The Shard](https://nl.wikipedia.org/wiki/The_Shard) - 72 verdiepingen, 310 meter hoog_
{: .center}

![The Gherkin]({% link photos/2014-04-29-monument-valley/IMG_7733.jpeg %}){: .lightbox}  
_[The Gherkin](https://en.wikipedia.org/wiki/30_St_Mary_Axe), past goed bij de [Cucumber-cursus](https://cucumber.io) die ik hier volg_
{: .center}

![Leadenhall Building]({% link photos/2014-04-29-monument-valley/IMG_7734.jpeg %}){: .lightbox}  
_[Leadenhall Building](https://en.wikipedia.org/wiki/122_Leadenhall_Street)_
{: .center}

![Elizabeth Tower, a.k.a. "Big Ben"]({% link photos/2014-04-29-monument-valley/IMG_7735.jpeg %}){: .lightbox}  
_[Elizabeth Tower, "Big Ben"](https://nl.wikipedia.org/wiki/Big_Ben)_
{: .center}

![Big Ben close up]({% link photos/2014-04-29-monument-valley/IMG_7736.jpeg %}){: .lightbox}  
_Big Ben close up_
{: .center}

![Palace of Westminster]({% link photos/2014-04-29-monument-valley/IMG_7744.jpeg %}){: .lightbox}  
_[Palace of Westminster](https://nl.wikipedia.org/wiki/Palace_of_Westminster)_
{: .center}

![London Eye]({% link photos/2014-04-29-monument-valley/IMG_7750.jpeg %}){: .lightbox}  
_[London Eye](https://nl.wikipedia.org/wiki/London_Eye) en de [Theems](https://nl.wikipedia.org/wiki/Theems)_
{: .center}

<p class="center">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1241.1641109277587!2d-0.0775796!3d51.5255395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761cb0c877dff1%3A0x4b063d67310deeb0!2sustwo%20London!5e0!3m2!1sen!2snl!4v1674133981614!5m2!1sen!2snl" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</p>
