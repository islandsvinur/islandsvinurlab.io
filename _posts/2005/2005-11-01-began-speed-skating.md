---
layout: post
title: "Began Speed Skating"
categories: [sport]
date: 2005-11-01 21:13:43 +0100
---
![]({% link /image/speed-skates.jpg %}) Long ago (four years to be exact) Breda got its own ice rink and I was determined to go there often and instead of renting skates everytime I decided to buy my own... Well, turned out to be a wrong choice, because I only went two or three times that year, the next year also a few times and then they just laid about in the box. Luckily, good skates don't rot away, so I can use them now and they still fit (I didn't really grow that much in the last few years)!

Two weeks ago Tuesday (that was uhm... the 18th) I attended the open training of [E.S.S.V. Isis](http://www.essvisis.nl/), the students skate association of Eindhoven (especially students of TU/e and Fontys). I found it so much fun to stand on the irons again since last time that I decided to join Isis almost right away. I attended two more trainings on 25 and 27 October and today was my official first training as a member of Isis!

So far, I qualify for practicing sport everytime according to the [Admar definition](http://luon.net/~admar/journal/Game%21%3DSports%21.html):
 
> Sports is about pain. Physical pain. About suffering.
 
I qualify because I managed to fall on all of the three trainings. Somehow, in skating, the place to fall on with your full weight is not only the ice, but it is also on your knee. Another place just is impossible to hurt, except maybe for your thighs when you fall in a very strange way. Oh, and [Erben Wennemars showed](http://archief.sportcafe.nl/goudhaantje/g_69.htm) that you can also hurt your shoulder very badly, but only at very high speeds.
