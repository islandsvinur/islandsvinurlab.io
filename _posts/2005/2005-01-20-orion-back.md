---
layout: post
title: "Orion is back"
categories: [admin]
date: 2005-01-20 19:05:53 +0100
---
All right, everything is nice and shiny again. Orion, my "personal" server got a reinstall and a backup restore right after that. I hope it will be a bit more stable now, since it got uptimes of about... 3 days at max.

So, my journal and website are back from hibernation. I don't suppose you missed them :-)
