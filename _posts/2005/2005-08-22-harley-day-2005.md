---
layout: post
title: "Harley Day 2005"
date: 2005-08-22 12:32:47 +0200
---
Nice weather, "some" (i.e. around 8.000) nice bikes, but not so nice to take pictures. Why not? Downtown was full,
completely packed. About 75.000 people thought of going to watch and taking 50 pictures of the same pose but of a
different model isn't really interesting to watch. I
nevertheless took some and put them in my Luijten.org dump-repository.

Ah, I guess there are some alright, but not that great.

![Indian]({% link photos/2005-08-22-harley-day/IMG_0273.JPG %}){: .width-50 .lightbox}  
_Indian_
{: .center}

![Reflections]({% link photos/2005-08-22-harley-day/IMG_0275.JPG %}){: .width-50 .lightbox}  
_Reflections_
{: .center}

![Reflections II]({% link photos/2005-08-22-harley-day/IMG_0276.JPG %}){: .width-50 .lightbox}  
_Reflections II_
{: .center}

This one from the 2002 edition actually was better, but I couldn't find a nice set of bikes so nicely arranged this year, it was a mess.

![3 Heritage Softtails]({% link photos/2005-08-22-harley-day/dscf2521.jpg %}){: .width-50 .lightbox}  
_3 Heritage Softails_
{: .center}
