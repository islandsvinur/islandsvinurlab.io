---
layout: post
title: "Site update"
categories: [ admin ]
date: 2005-08-23 19:39:59 +0200
---

![]({% link /image/screenshot-site-2001.png %}){: .left} My old black and gray t-shirt look website is gone. I moved the
journal to the root of the site, building the rest around it and am now working on creating a photogallery entry type
for [Hobix](http://hobix.com/), my blog-software. [The old site](http://christian.luijten.org/) will still be available
on another location for a while.

In my view, it was a good design, simple yet attractive. But it didn't fit with the needs of the journal, which became
an increasingly bigger part of the site. Actually it was the only part which got updates.

So, like I said, I'm working on a photogallery entry type. The current status is that I have one single page with all
thumbnails on it, accompanied by the image description. A click on the thumbnail opens a new window with the larger
version. This is of course absolutely undesirable and I really have better ideas, but hey, it is only the first preview
not-even-release. The final version should have automatic thumbnail generation, some navigational facilities (maybe
using AJAX?) and some other cool things I don't came up with yet (like EXIF information).

You can already take a look at the [Berlin 2001](http://luijten.org/page/photos/berlin.html) gallery.
