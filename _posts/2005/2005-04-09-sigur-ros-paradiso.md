---
layout: post
title: "Sigur Rós in Paradiso!"
categories: [muziek]
date: 2005-04-09 20:38:03 +0200
---
A nice surprise yesterday when I came on the [Sigur Rós](http://www.sigur-ros.co.uk/) website. They come to the Netherlands on their 2005 tour! 13 July is the day, Paradiso Amsterdam the place. I'll be there, who'll be joining me?
