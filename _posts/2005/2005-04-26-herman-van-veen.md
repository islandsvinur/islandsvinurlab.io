---
layout: post
title: "Herman van Veen"
categories: [muziek]
date: 2005-04-26 19:36:43 +0200
---
![]({% link /image/Herman-van-Veen.png %}) It has been a while since I wrote something here. Yesterday I was surprised with a visit to [Herman van Veen](http://www.hermanvanveen.nl/), a Dutch singer and one of the creators of [Alfred Jodocus Kwak](http://www.jodokus.nl/). The school where my dad teaches is merged this year with another one and one of the festivities was this special gig of Herman van Veen for every employee with their partner.

It was a nice evening, Herman is doing his 60th birthday tour this year together with band; Erik van der Wurff, Edith Leerkes and Wieke Garcia and two more who's name I couldn't make out. He's doing old songs and new ones, the new ones being partly political, partly emotional. He is rethinking his life, thinking back about his mother, about his daughter as a baby, but also letting Edith sing a funny song about what she would have done should she'd be a man.

I won't try to translate some lines into English, since he can do it very much better than I.

> Maar mocht 't nodig zijn, ik haal je uit de diepste  
> de diepste kraters van m'n hart, m'n lijf, m'n liefde.  
> Dat je me één keer vasthoudt heb ik liever  
> dan heel de wereld bij mekaar.
