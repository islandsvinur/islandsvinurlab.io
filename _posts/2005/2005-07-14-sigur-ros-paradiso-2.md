---
layout: post
title: "Sigur Rós in Paradiso!"
categories: [muziek]
date: 2005-07-14 09:55:31 +0200
---
Yesterday, it was 13 July and like I already [posted earlier]({% post_url 2005/2005-04-09-sigur-ros-paradiso %}), Sigur Rós was performing in Paradiso Amsterdam.

![]({% link /image/ticket/sigur-ros-20050713.jpg %})  
{: .center}

Around three I went on my way to our nation's capital, arriving there by half past four. The weather was nice and the streets were packed with people, so I decided to join them and take a walk through the city. I went from the central station over Damrak to Dam (where I completely forgot to look for the location of the [new Apple Store](http://www.appleinsider.com/article.php?id=967) in the center of Amsterdam), then through the Kalverstraat to Leidseplein. There I called Esther to join them (her, her friend Erwin and his (Icelandic) girlfriend Sola) for dinner, they were in the Sarphatipark, very close to the famous Albert Cuyp market.

Getting a bit thirsty I tried to find a supermarket, but somehow I didn't manage to find one until I almost gave up looking for it... When I got in the park, it was already almost time to get us something to eat. That is not a very hard task in Oud-Zuid which is filled with little restaurants where you can have dinner for only a few euros. The hardest part is to decide which restaurant to go to.

At dinner, some dirty Icelandic words were exchanged and I learned a lot about them ;-). Especially what you shouldn't say too loud in Iceland! 'Tot straks' for instance, being just plain Dutch for 'see you later', is pronounced much like the Icelandic 'Tott strax' and that means you want a blowjob right now. Must feel strange being an Icelander in the Netherlands.

When we were finished, it was already time to go to Paradiso, the doors would open at seven and we were already late. Outside Paradiso there was a huge line, which was very special apparently because it drew the attention of many Amsterdammers. The funny thing is of course that no one who asks who we are all waiting for actually knows Sigur Rós... One woman even thought they were from Spain, ha!

Okay, so we went inside, after we had to pay another € 2,50 for a Paradiso membership card. Upstairs we went to get a good view on the stage, plus that there are seats on the balconies! After a beer, the lights went down and Amina came on stage. They have played with Sigur Rós a lot and as would come clear during the performance, will be playing with them for some time in the future.

Amina is an Icelandic string quartet, playing virtually every instrument with strings, acoustic as well as electric and their sound fits perfectly with Sigur Rós as well as with Efterklang. After playing for about an hour, they left the stage and a curtain closed.

About fifteen minutes later, the silhouettes of Sigur Rós became visible on the (see-through) curtains and they began playing their first song, which is a new one ('Introduction' followed by 'Glósóli') from their new album 'Takk...' (thank you) to be released in September. No one knew this of course, but it was received very well by the audience (and also by me of course). After this song, the curtains opened and Jónsi began a haunting Ný Batterí intro, followed by the rest of the concert :-).

 The setlist:
   	
* Introduction 
* Glósóli 
* Ný Batterí 
* Svefn-g-Englar 
* Sæglópur 
* Sé Lest 
* Mílanó 
* Gong 
* Andvari 
* Vaka (Untitled 1)
* Viðrar Vel til Loftárása 
* Hafssól
* Popplagið (Untitled 8)
 	
It truly was a great concert and I cannot really find the words for it, not in Dutch, let alone in English. I guess if you like their music, you really should try to catch a gig.

On the way back in the train to Eindhoven, I was not quite alone for [some other rockband](http://www.u2.com/) had a concert in Amsterdam too, only they were with a few more people.

I would like to send back a big 'Takk fyrir' to the guys of Sigur Rós and hope to see you next time!

**Update**:

The Dutch magazine OOR has [an article about the concert,](http://www.oor.nl/deruit_concertverslagen_details.asp?id=137) describing what I couldn't.

**Update 2**:

Added the setlist and a [Torrent file of nice FLACs](http://www.drrnwbb.com/bt/details.php?id=29&amp;hit=1) of the concert is available.
