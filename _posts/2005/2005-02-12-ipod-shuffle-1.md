---
layout: post
title: "iPod shuffle"
categories: [apple]
date: 2005-02-12 21:01:38 +0100
---
I completely forgot I promised to write something about my iPod shuffle. So here you have it:

Many words have been said now about the shuffle by many users and I just can't add much to that. It sounds all right, the electronic sounds of [Sigur Rós](http://www.sigur-ros.co.uk/) tend to clip (can also be an encoding problem, but I don't hear them that clear on my stereo) and switches slow between songs (about a second silence between each track which really is too ong). It might also give some audible feedback when pushing the buttons. Here are some interesting links about iPod shuffle.

* [Apple Talk on Gathering of Tweakers](http://gathering.tweakers.net/forum/list_messages/996841) (sorry, in Dutch) 
* [One of the first iPod shuffle covers](http://forums.ipodlounge.com/showthread.php?threadid=74274&#38;perpage=15&#38;pagenumber=1) and absolutely beautiful. The guy seems to got a lot of success by just putting some nice pics online :-)
* [A funny mod,](http://www.bootykika.com/images/shuffle_frisk_large.jpg) probably a weak pun on [Apple's comparison](http://images.apple.com/nl/ipodshuffle/images/indexwithgum20050111.jpg)

Okay, so that is what they say and do with it. I had my own personal iPod experience. About a week after I had activated it, the left headphone started to make a cracking noise. That was a bit disappointing, but since I of course have warranty, I called Apple and they sent me a new pair without any question. Way to go, Apple! Excellent!
