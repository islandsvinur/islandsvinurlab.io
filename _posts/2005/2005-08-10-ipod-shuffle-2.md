---
layout: post
title: "iPod shuffle on holiday"
categories: [apple]
date: 2005-08-10 08:03:04 +0200
---
My shuffle had its longest time without refill this holiday and I must say, two weeks is definitely too long for this device. After a week, you've heard every song and you're in need of a change.

That I couldn't update it was of course my own fault, since I had a laptop with me, also loaded with music. I just have to find out which free app is the best for updating iPods (in Linux).
