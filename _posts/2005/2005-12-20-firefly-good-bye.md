---
layout: post
title: "Good bye, Firefly"
categories: [film]
date: 2005-12-20 22:47:15 +0100
---
Well, [it is truly dead then](http://www.ew.com/ew/report/0,6115,1141343_1_0_,00.html)

Serenity, may you light your fire in our hearts and fly forever (bweh, that's cheesy).
