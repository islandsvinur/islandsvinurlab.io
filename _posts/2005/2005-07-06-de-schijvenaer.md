---
layout: post
title: "De Schijvenaer"
categories: [hacking]
date: 2005-07-06 21:40:22 +0200
---
Over the last few months I spent some time on a website for a colleague of my father. They started a mini camping this year and wanted a nice website.

So I started off once again on a job I never wanted to do again: Creating a website for a broad public (i.e. mostly Internet Explorer users). Having had some annoying experiences two years ago with the [Spacelabs](http://www.spacelabs.nl/) RGE project, I almost swore never again to write a site with IE in mind. However, something changed in the last two years. It was not the browser, it was not the XHTML and CSS standards, it was me. Two years ago terms like "semantic HTML" were mostly unknown to me and sites were written in some form of XHTML abusing the beautiful table-tag.

Enter 2004 and the [CSS Zen Garden](http://www.csszengarden.com/) which is a site demonstrating the possibilities of what can be achieved visually through CSS. The site exists in a multitude of CSS files, but only one single XHTML file, which is exactly the same for every single design.

One of the key requirements of a CSS to be accepted to CSS Zen Garden is that it has to work on "most" browsers, which of course includes Internet Explorer. The nice thing about that requirement is that the CSS files are thus compatible with most renderers (MSIE, Gecko and KHTML are the biggest right now, Opera usually also works); create your site in the same structure, and you'll already have a mostly working version!

This is what I did with de site for [Mini-Camping de Schijvenaer](http://www.deschijvenaer.nl/) which works in all browsers, except for the image of the Schijvenaer. This is a little extra for non-IE users...
