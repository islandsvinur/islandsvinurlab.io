---
layout: post
title: "iTunes 4.9 with Podcasting"
categories: [apple]
date: 2005-06-30 17:24:25 +0200
---
The idea of podcasting intrigued me, but since there were no really user-friendly applications (from my point of view) handling podcasts, I never really got into it.

Last tuesday, Apple released an updated version of iTunes which integrates Podcasting into their Music Store and they did it very well. So, I decided to listen what the fuss is all about and subscribed to the [Daily Source Code,](http://www.dailysourcecode.com/) [Adam Curry's](http://www.curry.com/) podcast.

I also subscribed to some other feeds to check the quality of the broadcasts. Now, the content isn't always of very high quality, but the idea of never again miss a radio show because you didn't turn your radio on too late is great!

Already quite a few bloggers are doing podcasting in audio and some of them even do video broadcasts. I hear great success stories from Adam Curry, about how unknown bands become famous just by having their song played on a podcast. They would never get on regional radio, let alone national, so that is pretty cool. But then again, Curry is of course one of the initiators of the whole podcasting thing, so it might be a little bit biased.
