---
layout: post
title: "System memory upgrade"
categories: [apple]
date: 2005-07-21 17:01:37 +0200
---
Today, I had completely had it with the swapping and other memory issues in OS X, so I went to the [Computerland](http://www.computerland.nl/) and bought two very nice and not-so-very-pricy Kingston DDR PC3200 256MB parts and installed them in my Mac.

To open the case and install the memory exactly zero screws are to be unscrewed and exactly one tool, namely one of your fingers, is needed: Open the external case latch, remove the aluminum plate, remove the plastic air deflector shield thingy, remove the two fans in front of the CPU cooling block by just pulling them out. Insert your RAM banks, put everything back in place and boot the system... Yes, very easy, even an idiot could do it. And to prove that, I forgot to put the fans back in :-)

![]({% link /image/memory-1gb.png %}) I already can feel some grave differences between 512 MB and 1 GB. For instance, calling the Dashboard is a lot more responsive, the harddisks have more time off, switching between programs isn't as slow anymore... Of course, there is also the excitement factor, the improvements might not be this great, but there actually are improvements and that's good.

It has to be said, my moments with Linux (albeit short and few) were better than what OS X does to the memory. Much can be improved here on Apple's behalf, I think.
