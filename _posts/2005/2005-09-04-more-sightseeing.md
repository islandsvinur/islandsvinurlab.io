---
layout: post
title: "More sightseeing"
date: 2005-09-04 21:00:18 +0200
---
Some more nice places:

* [Bolungarvík](http://maps.google.com/maps?ll=66.153502,-23.294277&#38;spn=.121312,.256685&#38;t=k&#38;hl=en) is where they shot [Nói](http://www.noi-themovie.com/) [Albínói](http://imdb.com/title/tt0351461/). You most probably cannot get any further away from the European continent (except for Greenland) within Europe.
* [Aletschgletscher](http://maps.google.com/maps?ll=46.458437,8.053665&#38;spn=.826831,1.026741&#38;t=k&#38;hl=en) (the largest gletscher of the Alpes) compared to [Vatnajökull](http://maps.google.com/maps?ll=64.135176,-16.785736&#38;spn=.523618,1.026741&#38;t=k&#38;hl=en) (yes, that is the same scale!). It is truly an enormous ice mass! Now you see why I'm fascinated by Iceland? ;-)
* I wonder what [Akranes](http://maps.google.com/maps?ll=64.316259,-22.089472&#38;spn=0.032512,0.064171&#38;t=k&#38;hl=en) looks like from the ground. Are these rocks above or below the surface? *Edit: [They are above the surface](http://www.ismennt.is/not/jonasg/0landid/jg05/akranes/akranes02.html), seems a friendly town to me.*
