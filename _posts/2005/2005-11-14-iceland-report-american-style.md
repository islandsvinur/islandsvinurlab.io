---
layout: post
title: "Read on Iceland Report: American Style"
categories: [ijsland]
date: 2005-11-14 15:44:23 +0100
---
Read a [kinda funny entry](http://icelandreport.blogspot.com/2005/11/american-style.html) on the Iceland Report (found it via the [Noisedfisk](http://noisedfisk.com/) weblog, very interesting if you're interested in Nordic culture) about a new "American style" restaurant called American Style in downtown Reykjavík.

It made me think of [Pulp Fiction](http://imdb.com/title/tt0110912/) (the "little differences" scene) and somehow also of [101 Reykjavík](http://imdb.com/title/tt0237993/). Small quote:

> Last but not least, "Andskotinn, er ekki fuckin sæti!" is not something the local burger-shack rednecks are wont to say in the US of A.
 
"Damn, no fucking seats!"
