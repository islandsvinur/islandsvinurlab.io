---
layout: post
title: "Efterklang - Tripper"
categories: [muziek]
date: 2005-07-06 22:07:02 +0200
---
[Efterklang](http://www.efterklang.net/) (Flash only site) uses a very special electronic percussion and base sounds which reminds of Kraftwerk's drum computers. This combined with the soft voices and acoustic instruments (trumpet, flügelhorn, piano) gives the music this very unique sound

Efterklang is a Danish group and the name means something like aftersound, or reverberation. On the album Tripper they play together with [Amina](http://www.sigur-ros.co.uk/media/intervi/amina1.html), who already had done ( ) with [Sigur Rós](http://www.sigur-ros.co.uk/).

You can download the video clip for Swarming [(Quicktime)](http://www.posteverything.com/leaf/efterklang/swarming.mov) [(WMV)](http://www.posteverything.com/leaf/efterklang/swarming.wmv) from the groups [web shop](http://www.posteverything.com/artists/release.php?id=8102) which gives a very good idea of their music.
