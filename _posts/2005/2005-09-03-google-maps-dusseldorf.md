---
layout: post
title: "Google Maps Düsseldorf goof"
categories: [geek-stuff]
date: 2005-09-03 09:41:06 +0200
---
Hmm, [Düsseldorf now has two airports](http://maps.google.com/maps?ll=51.263311,6.771698&#38;spn=0.093881,.138127&#38;t=k&#38;hl=en), one on the original location and one in the Rhine where the Theodor Heuss Bridge used to be? Of course I [reported](http://www.google.com/support/maps/bin/request.py?contact_type=report) this to Google.

It's quite an old photo, the bridge over the Rhine they are building in the left is in service a few years already.

**Update**: In the meantime, Google Maps was updated extensively (higher resolution pictures are now available of the area) and the error has been removed.
