---
layout: post
title: "Installed Debian GNU/Linux!!!"
categories: [apple]
date: 2005-01-06 23:12:09 +0100
---
All right then, I installed Linux on my Mac today, finally I was able to do so since the Debian installer from the 4th on has a kernel which supports this machine.

A bit of fooling around with `chroot` and `dpkg` gave me a working system, which can also be accomplished by following the instructions in the [thread](http://lists.debian.org/debian-powerpc/2005/01/msg00159.html) on the [debian-powerpc list](http://lists.debian.org/debian-powerpc/).

I'm happy and can go to sleep now :-)
