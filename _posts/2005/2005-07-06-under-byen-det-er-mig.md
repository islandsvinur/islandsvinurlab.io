---
layout: post
title: "Under Byen - Det Er Mig Der Holder Træerne Sammen"
categories: [muziek]
date: 2005-07-06 23:20:56 +0200
---
[Under Byen](http://www.underbyen.dk/index_uk.htm) is like Efterklang also a Danish group. They play rock music with a sensitive touch.

I always tend to be inclined towards rockbands with female lead singers, and if they are from Scandinavia they must be great. Under Byen is no exception, except that they are exceptionally different since you won't find a guitar in the band (well, there is a bass player, but no lead).

Det Er Mig Der Holder Træerne Sammen is the second album of Under Byen and is, stylewise, a continuation of their first album, Kyst. All lyrics are Danish, which is of course very interesting, but hard to follow. Luckily, there is the website [Always on the Run](http://www.alwaysontherun.net/underbyen.htm) which can give us translations of each and every song Under Byen released. Here, we learn that the albumtitle means 'It is me who holds the trees together' and 'Under Byen' means 'Below the city'.

A download of the title song [Det Er Mig Der Holder Træerne Sammen](http://www.underbyen.dk/audio/determig.mp3) is available for download from their website.
