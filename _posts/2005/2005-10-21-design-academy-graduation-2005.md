---
layout: post
title: "Visited Graduation 2005 at the Design Academy"
date: 2005-10-21 22:01:18 +0200
---
![]({% link /image/ticket/da-graduation-2005.png %}) Yesterday, the whole bunch of the flat went to the [Design Academy](http://www.designacademy.nl/) for the yearly Graduation show. Of course, flatmate [Esther Ermers](http://estherermers.nl/) who graduated last December was there with her Spinning abc. She created 26 stools out of the letters of the (Dutch) alphabet.

![]({% link /image/graduation-2005-spinning-abc.jpg %})  
*Esther Ermers, Spinning abc*
{: .center}

There was also a project, of which one part was this (sorry for the very bad quality picture):

![]({% link /image/graduation-2005-led-it-be.jpg %})  
*LED it be*
{: .center}

Uhm, OK. If it needs explanation: It is indeed a LED and a switch in a cilindrical case and nothing more.
