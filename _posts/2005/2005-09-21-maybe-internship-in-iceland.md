---
layout: post
title: "An internship in Iceland?"
categories: [studie]
date: 2005-09-21 19:08:32 +0200
---
My day couldn't be better when I received this E-mail:

> Komdu sæll Christian 	
> Hvernig gengur þér að læra íslensku? Það er gaman að þú viljir læra íslensku og viljir koma að læra á Háskólanum í Reykjavík.
> 
> I will now switch to English to prevent misunderstanding. We have just set up an Erasmus/Socrates exchange agreement with Eindhoven University of Technology so you could come as part of that agreement. We do offer an MSc. programme in Computer Science and it would be great if you could join in and use your period of academic intership to do some research work here. Would that be an option?
> 
> Kærar kveðjur frá Reykjavík,

Wheeee, an opening. Now I just have to find out what to do here in Eindhoven. Oh yeah, and wait for about a year.
