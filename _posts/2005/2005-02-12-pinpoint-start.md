---
layout: post
title: "Pinpoint"
categories: [hacking]
date: 2005-02-12 20:39:19 +0100
---
[Pinpoint](http://nogates.nl/pinpoint/) is the name of my first real project of [Nogates](http://nogates.nl/). It is a bowling (hence the Pin) score (hence the Point) management application written in the beautiful [Ruby programming language](http://www.ruby-lang.org/).

It will feature multiple frontend interfaces, but will primarily be built upon [GNOME2/Gtk+](http://www.gtk.org/). Maybe a Cocoa frontend will also be created, if I manage to understand the [RubyCocoa](http://www.fobj.com/rubycocoa/doc/) bindings.

The SVN version can be checked out using the command:

```
svn co http://nogates.nl/svn/pinpoint</code>
```
