---
layout: post
title: "Orion back once again and Planet Luon"
categories: [admin]
date: 2005-06-30 17:13:54 +0200
---
Well, revived Orion once again, this time back in its old case with its old mainboard and all is back as it was before we got the new (cursed?) hardware. So I just hope things will run smoothly until I say it to stop instead of stopping on its own.

In the meantime, [Paul](http://paul.luon.net/journal/) and Bram apparently created [Planet Luon](http://planet.luon.net/) and put me on it, so you'll find my journal there as well! A two-feed planet is kind of boring I thought, but it appears that both [Admar](http://luon.net/~admar/journal) and [Bram](http://weatherwax.luon.net/~bram/) started blogging this month, so now we're four.
