---
layout: post
title: "Utopia 7 Years Meeting"
categories: [geek-stuff]
date: 2005-09-21 18:59:38 +0200
---
I almost cannot believe I am the only one posting about this...

![]({% link /image/photos/utopia-7-jaar_thumbnail.png %})  
{: .center}

Last weekend we went with [Utopia](http://utopiamoo.net/) on a meeting in the Drenthean village Drouwen, in the bungalow parc Drouwenerzand, right in between Emmen, Groningen and Assen (that's in the North of the Netherlands).

My compliments go to the organization: Paul, Lotte, Sjoerd and Lise, you did a great job, we had three great days!
