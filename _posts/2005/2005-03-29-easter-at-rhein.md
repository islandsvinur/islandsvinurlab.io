---
layout: post
title: "Happy Easter from Düsseldorf"
date: 2005-03-29 10:36:50 +0200
---
I've been visting family in Düsseldorf this weekend, it was my uncle's birthday and we went to his party in an Altstadt (downtown) café.

Easter Monday we went to walk along the Rhein to take a look at the new quarter called [MedienHafen.](http://www.medienhafen.de/) Modeled after the London Docklands, this old harbour area got a thorough restyle and is now becoming a vivid (and probably expensive, judging by the many Porsches, Jaguars and one Hummer) place to live and work.

Behind the [WDR](http://www.wdr.de/) Funkhaus building (which existed there quite some time already), [three buildings](http://maps.google.com/maps?ll=51.216205,6.757783&#38;spn=0.005874,0.008633&#38;t=k&#38;hl=en) designed by [Frank Gehry](http://en.wikipedia.org/wiki/Frank_Gehry) called [Der Neue Zollhof](http://www.arcspace.com/architects/gehry/zolhoff/) are the first pieces of modern architecture to appear in this area. Some old warehouses got a restyle, others were torn down and got replaced by glass boxes. Most of them aren't very interesting, but it is the "Gehry-Bauten" as they are called by the Düsseldorfers which are the landmarks of this newly developed quarter.

[Some photos I took:](http://photos.luijten.org/Christian/2005/2005-03-28.%20MedienHafen%20Du%CC%88sseldorf/) First an impression of the Rhein-Promenade, which was created around 1990 with the building of a tunnel between Oberkasselerbrücke and Kniebrücke (both bridges can be seen in the first picture). Before the tunnel existed, there was no real possibility to get anywhere near the water from the city center. Photos 11 and 12 show the Landtag building, the administration of Nordrhein Westfalen, of which Düsseldorf is the capital. The tower is called the [Rheinturm](http://en.wikipedia.org/wiki/Rheinturm_D%fcsseldorf) and was built in 1981, it features the largest decimal clock in the world. The rest of the photos are all buildings part of MedienHafen.
