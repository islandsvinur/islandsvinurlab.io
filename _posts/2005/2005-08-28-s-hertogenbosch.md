---
layout: post
title: "'s-Hertogenbosch"
date: 2005-08-28 12:55:26 +0200
---
![]({% link /image/photos/s-hertogenbosch-small_thumbnail.png %}) [Photos](http://photos.luijten.org/Christian/2005/2005-08-30.%20's-Hertogenbosch/) of yesterday's visit to 's-Hertogenbosch, our province's capital.
