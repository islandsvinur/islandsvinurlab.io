---
layout: post
title: "The genitive is in grave danger"
categories: [taal]
date: 2007-02-14 22:46:17 +0100
---
Indo-European languages all had this beautiful case system with at least four base cases, nomenative, accusative, dative and genitive (which is the Icelandic order) and usually [a few more](https://en.wikipedia.org/wiki/List_of_grammatical_cases).

In many languages however, including Dutch, those cases have almost completely disappeared. We have remnants of cases in our personal pronouns (ik (nom.), mij (acc.), mijn (dat./gen.)), put 's' behind words to indicate ownership (i.e. a simple genitivus form) and do weird things (they're in fact inflections) with adjectives ("sometimes" add 'e' suffix), but otherwise they're completely gone.

In some languages which still have cases, such as German and Icelandic, the genitivus tends to disappear slowly in favor of a construction using the dativus.

[Der Dativ ist dem Genitiv sein Tod](https://de.wikipedia.org/wiki/Der_Dativ_ist_dem_Genitiv_sein_Tod) is an example of a sentence which doesn't use the genitive but the dative instead, and becoming more and more common in spoken language. It is already pretty common to use this construction in Dutch ("De dativus is de genitivus zijn dood") and now look what this language has become! The sentences "Der Dativ ist der Tod des Genitivs" or even "Der Dativ ist des Genitivs Tod" say exactly the same and are more aestetically pleasing.

In Icelandic the genitive is used extensively for creating adjectives out of nouns: "Ráðhúsið Reykjavíkur" (City Hall of Reykjavik), "Flugstöð Leifs Eiríkssonar" (Leifur Eiriksson Airterminal). However, here also the genitive is threatened by the dative in the sense that they will probably merge somewhere in the next 200 years.

As a citizen of a case-deprived country I would like to say to those lucky enough to still have them: Save and protect them!

And to those living in case-deprived countries having to learn a highly inflectional language: There will be a day when the penny drops and all of a sudden it just comes naturally, believe me.
