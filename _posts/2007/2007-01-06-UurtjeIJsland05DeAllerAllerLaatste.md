---
layout: post
title: "Uurtje IJsland #05: De Aller Allerlaatste"
categories: [ijsland]
tags: [podcast]
date: 2007-01-06 17:55:43 +0100
---
Nou, vooruit, dit is dan echt de allerallerlaatste.

1. Bang Gang -- Find What You Can Get
2. Jeff Who? -- Barfly
3. The Sugarcubes -- Motorcrash
4. Apparat Organ Quartet -- Cruise Control
5. Einóma -- Hringlögun
6. The Sugarcubes -- Deus (remix)
7. Bang Gang -- So Alone
8. GusGus -- Purple
9. Pendulum -- Follower
10. Ghostigital -- The Heart
11. Stafrænn Hákon -- Rafmagn 
12. Stafrænn Hákon -- Gorecki Magnus
13. Sigur Rós -- Refur

Directe download: [hier](https://www.dropbox.com/s/dgii7or5a5vwsd4/05%20%2305.%20Uurtje%20IJsland%2C%206%20februari%202007.m4a?dl=0)
