---
layout: post
title: "Anneke van Giersbergen leaving The Gathering"
categories: [muziek]
date: 2007-06-08 08:47:10 +0200
---
[Sad news](http://www.omroepbrabant.nl/news.aspx?id=83197) this morning in my feed reader; Anneke van Giersbergen, singer of [The Gathering](http://www.gathering.nl/) is leaving the band after 13 years for starting a new band called [Agua de Annique](http://www.aguadeannique.com/).

* [Band's statement](http://www.gathering.nl/news.php?id=857)
* [Anneke's statement](http://blog.myspace.com/index.cfm?fuseaction=blog.view&#38;friendID=80545931&#38;blogID=272698767&#38;MyToken=896c7dc1-3716-4215-b7a2-b3cf592307a2)

I hope they will both find their ways and come back at both their full strengths, then we will have *two* great acts.
