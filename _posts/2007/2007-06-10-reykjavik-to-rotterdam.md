---
layout: post
title: "Reykjaví­k To Rotterdam"
categories: [ijsland]
date: 2007-06-10 20:49:50 +0200
---
<img src="http://www.reykjavik.to/rotterdam/images/poster1.jpg" alt="" />

There's an interesting festival coming up in November: [Reykjavík to Rotterdam](http://www.reykjavik.to/rotterdam/), featuring music, dance, movies and art from our beloved island up north.

Gigs I'd be interested in attending are Hafdís Huld and Apparat Organ Quartet, but then there are also the movies, with Baltasar Kormákur present for Q and A. Full programme already.

I'll have to see what will come of that, as usual I tend to overestimate my time and money ;)

Funny thing I noticed that I once met the secretary of the festival at the [Sigur Rós concert back in July 2005]{% post_url 2005/2005-07-14-sigur-ros-paradiso-2 %})... Small world :)
