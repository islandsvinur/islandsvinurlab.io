---
layout: post
title: "De Stad van de Dromende Boeken"
categories: [boek]
date: 2007-10-14 21:59:43 +0200
---
![]({% link /image/de-stad-van-de-dromende-boeken.gif %}) Daar sta je dan bij Scholtens (al raar zat dat ze niet Van Piere of Gianotten heten eigenlijk :-)) een cadeautje uit te zoeken en je oog valt op een wel heel bekende boekomslag... Is dat... Nee, dat kan toch niet? In Nederland? Hee, die titel is zelfs in 't Nederlands... Raarrr!

"Die Stad der träumenden Bücher" is vertaald naar het Nederlands en heet hier ["De Stad van de Dromende Boeken"](http://www.nl.bol.com/is-bin/INTERSHOP.enfinity/eCS/Store/nl/-/EUR/BOL_DisplayProductInformation-Start?Section=BOOK&#38;BOL_OWNER_ID=1001004002500106)!
 
 > Als de peetvader van de jonge dichter Hildegunst von Mythenmetz sterft, laat hij zijn beschermeling niet veel meer na dan een manuscript. Mythenmetz, een lintworm, ziet zich gedwongen het geheim van zijn afkomst na te gaan. Het spoor leidt naar Buchheim in Zamonië, de Stad van de Dromende Boeken. Als de held de stad betreedt is het alsof hij de deur van een gigantische boekhandel opentrekt. Hij ruikt iets zuurs dat hem aan de geur van citroenbomen doet denken, het opwindende aroma van oud leer en het scherpe en intelligente parfum van drukinkt. Eens in de klauwen van deze op boeken verzotte stad raakt Mythenmetz steeds dieper verstrikt in de labyrintische wereld, waarin lezen een reëel gevaar is, meedogenloze boekenjagers op bibliofiele schatten azen, 'boekelingen' streken uithalen en de mysterieuze Schaduwkoning heerst.
> 
> Walter Moers ontvoert de lezer naar het betoverde rijk van de literatuur, waar lezen een avontuur is, waar boeken hun lezers niet alleen op een spannende manier vermaken en aan het lachen brengen, maar ook tot waanzin drijven of zelfs ombrengen. De stad van de dromende boeken is een avonturenroman met de vaart van een thriller, een horrorverhaal vol romantisch uitvergrote creaturen en een bijzonder leuke parodie op de wereldliteratuur. Tenminste als je met grappen over Goethe en Shakespeare kunt lachen. Of net niet.
> 
> Bron: Bol.com

Dus, geen excuus meer om weer eens een gezellig [Walter Moers]({% post_url 2007/2007-08-27-der-schrecksenmeister %}) boek te lezen, ook al is je Duits niet zo goed ;-)
