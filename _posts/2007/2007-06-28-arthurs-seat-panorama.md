---
layout: post
title: "Arthur's Seat panorama"
date: 2007-06-28 16:31:29 +0200
---
And I found yet another panorama in my photo collection!

![Arthur's Seat]({% link /photos/arthurs-seat-panorama-small.jpg %})

A very wide view over the friendly city of Edinburgh, Scotland, seen from 
[Arthur's Seat](https://en.wikipedia.org/wiki/Arthur%27s_Seat%2C_Edinburgh).
