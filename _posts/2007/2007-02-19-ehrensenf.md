---
layout: post
title: "Ehrensenf"
categories: [web-stuff]
date: 2007-02-19 13:17:21 +0100
---
As a regular [Rocketboom](http://www.rocketboom.com/) viewer, I was [introduced in December](http://www.rocketboom.com/vlog/archives/2006/12/rb_06_dec_14.html) to the german show [Ehrensenf](http://www.ehrensenf.de/) (ES) (anagram of Fernsehen, or television), which I since then catch daily, too.

![]({% link /image/ehrensenf.png %})

ES reached 300 episodes today! Congratulations!

While it seems to be a copy of Rocketboom at first sight, be it in German, the show turns out to be quite fun to watch *and* to be different.

Both shows are produced and scripted, but most hosts of ES (they have "backups" so to say in case Katrin is on the road for [3sat](http://www.3sat.de/) or otherwise busy for the radio or her studies in technical journalism) are in my opinion better than Joanne from Rocketboom. It just looks a bit more spontaneous, less rehearsed.

ES is funny, despite the [Germans are thought of to have no humour at all](https://en.wikipedia.org/wiki/German_humour) (which is true in many cases, especially TV shows) and even though the ES team is from Cologne (and thus not from Düsseldorf).

That being said, I think the Netherlands lack a *good* light news internet-tv show, but somehow I just can't imagine it happening here. The only site I would expect to come out with such a thing would be Geenstijl... Yay, what fun would that be... *(pause)* NOT!
