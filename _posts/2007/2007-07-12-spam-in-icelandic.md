---
layout: post
title: "Spam in Icelandic?"
categories: [ijsland]
date: 2007-07-12 16:44:32 +0200
---
Hmm, found this in my mailbox today:

> Og einn dag . .inginu er menn gengu fj.lmennir til L.gbergs og er .ar var lykt a. m.la l.gskilum .. kvaddi Gunnlaugur s.r hlj..s og m.lti: "Er Hrafn h.r .nundarson?"
 
Accompanied by the picture of a certain female I've never met, a text of a sexual nature and a web address.

But that last part wasn't even the most interesting, it was the text. It obviously is no English; but by a coincidence I could clearly recognize it as being written in the language of the Icelanders.

A little searching revealed that we're dealing here with an extract from [chapter 11](http://www.skolavefur.is/_opid/islenska/bokmenntir/isl_sogur/tegund/sagas/sogur/gunnlaugssaga/kaflar/11/index.htm) of [Gunnlaugs saga](http://www.skolavefur.is/_opid/islenska/bokmenntir/isl_sogur/tegund/sagas/sogur/gunnlaugssaga/):

> Og einn dag á þinginu er menn gengu fjölmennir til Lögbergs og er þar var lykt að mæla lögskilum þá kvaddi Gunnlaugur sér hljóðs og mælti: "Er Hrafn hér Önundarson?"

> And on one day of the [thing](https://en.wikipedia.org/wiki/Thing_%28assembly%29) (an assembly) many people gathered to the [Lögberg](https://en.wikipedia.org/wiki/L%D6gberg) (the hill on [Þingvellir](https://en.wikipedia.org/wiki/Thingvellir) where law was spoken) and there was a smell of talk about the definition of a law when Gunnlaugur took the floor and spoke "Is Hrafn here Önundur's son?"
 	
Jæja... Well, I haven't read the saga yet, but now I might, thanks to some spam on a rainy day :)

Detail for the Lord of the Rings fans; Gunnlaugs nickname is *Ormstunga*; Worm-Tongue or Snake-Tongue. Guess where J.R.R. got Gríma's name!
