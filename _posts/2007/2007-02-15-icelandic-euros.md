---
layout: post
title: "Icelandic euros?"
categories: [ijsland]
date: 2007-02-15 16:57:00 +0100
---
An article titled [Icelandic euros sold on eBay](http://www.mbl.is/mm/frettir/frett.html?nid=1253706;rss=1) in Morgunblaðið caught my attention today.

![]({% link /image/ebay-icelandic-euros.png %})

The eBay auctionnair ede2016 claims they are proof coins. The MBL article however mentions that they are coined in 2004 by a Swiss company, which also created and sold Swiss, Greenlandic and Faroese euros.

The coins don't have any money value, and probably not even collector's value. Iceland is neither a member of the EU nor of the EMU (European Monetary Union), but they signed a number of EU treaties such as Schengen (Because of this treaty, it is possible for a EU citizen to stay in Iceland for as long as six months without residence permit, as opposed to 3 months for non-EU citizens).

The Icelandic krónur is completely independent from any other currency (which is the reason why it fluctuates so heavily, usually between 75 and 100 eurocents within one year) and it is unlikely that they will take up the euro soon or will even tie their currency to the European common coin. However there are some voices to do the latter.
