---
layout: post
title: "Cellphone kaputt"
date: 2007-07-17 08:57:06 +0200
---
To anyone having my cellphone number; my phone is broken and is now turned off pretty much all the time. It had a hole in the display for a few weeks, now the keys 2, 5, 8, and 0 don't work anymore.

**Update:** I've been to the Vodafone store, where they could tell me that on August 9, my contract will enter its 3 last months and I can choose between a new phone or a SIM-only discount. So I'll just wait for that and see what they'll have on offer by then. The keys magically started working again, by the way.
