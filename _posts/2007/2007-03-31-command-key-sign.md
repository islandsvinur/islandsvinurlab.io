---
layout: post
title: So, how did the command key get this ⌘ next to the ?
categories: [apple]
date: 2007-03-31 21:43:56 +0200
---
![]({% link /image/place-of-interest-sign.png %})

When I came to Iceland, I saw the same symbol the command key on Apple keyboard carries (⌘) everywhere there was something interesting to see. It is called the [Saint Hannes cross](https://en.wikipedia.org/wiki/Saint_John%27s_Arms) and is an old viking symbol and now used in the Nordic countries and Estonia to denote sites of touristic interest.

I wondered about this and apparantly there is a funny [anecdote](http://www.folklore.org/StoryView.py?project=Macintosh&#38;story=Swedish_Campground.txt) about how the "clover" sign as some refer to it came on the command key.

When Steve Jobs first saw the large application menus of the MacDraw application, he saw a list full of  logos. He didn't like this, thought it would take the logo in vain and ordered them to find another symbol to take its place.

**Update**: Hmm, I forgot to mention that I keep running into the clover. Those were actually the reason for this post!

German band [Silbermond](http://www.silbermond.de/) uses it on the cover of their newest album 'Laut gedacht'. No idea why, but they have one track called 'Das Ende vom Kreis'. The End of the Circle as universal symbol of infinity and is ⌘ the new one?

In [Die 13 1/2 Leben des Käpt'n Blaubär](https://en.wikipedia.org/wiki/The_Thirteen_and_a_Half_Lives_of_Captain_Bluebear) by [Walter Moers](https://en.wikipedia.org/wiki/Walter_Moers), the protagonist lands inside the Eternal Tornado which is describing a course in the form of the Saint Hannes cross of about two thousand kilometers long.

