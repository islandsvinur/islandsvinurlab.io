---
layout: post
title: "From Cultur to Commerz"
date: 2007-05-14 07:43:17 +0200
---

![]({% link /image/ampelmann-cultur.png %})  
*Ampelmann anno 1998*
{: .center}

![]({% link /image/ampelmann-commerz.png %})  
*Ampelmann anno 2007*
{: .center}

[Ampelmann](https://en.wikipedia.org/wiki/Ampelm%C3%A4nnchen) became [big business](http://www.ampelmann.de/) in little more than a few years. Back then, a t-shirt was all you could get. Now there are mouse pads, ice cubes, reflectors, bags, et cetera. Back then the shirt was a protest against the Westernisation of the traffic lights in former East-Berlin. Now it is just another souvenir, be it one with history.
