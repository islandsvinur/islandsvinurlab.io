---
layout: post
title: "The skating rink is closed, and so is the season"
categories: [sport]
date: 2007-03-09 09:28:19 +0100
---
While [Ireen and Sven are becoming world champions in Calgary](https://nl.wikipedia.org/wiki/Wereldkampioenschap_schaatsen_allround_2006), the season in Eindhoven is already over. Wednesday was the last time on the ice for a few months and as a nice last training, we did a 10 kilometers, or 25 rounds.

I can only say this, there was a lot of pain involved. After about 15 rounds, my back hurt like hell and the only way to get on was to stand up, but then you lose stability, so you have to go down again.

Then, after 18 rounds, the trainer said something which I understood as 'only 18 rounds left'... What! But it was a relief then to hear him shout 'last round!' after 5 more rounds already :-)

In the end I did the 10000 meters in 26.00 minutes, with a feeling of satisfaction. The world record of <del>12.49,88</del> [12.41,69](https://nl.wikipedia.org/wiki/Lijst_van_schaatsrecords_%28langebaan%29#Heren_senioren) is set by Sven Kramer, so I'm only about twice as slow as him ;-)

Last week on [Monday](http://www.essvisis.nl/nieuw/fotos/fotos.php?action=album&#38;id=176) and [Tuesday](http://www.essvisis.nl/nieuw/fotos/fotos.php?action=album&#38;id=177) by the way was the yearly [Isis](http://www.essvisis.nl/) tournament where I played photographer (see the links).
