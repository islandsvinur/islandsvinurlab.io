---
layout: post
title: "IKEA Hacker"
categories: [web-stuff]
date: 2007-02-05 09:47:56 +0100
---
Obligatory [reading material](http://ikeahacker.blogspot.com/) for every IKEA fan!
 	
![]({% link /image/photos/mofocase.png %})  
*hip ikea music machine*

> WANTED: your ikea hacks  
> i like tinkering with ikea stuff and i know many other people who do too. whatever they may be -- a funked up karlanda sofa, an ingenious idea for your pax wardrobe, a creative twist on your kitchen countertop, or even advice on how to finally stop flimsy forby stools from wobbling, i'd love to hear from you. email me at ikeahacker [at] gmail [dot] com.

Very useful, I say. Although I also read that the LACK wall shelve was hard to install and also EXPEDIT is causing troubles, even needing extra tools to build them...

Now, I've seen JG (flatmate, the ones who know him should know enough ;-)) build up his EXPEDIT on his own without any help nor did he need additional tools, so I gathered this should be doable by anyone.

Maybe European build instructions are incompatible with Americans? Are our "little differences" a bit too big or does the blogger in question have two left hands?

Other nice hacks:

* [ikea dna lamp](http://ikeahacker.blogspot.com/2006/07/make-your-own-ikea-dna-lamp.html)
* [lack music cabinet](http://ikeahacker.blogspot.com/2006/06/one-for-music.html)
* [tupplur home cinema screen](http://ikeahacker.blogspot.com/2006/07/timtoms-trio-1-tupplur-home-cinema.html)
