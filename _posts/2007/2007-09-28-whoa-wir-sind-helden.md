---
layout: post
title: "Whoa! Wir sind Helden in Effenaar"
categories: [ muziek ]
date: 2007-09-28 10:05:02 +0200
---
Great concert! Thanks!

[![]({% link /image/wir-sind-helden.gif %})](http://www.wirsindhelden.com/)
{: .center}

For those who missed it, you'll get a new chance tonight in [Paradiso, Amsterdam via Fabchannel](http://www.fabchannel.com/wir_sind_helden).
