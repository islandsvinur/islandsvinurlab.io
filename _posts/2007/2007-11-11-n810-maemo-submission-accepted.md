---
layout: post
title: "N810 maemo submission accepted"
categories: [hacking]
date: 2007-11-11 07:50:19 +0100
---

Okay, joining the other 498 posters on [Planet Maemo](http://planet.maemo.org/):

> N810 maemo submission accepted  
> Congratulations! You have been accepted to the [N810](http://europe.nokia.com/A4568578) [maemo](http://www.maemo.org/) device program. We will send your discount and instructions as soon as the device is available in your selected shop (soon).

Wheeee! Let's find out how the N810 tastes with an Apple...
