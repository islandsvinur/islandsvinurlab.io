---
layout: post
title: "U2 - Stay (Faraway, So Close!)"
categories: [muziek]
date: 2007-02-11 21:50:02 +0100
tags: [youtube]
---
I was pointed to this [U2 music video](https://www.youtube.com/watch?v=Xci8hM42Cz0) ([transcoded to h.264](http://video.luijten.org/u2-faraway-so-close.mp4)) yesterday.

{% youtube "https://www.youtube.com/watch?v=Xci8hM42Cz0" %}
	
People who've seen [Himmel über Berlin](http://imdb.com/title/tt0093191/) will recognize the theme... And the sequel to that movie is called... [In weiter Ferne, so nah!](http://imdb.com/title/tt0107209/) or in English "Faraway, So Close!"
	
Further, if you take a close look at the leader, you'll notice that [Wim Wenders](http://imdb.com/name/nm0000694/) himself was the director of the video. It was produced in 1993, the same year as the follow-up movie.
