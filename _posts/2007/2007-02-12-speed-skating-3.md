---
layout: post
title: "Speed skating and the goal for the end of this season"
categories: [sport]
date: 2007-02-12 20:22:05 +0100
---
Hmm... today, I heard we're supposed to do 30 rounds (without rest) in less than an hour by the end of the season... That's next month by the way.

Until a week ago, exercises of 4 rounds was the most we did, today it was 10 minutes rounds of one minute, one minute rest, 10 minutes rounds of 50 seconds, two rounds rest, four times three rounds, two rounds rest and then fifteen minutes rounds of 50 seconds.

The nice thing is that I feel my technique improving by the round. This higher group certainly was a good choice.
