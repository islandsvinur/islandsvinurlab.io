---
layout: post
title: "Eindelijk: Eerste schaatstijden!"
categories: [sport]
date: 2007-11-08 00:24:56 +0100
---
Vandaag werden de eerste twee afstanden van de gloednieuwe[^1]
interne Isis competitie gereden. Vanaf ongeveer half 11 hadden we een uur de
tijd om met alle 28 deelnemers de 500 en 1000 meter te gaan rijden.

Voor velen was het de eerste rit na de zomerstop en de gedegen
seizoensvoorbereiding in de vorm
van [droogtraining](http://www.essvisis.nl/nieuw/trainingen/trainingen.php?item=droogtrainen)
onder leiding van Jos, een van onze geweldige trainers (en ook een super
motivator!) en sinds het begin van het schaatsseizoen natuurlijk ook van -- in
mijn geval -- Bob zorgde dan ook voor vele persoonlijke records.

Mijn resultaat, PRs op zowel de 500 als 1000... Of eigenlijk eerste tijden, want
een wedstrijd had ik nooit gereden. Jos: "Zie je, die droogtrainingen hebben bij
jou dit jaar echt effect gehad"... Ja, nou, dat kon ik dus helaas niet echt in
cijfers uitgedrukt zien, maar het voelt dit jaar gewoon veel lekkerder.

Omdat er alleen met handtijd werd gemeten (dus lekker ouderwets met stopwatches
en zonder electronische tijdswaarneming) hier mijn resultaten (en
de [wereldrecords](https://en.wikipedia.org/wiki/List_of_speed_skating_records)
ter referentie ;)) op honderdste seconden "nauwkeurig":

| Afstand | Tijd | WR |
|---------| --- | --- |
| 500m    | 1:02.83 | 0:34.25 (Lee Kang-Seok) |
| 1000m   | 2:15.06 | 1:07.03 (Shani Davis) |

Het ijs was nu veel harder en gladder dan tijdens de trainingsuren, hier moest
ik eerst wel even aan wennen. En ik had net m'n schaatsen opnieuw geslepen, wat
het gevoel ook een beetje verandert.

Ik vond het geweldig om eens te doen en ik zie dan ook alweer uit naar de
volgende wedstrijden. En natuurlijk, ja het is een cliché, ik heb spijt dat ik
niet veeel eerder al heb meegedaan aan wedstrijden!

[^1]: Tot aan seizoen 2005/2006 hadden alle leden een [KNSB licentie](http://www.knsb.nl/content/algemeen/licenties.asp) en [wedstrijdabonnement](http://www.knsbzuid.nl/users/bc-ehv/index.html), in seizoen 2006/2007 werd dit overbodig doordat de niet-selectie-leden op "open uren" gingen schaatsen met een normaal recreatief baanabonnement. Hierdoor werden bijna geen wedstrijden meer gereden door de leden. Dit is echter wel een van de in de statuten vastgestelde doelstellingen van de vereniging. Voor seizoen 2007/2008 heeft het bestuur daarom bij het sportcentrum voorgesteld om zelf wedstrijden te organiseren en dit werd toegekend.
