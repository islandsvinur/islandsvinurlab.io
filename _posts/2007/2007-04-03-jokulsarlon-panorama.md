---
layout: post
title: "Jökulsárlón panorama"
categories: [ijsland]
date: 2007-04-03 15:20:30 +0200
---
Played a bit with some photos I had still laying around waiting to be processed and found the seven images that make up the following panoramic image of Jökulsárlón in Iceland. That's the same lake as you see in the header, except the photographer thereof was a bit more lucky with the weather :-).

![]({% link /photos/iceland/jokulsarlon-panorama-small.jpg %})
{: .center}

Anyway, the full size picture is over 28 megapixel large, I also have [a smaller version]{% link /photos/iceland/jokulsarlon-panorama-small.jpg %}), still around 2 megapixel.

If you'd want to print the full size: at 150 dpi it would be 2m50 by 30cm.
