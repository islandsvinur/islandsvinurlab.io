---
layout: post
title: "Migration successful"
categories: [admin]
date: 2007-05-22 08:40:51 +0200
tags: [abraracourcix, sunny]
---
Over the weekend, I have transferred the web site from [Abraracourcix](http://abraracourcix.luon.net/) to [Sunny](http://sunny.luon.net/). I'm in the process of slowly phasing out Abraracourcix' services, eventually moving the hard disk into Sunny and then put it to a well deserved sleep.

My own mail setup has been moved to Sunny already, the other accounts will follow somewhere in the next few weeks.

The web server changed from [Apache](http://httpd.apache.org/) to [lighttpd](http://lighttpd.net/) and so far, I'm pretty satisfied with it. Configuration is a breeze, it responds quickly, logs are in the [common logfile format](http://www.w3.org/Daemon/User/Config/Logging.html#common-logfile-format) so the [local Webalizer statistics](http://sunny.luon.net/webalizer/) also still work, there's an [RRDtool](http://oss.oetiker.ch/rrdtool/) plugin which makes it pretty easy to construct [graphs of the web traffic](http://sunny.luon.net/lighttpd-rrd/) (also per virtual host or even for specific directories).

Abraracourcix has been my personal server for more than 7 years. It was a router for the internet connection at my parents' home and before that it used to be my desktop. In 2003, operations moved to Spacelabs where it became the main web and e-mail server for the luijten.org domain, serving up all static web content and doing SMTP and IMAP. <br /> Especially the latter service is very heavy for this Pentium 166 with only 48 MB RAM, so it had to go off the machine.

Sunny is a [Sun Ultra 5](http://sunsolve.sun.com/handbook_pub/Systems/U5/U5.html) desktop machine with a UltraSparc IIi at 360 MHz and 256 MB RAM, which should be able to hold up the load a bit longer. I got this somewhere in 2005 from [Tilburg University](http://www.uvt.nl/) who were dumping their old hardware.
