---
layout: post
title: "Hunting your own food"
date: 2007-03-15 14:58:20 +0100
---
Walking dinner... or... hunting your own food. It is a yearly activity at Isis and usually one of the best-attended ones (most probably because there's food involved). I don't have exact numbers, but making an wild guess I would say about 35-40 out of the total 100 members were present.

If you're not familiar with the concept, a small introduction. Everyone gets three addresses where they're going to have entry, main dish and dessert, along with what they're going to make themselves. You get this of course far too late to prepare properly and you don't really have time to arrange much.

So, Jacco and I were a "team" so to speak and had our entry course, mustard pea soup with lots of smoked sausage at Rinka's house at 6pm... Others at the table were Maarten, Erwin, Andre, Juliën, and Marcel.

![]({% link /image/photos/walking-dinner.png %}) Then, at half past seven we rushed home to prepare our main course: cheese fondue. Man, that's the last time I attempt that at a walking dinner :&ETH; All was good, it just takes soooo much time to melt the cheese properly. We got Herjan, Lisa, Sibrecht, Floor, Wouter, and Michiel for dinner.

Barely making the two hours planned for dinner, we arrived a little late at Roel's, where the Tiramisù was already waiting for Sandra, Mijke, Maresa, and us.

Afterwards, we met in café Spijker and thus and there the walking dinner 2007 was concluded.

I would say, it was very successful, we had a lot of fun and I'm looking forward to next year's walking dinner. Hopefully I won't have to make the main dish then, it's fun to do, but also a bit stressy.

**Update:** There are [photos](http://www.essvisis.nl/nieuw/fotos/fotos.php?action=album&#38;id=180) online.
