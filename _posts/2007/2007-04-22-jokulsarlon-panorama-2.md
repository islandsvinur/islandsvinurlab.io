---
layout: post
title: "Jökulsárlón panorama revisited"
categories: [ijsland]
date: 2007-04-22 15:58:29 +0200
---
And yet another series of collected images which become a panoramic image when arranged in the right way... I can't recall myself making so many of them :D But that's alright, because panoramas are good.

We're once again at Jökulsárlón, this time at the other side of the river. You can see the place where [the other panorama]({% post_url 2007/2007-04-03-jokulsarlon-panorama %}) was taken on the left, on the hill, somewhere around the electricity mast.

On the other picture, you can see a hill next to a little restaurant and amphibious vehicle base (during summer, you can take a trip on the lake between the ice blocks). It's where I stood and took this one.

![]({% link /photos/iceland/jokulsarlon-panorama-2-small.jpg %})

The lake looks very different on this side, big blocks float directly (but very, very slowly) into the sea, the smaller ones get trapped in the bay on the other side. The largest blocks can't get through the narrow stream to sea and have to stay a while to melt away.
