---
layout: post
title: "RSS, two and a half years later"
date: 2007-04-23 08:59:13 +0200
tags: [rss]
---

Two and a half years ago I wrote [an article]({% post_url 2004/2004-10-12-rss-why-1 %}) (has it been that long, time
really flies) about the state and future of RSS. I thought it would be nice to revisit this article and delibrate on the
next steps.

The first problem I brought up was the fact that it is a poll system, as opposed to an event-based system. I asserted
that if RSS wants to be successful, it has to be converted into a push method. Well, I was quite wrong :-) It turned out
to be so successful that there are now companies and services that only publish RSS feeds from other
feeds ([Feedburner](http://www.feedburner.com/), [Yahoo! Pipes](http://pipes.yahoo.com/)).

![]({% link /image/podcast.png %})

Something which developed very quickly during the end of 2004 was a thing
called [podcasting](https://en.wikipedia.org/wiki/Podcast). Audio and video files are enclosed in the RSS feed and can
be automatically downloaded by a newsreader or "podcatcher". Many podcasts exist, most are of questionable quality, but
some stand out, just like with written weblogs.

The idea of podcasting caught quite a bit of attention and the relatively simple technology behind it created a slew of
more or less successful ideas. [Apple](http://apple.com/) thought of photocasting where the iPhoto application creates
RSS feeds with an item for each photo that is to be published. Other users of iPhoto can then subscribe to the feed and
get automatic updates of others' photos. It didn't really catch on, although [Flickr](http://flickr.com/) is now
offering RSS feeds with photos enclosed which can be used with iPhoto (or any other feed reader, for that matter).

![Hackergotchi]({% link /image/hackergotchi.png %})

In Free Software development circles, a Python script called [Planet](http://www.planetplanet.org/) became
popular [to](http://planet.luon.net/) 
[aggregate](http://planet.gnome.org/) 
[the](http://planet.debian.net/) 
[weblogs](http://planetkde.org/) 
[of](http://planet.freedesktop.org/) 
[the](http://planet.go-oo.org/) 
[many](http://planet.jabber.org/) 
[developers](http://planet.mozilla.org/) 
[working](http://gstreamer.freedesktop.org/planet/) 
[on](http://www.planetapache.org/)
[a project](http://planet.adiumx.com/). The [Hackergotchi]({% post_url 2004/2004-10-20-hackergotchi %}), a small picture
with or without fancy drop shadow to identify authors of the different posts was born.

The simple way in which RSS feeds can be parsed and extended, thanks to XML, generates new applications every day. RSS
no longer is solely a way of delivering blog content to the end user, it isn't even bound to things happening over time
anymore. It has become a standardized way to exchange content of any kind.

It is an important step in the becoming of the [semantic web](https://en.wikipedia.org/wiki/Semantic_Web), where every
piece of information is put into context and accessible from anywhere using any device. It was a driving force in the
success of the Web 2.0 bubble which didn't burst like the first.

Advertising companies understood the possibilities of RSS and started offering advertisements for inclusion in feeds,
either in the form of a banner in the footer of posts, or as advertorials in separate feed items.

![Creative Commons - Some rights reserved](http://creativecommons.org/images/public/somerights20.png)

Recently, a discussion about the copyrights of feeds came up
when [some](http://frank-ly.nl/verandering-in-de-rechten-van-de-webfeed) [people](http://www.yme.nl/ymerce/2007/04/13/original-signal-klopt-dit-wel/) [discovered](http://www.marketingfacts.nl/berichten/20070415_hoe_ver_mag_je_gaan_met_de_rss_feed_van_anderen/)
that their feeds were aggregated by a commercial site which put advertisements in their posts (thereby possibly
exercising their fair-use rights). They responded by relicensing the feed under a non-commercial Creative Commons
license. They have no problem with people earning money with what they write, they *do* have a problem with people
earning money with their writings without sharing or even telling.

*So, what do you think, will the article I will write two and a half years from now still be about RSS? Or will we
arrive at a better technology? What will be the challenges for the next years? Will this technology or its successor
ever become so user friendly your mother would use it?*

*I've added comments to the site, just follow the link below and leave a message!*

**Update:** I found
this [little video trying to explain RSS in plain English](http://www.commoncraft.com/rss_plain_english). I think this
still is way too technical, but it's a start.
