---
layout: post
title: "Meer natuurijs!"
date: 2007-12-21 17:42:19 +0100
---
Spontane acties zijn altijd de beste en zo was de spontane actie van mede-Isisiaan Michiel om dan maar eens een goed bevroren meer op te gaan zoeken een hele goede.

Via Omroep Brabant al berichten binnengekregen dat op het Sonse Meer ([kaartje](http://maps.google.nl/maps?f=q&#38;hl=nl&#38;geocode=&#38;q=Oud+Meer&#38;sll=51.515526,5.456514&#38;sspn=0.014154,0.030427&#38;ie=UTF8&#38;ll=51.515166,5.458596&#38;spn=0.007077,0.015213&#38;t=h&#38;z=16&#38;iwloc=addr&#38;om=1)) al geschaatst werd, dus daar maar eens heen gegaan. En inderdaad, een mooie grote ijsvlakte doemt op uit de mist...
 	<img src="http://photos.luijten.org/Christian/2007/2007-12-21.%20Sonse%20Meer/.cache/400x265-IMG_8033.jpg" alt="" />
 	<img src="http://photos.luijten.org/Christian/2007/2007-12-21.%20Sonse%20Meer/.cache/400x265-IMG_8050.jpg" alt="" />
 	<img src="http://photos.luijten.org/Christian/2007/2007-12-21.%20Sonse%20Meer/.cache/400x265-IMG_8056.jpg" alt="" />

[Mooi hoor](http://photos.luijten.org/Christian/2007/2007-12-21.%20Sonse%20Meer/), maar wat een gekraak en geknars. Het was voor mij trouwens ook de eerste keer dat ik op een serieuze plas stond en niet bij ons in het park op de vijver. Tsja, Brabant is nou eenmaal niet zo goed bedeeld als het noorden van het land wat meren en sloten en koud weer betreft.
