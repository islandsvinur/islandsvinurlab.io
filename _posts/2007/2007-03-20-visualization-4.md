---
layout: post
title: "Fluids 0.1.0 Equinox"
categories: [studie]
date: 2007-03-20 17:39:49 +0100
---
Well, so much for the practical part of the assignment, this is the version I'll probably deliver end of this week. Added since the last pre-release are three types of isolines: Based on fixed levels, based on a fixed number of lines, based on fixed points in space. Also, it can do fullscreen, yay!
 
![]({% link /image/study/fluids-isolines.png %})

So, I hereby release version 0.1 of [Fluids](http://nogates.nl/fluids/) and name it [Equinox](https://en.wikipedia.org/wiki/Equinox), since that's exactly what is going to happen tomorrow at 0:07 UTC. The days are becoming longer and longer, and starting tomorrow, they'll be longer in Iceland than they are here again. Spring's here!

* [Fluids 0.1 intel]{% link /files/fluids/Fluids-i386-0.1.0.dmg %})
* [Fluids 0.1 powerpc]{% link /files/fluids/Fluids-powerpc-0.1.0.dmg %})
* [Fluids source code]{% link /files/fluids/fluids-0.1.0.tgz %})
