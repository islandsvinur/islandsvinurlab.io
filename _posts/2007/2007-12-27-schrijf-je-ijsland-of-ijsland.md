---
layout: post
title: 'Schrijf je "Ijsland" of "Ĳsland"?'
categories: [taal]
date: 2007-12-27 10:19:04 +0100
---
Het [antwoord](http://taaladvies.net/taal/advies/vraag/1188/) van de Nederlandse Taalunie: 

> Juist is Ĳsland. Als een woord dat met een ĳ begint met een hoofdletter moet worden geschreven, moet de ĳ in zĳn geheel in hoofdletters worden gezet.

De redenering is niet te begrĳpen, want het is -- zoals de regel is in de Nederlandse taal -- een uitzondering.

De oer-Nederlandse letter ĳ (ligatuur van i en j) is voortgekomen uit ii (net als we -- ook typerend voor de Nederlandse taal -- de digrafen ee, aa, oo en uu kennen), met als uitspraak ie. In het Fries bestaat de ii nog als zodanig en komt daar voor waar wĳ een ĳ schrĳven (zie tĳd -- tiid) en wordt de ĳ niet algemeen gebruikt (in [frije tiid](https://fy.wikipedia.org/wiki/Frije_tiid) zĳn de i en j deel van twee aparte lettergrepen).

In de middeleeuwen werd in handschriften de <span style="font-family: serif; font-style: italic;">i</span> gewoonlĳk zonder punt geschreven (<span style="font-family: serif; font-style: italic;">ı</span>), zodoende leverde dat <span style="font-family: serif; font-style: italic;">ıı</span> op. Vanwege mogelĳk verwarring met de letter <span style="font-family: serif; font-style: italic;">u</span> in handschriften (<span style="font-family: serif; font-style: italic;">ıı</span> vs. <span style="font-family: serif; font-style: italic;">u</span>) is besloten de tweede <span style="font-family: serif; font-style: italic;">i</span> te verlengen tot <span style="font-family: serif; font-style: italic;">ıȷ</span>. Na verloop van tĳd werd dit dus een <span style="font-family: serif; font-style: italic;">j</span> en zodoende was de nieuwe digraaf <span style="font-family: serif; font-style: italic;">ĳ</span> ontstaan.

Algemeen worden ligaturen als geheel gekapitaliseerd, zoals æ / Æ en œ / Œ. Daarom dus ook ĳ / Ĳ. Inmiddels is Ĳ geen echte ligatuur meer, maar twee losse letters geworden.

Dus nou wil ik geen Ijslands (uitspraak zoals het Ĳslandse Ísland?) meer zien he :P

**Opmerking**: Als je dit bericht leest vanaf de [planet](http://planet.luon.net/), klik dan even door, want diens webfeed verbouwt het zodanig dat een en ander slecht of zelfs totaal niet overkomt.
