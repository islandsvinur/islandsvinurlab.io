---
layout: post
title: "Whee! Natuurijs!"
date: 2007-12-20 22:17:17 +0100
---
Menig schaatsershart gaat harder kloppen als de temperatuur enkele dagen
onder nul raakt... Gaan we marathons rijden op natuurijs, komt er dit
jaar eindelijk weer een Elfstedentocht?

Die [marathon is
verreden](https://nl.wikipedia.org/wiki/Haaksbergen#Trivia), maar de [Tocht der Tochten](https://nl.wikipedia.org/wiki/Elfstedentocht)
moeten we voorlopig maar even vergeten (hoewel de zon er dit jaar wel [goed bij hangt](https://nl.wikipedia.org/wiki/Zonnecyclus)). Dat
weerhield ons er echter niet van om vandaag toch even het ijs op te gaan.

p(center). <img src="http://photos.luijten.org/Christian/2007/2007-12-20.%20Op%20natuurijs/.cache/400x266-IMG_7899.jpg" alt="" /> <br /> *Zie die enorme kniehoek... "Omlaag!" zou Bob zeggen of "Stay low!" aldus Chris*

p(center). <img src="http://photos.luijten.org/Christian/2007/2007-12-20.%20Op%20natuurijs/.cache/400x266-IMG_7916.jpg" alt="" /> <br /> *Het voltallige Isis-bestuur*

Wat voorzichtige stapjes, behoedzaam aftasten en dan... Schaatsen aan en het ijs op!

p(center). <img src="http://photos.luijten.org/Christian/2007/2007-12-20.%20Op%20natuurijs/.cache/400x266-IMG_7952.jpg" alt="" />

p(center). <img src="http://photos.luijten.org/Christian/2007/2007-12-20.%20Op%20natuurijs/.cache/400x266-IMG_7965.jpg" alt="" /> <br /> *Aan de houding kan je wel zien dat ik er nog niet al te gerust op ben ;-)*

Met dank aan [Paul](http://paul.luon.net/) voor het nemen van <a
href="http://photos.luijten.org/Christian/2007/2007-12-20.%20Op%20natuurijs/?width=600">de
meeste foto's</a> (en daarbij verkleumen van de kou).

![]({% link /image/photos/isis-kerstkaart.png %}) <br /> *De kerstkaart waar het allemaal om begonnen was*
