---
layout: post
title: "To Dresden"
categories: [reizen]
date: 2007-07-23 13:43:27 +0200
---
Got my [Interrail](http://www.interrail.com/) ticket today, on Thursday I'm leaving for Dresden to visit [Admar](http://luon.net/~admar/journal/) -- who really should post something new to his journal, by the way!

So I'm leaving on Thursday morning, 7:02 with the Intercity 1917 to [Venlo](https://en.wikipedia.org/wiki/Venlo_railway_station), wait 20 minutes, then with the RegionalExpress 9011 to [Düsseldorf Hbf](https://en.wikipedia.org/wiki/D%C3%BCsseldorf_Hauptbahnhof), wait some 40 minutes and then with the InterCityExpress 1759 straight to [Dresden-Neustadt](https://en.wikipedia.org/wiki/Dresden-Neustadt_railway_station), where I will arrive 10 hours later, save one minute, at 17:01.

I'm looking forward to it!
