---
layout: post
title: "IKEA-drukte"
date: 2007-12-27 14:11:11 +0100
---
Het traditionele tweede-kerstdag-uitje naar de IKEA werd dit jaar niet zo goed bezocht: [Drukte IKEA valt mee op tweede kerstdag](http://www.omroepbrabant.nl/?news/92004332/Drukte+IKEA+valt+mee+op+tweede+kerstdag.aspx)

Daarentegen wordt het niet-zo-traditionele derde-kerstdag-uitje naar de IKEA een stuk beter bezocht: [Drukte IKEA leidt tot sluiting afrit](http://www.nu.nl/news/1369445/10/rss/Drukte_Ikea_leidt_tot_sluiting_afrit.html). Opvallend overigens dat juist deze [IKEA in Delft gisteren dus dicht was](http://www.nu.nl/news/1367377/30/Ikea_Delft_met_kerst_dicht_uit_vrees_voor_chaos.html).
