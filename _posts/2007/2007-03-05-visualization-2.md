---
layout: post
title: "Fluids: My First OS X Application release"
categories: [studie]
date: 2007-03-05 23:40:21 +0100
---
Hooray! I actually managed today to get my [visualization]{% post_url 2006/2006-03-05-visualization-1 %}) compiled and running under Mac OS X. It wasn't that hard after all, just finding the right libraries to include.

## GLUT... X11 or Aqua?

glut.h exists in [fink](http://fink.sourceforge.net/) as well as in OS X, the former binds to X11, while the latter does the OpenGL directly in Aqua. When I found out about this, it was easy. I now only need Fink for the [fast fourier transform library](http://www.fftw.org/).

## NEXTSTEP Application Bundles

Then the next challenge came, creating a real application bundle... Again not very hard, it's just putting files in the right location and relinking the libraries.

So, now there is one little thing left, Universal Binaryness. But I can live with this result. It needs a pretty icon by the way.

![]({% link /image/study/streamlines-0.0.1.png %})

* [Fluids 0.0.1 powerpc]{% link /files/fluids/Fluids-powerpc-0.0.1.dmg %})
* [Fluids 0.0.1 intel]{% link /files/fluids/Fluids-i386-0.0.1.dmg %})
 	
I fixed some last bits in the streamlines and found a bug that was festering along all the time without really getting noticed. The source is included in the bundle, by the way.

Last stage, implementing isolines. Then I'll write a report about it all and then hopefully [after more than a year]{% post_url 2005/2005-12-29-trimester-4.2 %}) I can finish the [Visualization course](http://www.win.tue.nl/~alext/COURSES/INFO_VIS/).
