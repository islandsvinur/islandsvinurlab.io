---
layout: post
title: "Now also for Internet Explorer!"
categories: [admin]
date: 2007-08-22 18:24:39 +0200
---
I never thought I'd manage it to get the site display roughly (in the sense that it looks like it should look that way) the same on every single browser but tadaaa, here it is. [Look!](http://browsershots.org/http://luijten.org/)

Okay, so clearly now it's time to start thinking about a new design again :D
