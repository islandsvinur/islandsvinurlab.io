---
layout: post
title: "Lichtvervuiling"
date: 2007-04-03 21:59:01 +0200
---
Vergelijk deze foto:

![Nachtelijke hemel boven Snæfellsness, IJsland]({% link /photos/night-sky/snaefellsnes.jpg %})
{: .center}

*Nachtelijke hemel boven Snæfellsness, IJsland*
{: .center}

met deze:

![Nachtelijke hemel boven Eindhoven, Nederland]({% link /photos/night-sky/eindhoven.jpg %})
{: .center}

*Nachtelijke hemel boven Eindhoven, Nederland*
{: .center}

Beiden hebben exact dezelfde instellingen; 20 seconden belichting, f/3.5
diafragma, ISO 800. Dit is gewoon vreselijk en nu zie je gewoon objectief hoe
donker het is in IJsland en hoe licht hier in Nederland. Geen wonder dat we
het noorderlicht hier nooit zien, als het zich eens in de zuidelijkere
regionen laat zien.
