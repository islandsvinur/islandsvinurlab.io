---
layout: post
title: "Gullfoss panorama"
categories: [ijsland]
date: 2007-04-22 13:18:24 +0200
---
When we were at Gullfoss, the golden waterfall in the river Hvítá (White River), I took a series of photos. They weren't really intended as a panorama, but I could somewhat stitch them together like I did with the [Jökulsárlón panorama]({% post_url 2007/2007-04-03-jokulsarlon-panorama %}).
 	
![]({% link /photos/iceland/gullfoss-panorama-small.jpg %})

It has lots of stitching errors, there's even a person cut in half. Like last time, there is a [somewhat bigger version]({% link /photos/iceland/gullfoss-panorama-small.jpg %}).
