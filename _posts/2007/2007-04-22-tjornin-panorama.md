---
layout: post
title: "Tjörnin panorama"
categories: [ijsland]
date: 2007-04-22 16:30:46 +0200
---
This is the last panorama from Iceland I have, made on my last weekend there, when it after some days of frost suddenly started snowing and the frozen-over lake Tjörnin (The Pond) in the center of Reykjavík became a big snowfield.

![]({% link /photos/iceland/tjornin-panorama-small.jpg %})

On the far left, in the distance, the Askja building of the University of Iceland can be seen, where I had my introduction to the Icelandic language and culture in August. On the far right is the modern city hall of the city of Reykjavík.
