---
layout: post
title: "Back from Dresden"
categories: [reizen]
date: 2007-08-07 13:59:59 +0200
---
I'm back from my little Germany Interrail trip to Dresden, stopped over at Düsseldorf to visit family for a few days.

Dresden turns out to be a great city with lots of stuff to see and to do! Certainly worth another visit, I think. Thank you, Admar for some nice days.
 	<img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/400x266-014%20Dresden.jpg" alt="" /> <br /> [*Semperoper*](https://en.wikipedia.org/wiki/Semperoper)
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x184-016%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x107-016%20Dresden.jpg" alt="" /></a> <br /> * Panorama of the [Zwinger palace](https://en.wikipedia.org/wiki/Zwinger) *
 	<img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/400x266-023%20Dresden.jpg" alt="" /> <br /> * The beautifully restored ([picture of 1991](https://en.wikipedia.org/wiki/Image:Frauenkirche_Dresden_1991.jpg)) [Frauenkirche](https://en.wikipedia.org/wiki/Dresden_Frauenkirche) *
 	<img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/400x266-035%20Dresden.jpg" alt="" /> <br /> * Statue for the [Trümmerfrauen](https://de.wikipedia.org/wiki/Tr%FCmmerfrau) in Dresden *
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x131-031%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x76-031%20Dresden.jpg" alt="" /></a> <br /> * Altstadt panorama from the Carolabrücke *
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x142-110%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x83-110%20Dresden.jpg" alt="" /></a> <br /> * Altstadt panorama from the Marienbrücke *
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x360-074%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x210-074%20Dresden.jpg" alt="" /></a> <br /> <em> View over the [Elbtal](https://en.wikipedia.org/wiki/Dresden_Elbe_Valley) (on the [UNESCO List of World Heritage in Danger](http://whc.unesco.org/en/list/1156)) </em>
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x171-089%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x100-089%20Dresden.jpg" alt="" /></a> <br /> * [Blaues Wunder](https://en.wikipedia.org/wiki/Blaues_Wunder) panorama to the north *
 	<a href="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/1024x180-097%20Dresden.jpg"><img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/600x105-097%20Dresden.jpg" alt="" /></a> <br /> * Blaues Wunder panorama to the south *
 	<img src="http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/.cache/329x400-032%20Dresden.jpg" alt="" /> <br /> * [Ampelmädchen](https://de.wikipedia.org/wiki/Ampelfrau) *

More photos at [P.hot.os](http://photos.luijten.org/Christian/2007/2007-07-26.%20Interrail%20Germany/)!
