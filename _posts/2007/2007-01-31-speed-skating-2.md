---
layout: post
title: "News from the skating rink"
categories: [sport]
date: 2007-01-31 22:01:11 +0100
---
Since well over a year now I've [been speed skating regularly]({% post_url 2005/2005-11-01-began-speed-skating %}}), so maybe it is time for an update on the state of affairs up until now.

Last week, I got promoted to a higher training group, moving up from the D to the C group. This year, [Isis](http://www.essvisis.nl/) got only a few new members, so there was no direct need of an E group where I was in last year and I started in D right away when I came back from Iceland. So basically, this is my second promotion in one year :)

The groups differ mainly in the balance of technique and endurance exercises. Last wednesday was my first traning in my new group and we started right away with a warming up of 8 rounds "full speed" (which in this case means at about 70% of your power), 2 rounds rest, 8 rounds full, 2 rest, 6 full, 2 rest and another 6 full. In the D group, we hadn't done anything more than 6 at all... Ouch.

Another change from the lower groups is that you are supposed to take on at least one partner to ride along with and to learn from eachother. In D group, you usually did your exercises on your own.

So far on the news *on* the ice, along the track I wanted to do something back for the association, so I decided to do some work for the Crisis, the monthly newspaper.

And someone must have seen my Iceland pictures, because he asked me whether I would be interested in joining the photo commission. This would mean I get [photo gallery](http://www.essvisis.nl/nieuw/fotos/fotos.php) upload access at the Isis website and occasionally take pictures at activities of the club.
