---
layout: post
title: "Maí­stjarnan"
categories: [ijsland]
date: 2007-05-25 10:59:23 +0200
---
Here's another beautiful Icelandic song, [Maístjarnan](http://audio.luijten.org/iceland/maistjarnan.mp3) (the May star). By [Halldór Laxness](https://en.wikipedia.org/wiki/Halldór_Laxness), written in 1937.

> ó hve létt er þitt skóhljóð  
> og hve lengi ég beið þín,  
> þar er vorhret á glugga,  
> napur vindur sem hvín,  
> en ég veit eina stjörnu,  
> eina stjörnu sem skín,  
> og nú loks ertu komin,  
> þú ert komin til mín. 
>
> Það eru erfiðir tímar,  
> það er atvinnuþref,  
> ég hef ekkert að bjóða,  
> ekki ögn sem ég gef,  
> nema von mína og líf mitt  
> hvort ég vaki eða sef,  
> þetta eitt sem þú gafst mér  
> það er alt sem ég hef.
>
> En í kvöld lýkur vetri  
> sérhvers vinnandi manns,  
> og á morgun skín maísól,  
> það er maísólin hans,  
> það er maísólin okkar,  
> okkar einíngarbands,  
> fyrir þér ber ég fána  
> þessa framtíðarlands.
 
> Oh how light are your footsteps,  
> Oh how long have I waited for you,  
> There is spring rain on the window,  
> Cold whistling wind,  
> but I know one star,  
> one shining star,  
> and now finally are you coming,  
> you are coming to me. 	
>
> It are hard times,  
> there's not a lot work,  
> I've got nothing to offer,  
> not a bit which I give,  
> except my hope and my life,  
> whether I'm awake or asleep.  
> This one thing you gave me,  
> that is all I have.
>
> But tonight winter ends  
> for every working man,  
> and in the morning shines the May star,  
> that is his May sun,  
> that is our May sun,  
> our uniting bond.  
> For you I carry the flag  
> of this land's future.

The song being written in 1937, when Iceland was still a dependent nation under Danish flag -- the [Dannebrog](https://en.wikipedia.org/wiki/Flag_of_Denmark) -- I assume "the flag of this land's future" being a forward reference to the current flag and thus independence.

I'm not sure what the May star is, but I would say it is the sun, having returned with full force, starting to lighten up (and warm up!) the country after the hard and dark winter. Since a few days, Iceland doesn't have a [civil twilight](https://en.wikipedia.org/wiki/Civil_twilight) anymore for three months to come; the sun sets and doesn't go more than 6° under the horizon before climbing again for a new day.

**Update** I'm now a bit more educated and it's clear to me now that the May star is an obvious symbolism for socialism; "this land's future" referring to a future in socialism. [Halldór Laxness](https://en.wikipedia.org/wiki/Halldór_Laxness#1920s) was known to be attracted to socialism. He said that he "did not become a socialist in America from studying manuals of socialism but from watching the starving unemployed in the parks".
