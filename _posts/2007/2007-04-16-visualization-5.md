---
layout: post
title: "Visualization finished - VÃ¶kvar 1.0.0 released!"
categories: [studie]
date: 2007-04-16 22:41:39 +0200
---
Finally, after two years, I got my grade for the Visualization course today. Not that great, merely a seven, but well, it's done.

I decided that Fluids is a boring name, because it is just an English word. So I renamed it [Vökvar](http://digicoll.library.wisc.edu/cgi-bin/IcelOnline/IcelOnline.TEId-idx?type=entry&#38;eid=VO3KVI&#38;q1=vokvar), which also is just a word, but at least it's an Icelandic one — intrinsically cooler however — meaning exactly the same.

* [Vökvar 1.0.0 intel]{% link /files/fluids/Vokvar-i386-1.0.0.dmg %}) (dmg)
* [Vökvar 1.0.0 powerpc]{% link /files/fluids/Vokvar-powerpc-1.0.0.dmg %}) (dmg)
* [Vökvar 1.0.0 source code]{% link /files/fluids/vokvar-1.0.0.tgz %}) (tarball)
* [Report]{% link /files/papers/visualization - fluids.pdf %}) (pdf)

**Update**: I forgot that the usage is not at all trivial :)

So, here are some keystrokes that do stuff (they're also mentioned in the report):

* 1: Switches the "smoke" on and off 
* 2: Switches the speed vectors on and off 
* 3: Switches the streamlines on and off 
* 4: Switches the isolines on and off 
* p: Switches between palette (rainbow, rainbow with 5 discrete colours, grayscales) 
* o: Switches between isolines method (by value, by number, by point) 
* a: Temporarily freeze simulation and animation 
* f: Switches to and from fullscreen view 
* t/T: Changes timestep 
* s/S: Changes scale of vectors and streamlines 
* v/V: Changes viscosity 
* i/I: Changes number of isolines 
* q: Quits Vökvar
