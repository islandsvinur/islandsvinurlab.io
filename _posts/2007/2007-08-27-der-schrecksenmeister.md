---
layout: post
title: "Der Schrecksenmeister is here!"
categories: [ boek ]
date: 2007-08-27 22:34:21 +0200
---

![]({% link /image/der-schrecksenmeister.jpg %}){: .left}

[Der Schrecksenmeister -- Ein kulinarisches Märchen aus Zamonien von Gofid Letterkerl. Neu erzählt von Hildegunst von Mythenmetz](http://www.der-schrecksenmeister.de/) (
yes, it is a culinary fairy tale
by [a great poet](https://de.wikipedia.org/wiki/Die_Stadt_der_Tr%C3%A4umenden_B%C3%BCcher%23Die_Buchlinge), retold by
a [lizard-like novelist](https://de.wikipedia.org/wiki/Zamonien#Hildegunst_von_Mythenmetz), translated out of the
Zamonian language into German) is out! The newest book from [Walter Moers](https://en.wikipedia.org/wiki/Walter_Moers)
has finally arrived!

It didn't get as much attention as Harry Potter number 7 and the lines in front of the book stores weren't as big, but
I'm sure there are many fans of [Zamonian](http://www.zamonien.de/) literature (me included) who are happy that the wait
has come to an end. Or almost,
since [Amazon.de](http://www.amazon.de/Schrecksenmeister-Ein-kulinarisches-Letterkerl-Hildegunst-Mythenmetz/dp/3492049370/ref=pd_bbs_sr_1/302-0501611-1578457?ie=UTF8&#38;s=books&#38;qid=1188250815&#38;sr=8-1)
isn't too fast sending books to the Netherlands.
