---
layout: post
title: "Summer ice in Thialf"
categories: [sport]
date: 2007-07-02 20:11:28 +0200
---
Strangely weird to stand on ice skates in the [middle of summer](https://www.essvisis.nl/web/index.php?view=photo_album&id=191)... Even better to do so in [Thialf](http://www.thialf.nl/).

[![](https://www.essvisis.nl/fotoalbums/2007ZomerijsThialf/IMG_0370-s.JPG)](https://www.essvisis.nl/web/index.php?view=photo_album&id=191)

Also nice to skate on the "same" ice as [our heroes](https://nl.wikipedia.org/wiki/Categorie:Nederlands_langebaanschaatser) did [before us](https://nl.wikipedia.org/wiki/Wereldkampioenschap_schaatsen_allround_2007). It looks bigger on TV though, but then again, what doesn't.
