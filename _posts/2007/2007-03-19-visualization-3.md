---
layout: post
title: "Fluids, now with isolines!"
categories: [studie]
date: 2007-03-19 21:14:23 +0100
---
![]({% link /image/bytesmall.png %}) 

And we're already two weeks later! Today I hacked together isolines in Fluids, based on a [paper published in Byte magazine in July 1987](http://local.wasp.uwa.edu.au/~pbourke/papers/conrec/).

So, there it is, for Mac OS X in PowerPC and Intel glory:

* [Fluids 0.0.2 Intel version]{% link /files/fluids/Fluids-i386-0.0.2.dmg %})
* [Fluids 0.0.2 PowerPC version]{% link /files/fluids/Fluids-powerpc-0.0.2.dmg %})

And, another anouncement, over at [nogates.nl](http://nogates.nl/) I set up a [Trac](http://trac.edgewall.com/) page for the [Fluids project](http://nogates.nl/fluids/). There you can get the source and some more information.
