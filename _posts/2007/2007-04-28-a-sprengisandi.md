---
layout: post
title: "Go Mentos á Sprengisandi"
categories: [ijsland]
date: 2007-04-28 11:46:02 +0200
---
While it is not very uncommon to see advertisers go to Iceland to use the magnificient landscape as a backdrop for their productions ([Axe also does it](http://www.axe.nl/home/index.php?ID=commercials&#38;S=bomchickawahwah&#38;C)= for their self-tanning body lotion stuff by the way), it is pretty uncommon to hear Icelandic folksongs on Dutch television.

Mentos in their new series of demonstrating the freshness of their stuff show the people of Grundafjurdhuhurdhur (actually there is a town called [Grundarfjörður](http://www.grundarfjordur.is/) on the Snæfellsnes peninsula) involved in some refreshing bath in the fjord filled with Mentosessess (pun intended).

Another installment shows the refreshing of some guy's mouth by first throwing a bucket over his head to protect the others from the bad smell and then hanging him in some construction to put him headfirst in a tub filled with Mentosses.

Anyway, the song they sing at the end is a song about a horseback tour through the Sprengisandur desert. It's called [á Sprengisandi](http://audio.luijten.org/iceland/a-sprengisandi.mp3) and was written by [Grímur Thomsen](https://en.wikipedia.org/wiki/Gr%C3%ADmur_Thomsen) who lived from 1820 to 1896:
 
> Ríðum, ríðum og rekum yfir sandinn,  
> rennur sól á bak við Arnarfell,  
> hér á reiki er margur óhreinn andinn,  
> úr því fer að skyggja á jökulsvell;  
> Drottinn leiði drösulinn minn,  
> drjúgur verður síðasti áfanginn. 
>
> Þey þey! þey þey! Þaut í holti tóa,  
> þurran vill hún blóði væta góm,  
> eða líka einvher var að hóa  
> undarlega digrum karlaróm.  
> útilegumenn í ódáðahraun  
> eru kannski að smala fé á laun.  
> 
> Ríðum, ríðum og rekum yfir sandinn,  
> rokkrið er að síga á Herdubreið  
> álfadrottning er að beisla gandinn,  
> ekki er gott að verða á hennar leið  
> Vænsta klárinn vildi ég gefa til  
> að vera kominn ofan í Kiðagil.

The first strophe roughly translates to:

> Ride, ride, ride over the sand,  
> Run with the sun behind Arnarfell,  
> Bad spirits/winds come and go here.  
> Dusk is falling on the gletscher ice,  
> God leads my horse,  
> Long will be the last stage.

You won't find a Icelander that doesn't know this song. It's quite funny to hear this familiar tune in an advertisement, since we also had it at the Icelandic course last August as part of our cultural programme :-)
