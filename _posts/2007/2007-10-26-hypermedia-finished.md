---
layout: post
title: "Finally! Hypermedia Structures and Systems is Finished"
categories: [studie]
date: 2007-10-26 09:34:12 +0200
---
Yes! Finally I got around it and actually finished the work I started more than [two years ago]({% post_url 2005/2005-09-01-trimester-4.1 %}) (uh, correction, [almost two and a half years ago]({% post_url 2005/2005-07-06-holidays-2005 %}))!

The course is taught online, with a hyperdocument as course material. You first read your way through the hundred-something pages and a few small quizzes.

In the end you get the HTML'ized version of a (online) paper written in 1993 by Vijay Balasubramanian titled "State of the Art Review on Hypermedia Issues And Applications" and the assignment to convert it into a hypertext document.

In 1994 it was [converted to HTML](http://www.e-papyrus.com/hypertext_review/index.html) by Denys Duchier and now in 2007, for the [I](http://sandcat.nl/~bazkar/2L690/contents.html) [don't](http://www.meneerbruggeman.nl/2L690/hyperdocument/title.html) [know](http://hypermedia.reynout.nl/) [how](http://tue.markdehaas.nl/2l690/solution/introduction.html) [many'th](http://paul.luon.net/hypermedia/) time it is converted into an actual hypertext document. [So here's my take at it](http://hypermedia.luijten.org/).

Jæ, so I'm now only one course short of my Bachelor degree, which is that other course I "started" two years ago; History of Computing. I also am only one course and a Master project short of my Master degree, so it's actually looking like it's coming to an end now! More about the Master project in a few weeks.
