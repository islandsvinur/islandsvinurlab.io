---
layout: post
title: "Kijk ma, ik ben Ba.!"
categories: [studie]
date: 2007-12-18 12:27:18 +0100
---
![]({% link /image/bachelor-degree.jpg %})

Okee, niet helemaal, tis natuurlijk [BSc / BS](https://nl.wikipedia.org/wiki/Bachelor_of_Science), maar dat rijmt niet op ma.

Zo, nu nog een half jaartje en dan kan ik hopelijk 'Kijk ma, ik ben ook [Ma.](https://nl.wikipedia.org/wiki/Master_of_Science)' zeggen :)
