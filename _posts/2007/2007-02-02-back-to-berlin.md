---
layout: post
title: "Back to Berlin"
categories: [reizen]
date: 2007-02-02 15:25:10 +0100
---
<blockquote> Als das Kind Kind war, ging es mit hängenden Armen, <br /> wollte der Bach sei ein Fluss. Der Fluss sei ein Strom.  	Und diese Pfütze das Meer.
 Als das Kind Kind war, wusste es nicht, dass es Kind war. <br /> Alles war ihm beseelt und alle Seelen waren eins. </blockquote> 	With these words starts [Der Himmel über Berlin](http://www.imdb.com/title/tt0093191/), a movie by Wim Wenders (who is from Düsseldorf). It is set in 1987 Berlin, the divided Berlin. At that time cracks were already starting to show, the Cold War was thawing.

Since it's a West-German movie from before the Wende and I have only been in the former eastern part of the city (except for the Kreuzberg quarter and the Anhalter Bahnhof which [Peter Falk](http://imdb.com/name/nm0000393/) thought was a funny name), it showed an image of the the city which was new to me.

While we were watching it during our weekly [movie night](https://movienight.luon.net/?page=movies&#38;action=view&#38;movieid=35), it made me decide I want to go back to the city again -- I've been there a week in 1997 with a school exchange and returned in 2001 for holidays. This time to maybe see a bit more of the old western part.

It's always been in the back of my mind to return to the German capital city and this year it might just fit in the small eurotrip through Austria, Germany and Denmark I'm thinking of making...
