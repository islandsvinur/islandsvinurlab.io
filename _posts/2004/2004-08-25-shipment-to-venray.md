---
layout: post
title: "Bad news?"
categories: [apple]
date: 2004-08-25 00:26:15 +0200
---
<p>Hmm, the <span class="caps">TNT</span> Consignment Tracker gives me some second thoughts... Although both the tracking number of the and the web order number match mine, the order was going to Venray... And is now even delivered at someone named 'V Leuken'!</p> 	<p>I doubt that it is correct... Let's hear what Apple has to say about that.</p>