---
layout: post
title: "Prachtige time lapse van Eyjafjallajökull"
categories: [ijsland, vimeo]
date: 2010-05-18 21:41:45 +0200
---
Oneindig mooie [time lapse van Eyjafjallajökull](http://vimeo.com/11673745)

{% vimeo "https://vimeo.com/11673745" %}
