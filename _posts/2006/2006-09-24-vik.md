---
layout: post
title: Vík
categories: [photos]
---
<div class="pixels-photo">
  <p><img src="https://drscdn.500px.org/photo/1989950/m%3D900/29c6c8b887221059e6a354c2246a7527" alt="Photograph Vík by Christian Luijten on 500px">

  [Vík by Christian Luijten on 500px](https://500px.com/photo/1989950/v%C3%ADk-by-christian-luijten)
</div>
<script type="text/javascript" src="https://500px.com/embed.js"></script>
