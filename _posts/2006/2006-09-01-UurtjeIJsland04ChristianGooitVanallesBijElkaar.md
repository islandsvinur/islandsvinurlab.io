---
layout: post
title: "Uurtje IJsland #04: Christian gooit vanalles bij elkaar"
categories: [ijsland]
tags: [podcast]
date: 2006-09-01 18:55:43 +0200
---
Er staat Uurtje IJsland, maar dat is het niet. Ik kan het eigenlijk ook gewoon
niet categoriseren. Het is gewoon een verzameling muziek die ik kan waarderen,
uit verschillende genres en stromingen.

1. Einóma (.is) -- En Route
2. Dungen (.se) -- Panda
3. Efterklang (.dk) -- Chapter 6
4. Hilmar Örn Hilmarsson (.is) -- Litbriði
5. Yann Tiersen (.fr) -- The Deutsch Mark is Coming
6. Under Byen (.dk) -- Af Samme Stof som Stof
7. Howard Shore (.us) -- The Passing of the Elves
8. The Gathering (.nl) -- Solace
9. The Album Leaf (.us) -- Eastern Glow
10. Dungen (.se) -- Sol och Regn
11. Gingerbread Patriots (.us) -- Blood Beetle
12. Amiina (.is) -- Bláskjár
13. Yann Tiersen (.fr) -- Comptine d'un Autre été
14. Nightwish (.fi) -- Dead Boy's Poem
15. Sigur Rós (.is) -- Dögun
16. Sigur Rós (.is) -- Hún Jörð

Directe download: [hier](https://www.dropbox.com/s/5kzqcahyt93bssg/04%20%2304.%20Uurtje%20IJsland%2C%201%20juli%202006.m4a?dl=0)
