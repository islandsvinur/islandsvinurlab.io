---
layout: post
title: "Uurtje IJsland #03: Christian cast voor de laatste keer podmuziek"
categories: [ijsland]
tags: [podcast]
date: 2006-05-22 01:31:29 +0200
---
En dan nu voorlopig de laatste podcast van mij die alleen maar bestaat uit muziek. Nog éénmaal een tracklist en dan ligt de podcast voorlopig even stil.

1. Sigur Rós -- Hoppípolla
2. Sigur Rós -- Með Bloðnasir
3. múm -- Green Grass of Tunnel (linkerkanaal) / Grasi Vaxin Göng (rechterkanaal)
4. Björk -- Jóga (Howie B Main Mix)
5. Trabant -- Enter Spacebar
6. Silví­a Night -- Congratulations (het moet dan maar)
7. GusGus -- Northern Lights
8. Innvortis -- Dagurinn sem ég er ekki til
8. Sigur Rós -- Glósóli
9. Björk (met Tom Yorke) -- I've seen it all
10. Apparat Organ Quartet -- Global Capital
11. Björk -- Í dansi með þér
12. Sigur Rós -- Smáskifa

Het eindigt met hetzelfde als wat voorafging aan de allereerste set tijdens de
originele uitzending. Het was leuk, ik heb ervan genoten om het te doen, maar
er moet een einde aan komen. Ik hoop dat jullie een leuke indruk hebben
gekregen van de muziek uit IJsland. Natuurlijk is het bij lange na niet alles
wat er geproduceerd wordt, soms lijkt het wel alsof iedereen in een bandje zit
of tenminste één persoon kent die in een band speelt. De [site
rokk.is](http://www.rokk.is/) besteedt aandacht aan de lokale muziekscene en
niet alleen aan rock zoals de naam zou doen vermoeden.

Directe download: [hier](https://www.dropbox.com/s/w5tkmotvjppwake/03%20%2303.%20Uurtje%20IJsland%2C%2021%20mei%202006.m4a?dl=0)
