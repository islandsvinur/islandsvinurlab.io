---
layout: post
title: "Centraal Beheer doet het weer"
categories: [web-stuff]
date: 2006-06-17 21:01:26 +0200
---
Zit je net lekker een beetje TV te kijken, komt er een nieuwe reclame van Centraal Beheer voorbij. In eerste instantie is het gewoon de al wat oudere reclame van de kroongetuige die een nieuwe identiteit krijgt en vervolgens nationale bekendheid krijgt…

![]({% link /image/calluci-the-game.png %})

Nou, we kunnen nu *zelf* uit het huis ontsnappen zonder gezien te worden in [Calucci, the Game](http://actie.centraalbeheer.nl/commercialgame/caluccigame.htm). Briljant!

Voilà, nu doe ik voor de tweede keer mee aan een [viral marketing](https://en.wikipedia.org/wiki/Viral_marketing) truuk :)
