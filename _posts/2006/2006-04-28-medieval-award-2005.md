---
layout: post
title: "Received my Medieval Award 2005"
date: 2006-04-28 19:17:10 +0200
---
Alright, so I got this Medieval Award the other day. Medieval? Award? Doesn't really ring a bell right… Okay, so Medieval is a MOO, which can be seen as an online community avant-la-lettre; it's really lo-tech in today's Internet technology. So far, so good.

I created this together with the other 'Guardians' quite a while ago in 1999. We were friends from Breda and The Hague and we wanted to create an online world set in the Middle Ages, using our and other's fantasy as guideline. Our world grew and grew and at some point caught the attention of a whole slew of Pijnacker-persons. Now, the community strangely enough is grown quite close to eachother, despite the fact that most users wouldn't be compatible at all in "real life" (I put it between quotes because it seems to turn out the way we feel 'what real life is' is changing).

![]({% link /image/photos/medieval-award-2005.png %})  
*Medieval Award 2005*
{: .center}

I got the award already begin April, on Medieval's birthday meeting, but I couldn't/wouldn't come no matter how much they pushed (well, now I know why they did :)). Anyway, this award I received in the mail today was for the many years of administration of the server and database. I really appreciate this gesture since the system administrator is often forgotten. It encourages me to continue what I'm doing. Ah, and I should mention the fact that Peeg and DarkShadow also received the award for their work to keep Medieval up and running.
