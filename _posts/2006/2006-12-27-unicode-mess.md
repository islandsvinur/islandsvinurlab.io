---
layout: post
title: "Unicode mess"
categories: [geek-stuff]
date: 2006-12-27 23:41:38 +0100
---
![]({% link /image/rants.gif %}) Hurray!

You might not agree (along with me by the way), but i&#xCC;&#x81; (decomposition, Apple uses this in OS X) and &#xC3;&#xAD; (UTF-8), &#xED; (UTF-16 and UTF-32) and í (HTML Entity) should result in displaying the same characters. I am actually talking about the Unicode character called ['LATIN SMALL LETTER I WITH ACUTE'](http://www.fileformat.info/info/unicode/char/00ed/index.htm)

We can be glad that there are many different ways to enter the same character and that there is no single way to actually enter the original character and then conclude that we actually want to see the resource under the same name but written in a different way. Who cares whether it is one character or a combined character‽

Thank you Unicode! AARGH!

P.S. If you're wondering what I'm ranting about, these three URLs should go to the same page, two of them don't:

* [One](http://photos.luijten.org/Christian/Iceland/021.%20Trip%20to%20V%C3%ADk/)
* [Two](http://photos.luijten.org/Christian/Iceland/021.%20Trip%20to%20Vi%CC%81k/)
* [Three](http://photos.luijten.org/Christian/Iceland/021.%20Trip%20to%20V%EDk/)

The nice thing that Safari manages to show the correct name in the status bar, so with resolved names for links One and Two -- the latter one shifted up one pixel, very elegant (?) -, but not for Three, where I just see %ED in the URL. Ironically, this is the only character which actually shows the i acute correctly in the paragraph above. This is way above my head, many different systems at work at the same time, all trying their best to do *something*.
