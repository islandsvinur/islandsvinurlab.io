---
layout: post
title: "Slimme Marketing van Simyo"
categories: [web-stuff]
date: 2006-03-19 16:59:12 +0100
---
(Dutch only)

Rijd je hier door de stad, zie je overal posters van simyo hangen, met daarover stickers met teksten "Red de prepaid beller" en "Stop simyo"... Een bezoekje aan de website suggereert dat het hier gaat om een stichting die begaan is met het lot van de verdwijnende prepaidbeller. Robert Long (kent u hem nog?) spoort de bezoeker aan om zijn boodschap door te sturen naar anderen. Op de hele site geen referentie aan het moederbedrijf van de 'stichting' én van Simyo: KPN Mobile. Leve [whois](http://www.domaininformation.de/whois/reddeprepaidbeller.nl)!

Dus zó moet je reclame maken, door anti-reclame van jezelf eroverheen te plakken en je klanten te belazeren!
