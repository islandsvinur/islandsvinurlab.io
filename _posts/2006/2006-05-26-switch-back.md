---
layout: post
title: "Switch back"
categories: [apple]
date: 2006-05-26 11:58:41 +0200
---
I'm switching away<sup>[1](#fn1)</sup> from [PowerPC](http://www.apple.com/powermac/)… Yesterday I already installed Linux on my [SGI O2+](http://www.sgi.com/products/remarketed/o2plus/) (MIPS R5000 architecture), today my all new and shiny white [MacBook](http://www.apple.com/macbook/macbook.html) (Intel x86) arrived!

You know how hard it is to to shut up to anyone about it for a whole week just to make this surprise post? Especially when you're going to the Apple Centre with the whole bunch ;-)

![]({% link /image/photos/switch-back/IMG_3619.png %})

![]({% link /image/photos/switch-back/IMG_3620.png %})

![]({% link /image/photos/switch-back/IMG_3621.png %})

![]({% link /image/photos/switch-back/IMG_3622.png %})

![]({% link /image/photos/switch-back/IMG_3623.png %})

First impression: Nice thing! Also ordered the mini-DVI-to-DVI cable, so I can hook it up my Dell 2405FPW and enjoy the full 1920×1200! Apple removed the stupid restriction that the consumer level notebooks could only mirror the screen on the external screen, you can now also expand your desktop.

<p id="fn1"><sup>1</sup> Well of course I'm only joking about switching away from my PowerMac, but it sounded cool ;-)
