---
layout: post
title: "Uurtje IJsland #02: Christian cast toch nog een keer Pod"
categories: [ijsland]
tags: [podcast]
date: 2006-05-10 23:11:56 +0200
---
Ja, dan blijkt het eigenlijk toch best leuk te zijn om anderen je
muziekverzameling te laten horen en tegelijk een beetje creatief naar je muziek
te luisteren. Toegegeven, heel geweldige mixes zijn het niet maar het houdt je
van de straat he. Omdat ik het dus niet kon laten heb ik een tweede set
gemaakt.

1. Eivør Pálsdóttir -- Brostnar Borgir
2. Emiliana Torrini -- To be free
3. Björk -- Isobel (remix door Deodato)
4. Björk -- Oliver
5. GusGus -- Barry A/B (remix)
6. Trabant -- Superman
7. Sigur Rós -- 180 Sekúndur Fyrir Sólarupprás (remix door Curver)
8. GusGus -- Gun Acid (remix)
9. The Sugarcubes -- Hetero Scum
10. Sigur Rós -- Fönklagið (live in Reykjavík 1998)
11. Innvortis -- Hvað er þetta helvítis eitthvað
12. Sigur Rós -- Hafsól '97 vs. Hafsól '05 (Mithrandir mix)

Hafsól is één van de allereerste nummers van Sigur Rós en is een van die
nummers die blijven gespeeld worden en in de loop van de tijd een complete
evolutie meemaken. Ik heb geprobeerd beide versies in deze set onder te brengen
en dé manier is natuurlijk om ze te mixen.

Directe download: [hier](https://www.dropbox.com/s/ehdqlg7srb2tqs6/02%20%2302.%20Uurtje%20IJsland%2C%2011%20mei%202006.m4a?dl=0)
