---
layout: post
title: "Kaizers Orchestra In Effenaar"
categories: [muziek]
tag: [noorwegen]
date: 2006-03-13 18:02:03 +0100
---
![]({% link /image/photos/poster-kaizers-orchestra.png %})

Great concert, little short maybe, but well… Making it longer won't make it long enough :)

The setlist was something like this (taken from a gig in Danmark, changed where I knew the differences):

* KGB
* Delikatessen
* Knekker deg til sist
* Hevnervals
* Container
* Señor Flamingos Adieu
* Blitzregn Baby
* Bøn Fra Helvete
* Mann Mot Mann
* Kontroll På Kontinentet
* Christiania *(yes, the self-governed community in Denmark, but also the former name of Oslo)*
* På Ditt Skift
* Die Polizei *(request from audience)*
* Di Grind
* Dieter Meyers Institution
* Evig Pint
* Ompa til Du dør
* Maestro

Reprise:

* Mr. Kaizer… 
* Bak et Halleluja
* Resistansen

Thanks to Lotte, Sjoerd, and Bram for joining me. Ah, and Lowlands is a no go, since I'll be in Iceland by then after all (I thought Lowlands was in July).

And I found some English [translations](http://kaizers.de/x_kontroll.php) to the [lyrics](http://kaizers.no/articles/4/9/document38.ehtml)!
