---
layout: post
title: "Finally: Almost full Switch to Linux"
categories: [apple]
date: 2006-03-02 15:31:53 +0100
---
Finally, after having Hoefnix for more than a year, I'm booting Linux on my Mac more and more often. There were a few reasons I sticked to OS X, one of them was I didn't really had a reason to over to Linux; everything worked for me… Well, sort of, I'm getting fed up with looking for warez and shit while there is an enormous repository of Free Software in the world just waiting to be discovered and used.

The course of Visualization led me back to Linux, since the libraries that I needed aren't available in [DarwinPorts](http://www.darwinports.org/) at the time and they are in Debian, which is just so much easier. Gotta love a good package management system :-)

So now I'm dualbooting again, like in the old days with Windows and Linux. Basically for all the imaging stuff I use OS X. Ah, and for [Democracy TV](http://www.getdemocracy.com/) of course (Linux player still in very early alpha).
