---
layout: post
title: "Bieslog's tunneltje"
categories: [web-stuff]
date: 2006-04-24 23:50:15 +0200
---
Wim de Bie, een van de weinige mensen die van niets werkelijk iets kunnen maken (en daarmee sluit ik nu dus Hart van Nederland en kopiÃ«en uit, want die kunnen van niets niet werkelijk iets maken).

Goed, in ieder geval, op 23 november 2004 postte hij een berichtje op z'n Bieslog dat hij een link van iemand kreeg toegestuurd waarop het beeld van een webcam te zien was wat de uitgang van een tunneltje in Noorwegen te zien was. Dat is dan ook verder het enige, er is inderdaad een tunneltje te zien, beetje gras en natuurlijk de weg die eruit komt.

Wat krijgt De Bie vervolgens voor mekaar? Dat er een stel Nederlanders intens naar 'hun' cameraatje beginnen te kijken. Opeens heeft een oninteressant tunneltje in het Noorse achterland een schare trouwe fans en zodra er ook maar iets ernstigs gebeurd met de camera slaan ze alarm (zo is de camera al een paar keer van URL veranderd en ook meerdere dagen uitgevallen)!

[Interessant kijkvoer voor de maandagavond die nu langzaam dinsdagnacht begint te worden](http://bieslog.vpro.nl/programma/bieslog/binders.jsp?maps=4328074&#38;magazines=4328075&#38;binders=15117647).
