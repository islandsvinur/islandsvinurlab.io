---
layout: post
title: "Changing Apps"
categories: [apple]
date: 2006-07-19 10:28:55 +0200
---
While my stays in Linux (Ubuntu 6.06 currently) still are very brief, mainly checking some things out and then quickly rebooting to OS X.

All in all you could say I'm very much hooked to OS X, it only has one very big drawback: You have to pay for many pieces of software… Beh, don't like that ;-)

However, there are quite a few very good Free Software apps available, libgaim based IM client [**Adium**](http://www.adiumx.com/) being one of them. Safari is a nice browser, but some things just work better on a Gecko based browser like [**Camino**](http://caminobrowser.org/) and today, I found a freeware newsreader I really like: [**Shrook**](http://www.utsire.com/shrook/). It is not Free yet, but it's getting closer :-)

Actually, the reason I wanted to ditch NetNewsWire which I previously used was that it wasn't a Universal Binary and somehow apps using [Rosetta](http://www.apple.com/rosetta/) take up all your system resources and make it dead slow to the point that clicking the NNW-icon keeps you waiting for about half a minute before anything at all happens. And it isn't limited to NNW also, other apps become slow too.

Shrook has a few interesting features I haven't seen on other readers. Firstly, there is this thing called *Distributed Checking* which uses the fact that other Shrook users probably have some channels in common and if one of them has an update of the channel, this fact is submitted to Shrooks server and then pushed through to the other subscribers. So basically it is an implementation of push in a pull technique!

Secondly it is possible to synchronize your subscriptions with the Shrook server and in this way keep track of them on other machines as well (along with their article's read and flag status!).
