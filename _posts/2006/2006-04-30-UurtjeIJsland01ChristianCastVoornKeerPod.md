---
layout: post
title: "Uurtje IJsland #01: Christian cast voor één keer Pod"
categories: [ijsland]
tags: [podcast]
date: 2006-04-30 14:51:55 +0200
---
Dit is mijn eerste en waarschijnlijk laatste podcast. In ieder geval de laatste
die alleen maar muziek bevat, misschien worden latere posts vanuit IJsland ook
in podcast-vorm uitgebracht, maar dat moet ik dan nog maar zien.

Ik heb een kleine selectie uit mijn IJslandse muziek gemaakt om een idee te
geven hoe anders 'anders' nou is waar iedereen het over heeft als
ze het over de muziek hebben. Ik vind het zelf wel meevallen, het is alleen
behoorlijk vrolijk, ja, punk is er wat later aangekomen en heeft meer invloed
gehad op de muziek die later zou komen.

1. Sigur Rós -- Rafmagnið Búið
2. Sigur Rós -- Ný Batterí
3. Björk -- Bachelorette
4. Trabant -- Bahama Banana
5. GusGus -- Ladyshave
6. Sykurmolarnir -- Eat The Menu (IJslandstalige versie)
7. Apparat Organ Quartet -- Romantica
8. Ég -- Evrópukeppnin
9. Efterklang -- Prey And Predator (even smokkelen, Efterklang is namelijk een Deense groep)
10. Ég -- Santana kl. 9 um morguninn
11. Sigur Rós -- #8, a.k.a. Popplagið

Directe download: [hier](https://www.dropbox.com/s/iw811bbrk951q9x/01%20%2301.%20Uurtje%20IJsland%2C%2030%20april%202006.m4a?dl=0)
