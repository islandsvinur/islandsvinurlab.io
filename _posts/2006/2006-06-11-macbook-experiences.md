---
layout: post
title: "MacBook experiences"
categories: [apple]
date: 2006-06-11 10:43:14 +0200
---
After two weeks and two days with my MacBook, I think I can come to some conclusion about it.

Firstly, I called it Idéfix, after Obelix' dog since it is also white and black (the screen is really black compared to the white of the enclosure) and it goes with his master on all of his adventures, like how mine is going with me to Iceland.

## Heat problems

None. Really, I don't know what the others are doing to it, but mine isn't getting hotter than any laptop I've had my hands on. Unlike my old Fujitsu-Siemens, the keyboard hardly gets hot. The area around the tab-key is the hottest, while the right side actually remains quite cool.

In the past week, temperatures steadily rose to around 30 degrees and it was noticable on Idéfix. The little fellow can't really get rid of its own heat, so it now gets hotter than normal.

## Speed

It is fast, really fast: Converting a DVD to MPEG-4 is twice as fast as on Hoefnix, my Power Mac G5 2 x 2.5 GHz. It manages to get 60 fps while Hoefnix only gets 30. That isn't a daily task of course (well, for some it might be), but overall, it feels snappier than the Power Mac.

![]({% link /image/photos/switch-back/Handbrake.png %})  
 *Handbrake converting Forrest Gump DVD into an MPEG-4*
{: .center}

## Battery life

Better than my Fujitsu, but that goes without saying since that battery is broken :-) But okay, I already got four hours, with Bluetooth and AirPort turned off.

## Critics

Yes, tiny thing, namely that it could be finished better! The keyboard leaves a grease mark on the screen, not very nice and something the Fujitsu didn't have (one of the few good things about that one).

The polycarbonate case is a bit wobbly at some places, nothing terrible, but I think the finishing on that could be a bit better. One screw isn't fastened deep enough, I tried to give it an extra turn but it won't go in further.

## Further stuff

The first days I took it with me in its protective cover (see picture).

![]({% link /image/photos/switch-back/IMG_3622.png %})  
*Idéfix nicely packed in its box*
{: .center}

The second day it teared open and it was also raining, so I added a plastic bag of the Media Markt. The reason I didn't have a proper bag for it is that they simply don't exist yet. The form factor isn't new, but apparently if a notebook isn't an Apple, it isn't worth making a sleeve for according to the manufacturers… Because I cannot do without, I decided to get me a not-so-expensive one which doesn't fit exactly and look for a good one at a later point.

![]({% link /image/photos/switch-back/IMG_3781.png %})  
*Tucano Second Skin 14*
{: .center}

Yes, it says iBook. Yes, it is for a 14 inch model. Yes, a MacBook fits in it, if you add a piece of insulation tube :-)

![]({% link /image/photos/switch-back/IMG_3782.png %})  
*Insulation tube*
{: .center}
