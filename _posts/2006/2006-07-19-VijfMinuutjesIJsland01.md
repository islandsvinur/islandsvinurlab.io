---
layout: post
title: "Vijf minuutjes IJsland #01"
categories: [ijsland]
tags: [podcast, vlog, vimeo]
date: 2006-07-19 12:41:35 +0200
---
Goed, in een poging hip te doen zal ik me maar eens overgeven aan het 
[videobloggen](https://nl.wikipedia.org/wiki/VODcast). Eens kijken
of het wat voor mij is. Helaas zit m'n camera vast aan m'n
computer, dus onderweg filmen zal er nooit echt in zitten. Veel meer dan
mijn hoofd en af en toe wat foto's zal er dan dus ook niet te zien
zijn.

{% vimeo https://vimeo.com/4044205 %}

Mijn excuses voor het enorm lage geluidsniveau, ik kreeg het niet harder
dan dit. Een draai aan de volumeknop moet het euvel maar even tijdelijk
oplossen. Volgende keer (mocht die komen) zal ik er beter op letten. Een
reactie via de website stel ik zeker op prijs!
