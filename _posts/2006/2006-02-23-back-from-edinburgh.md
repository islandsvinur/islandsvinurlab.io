---
layout: post
title: "Back from Edinburgh (and more)"
categories: [reizen]
date: 2006-02-23 14:57:42 +0100
---
Yes, well, actually it's been almost a month now since we came back from Edinburgh, but I didn't want to post without [pictures being online](http://photos.luijten.org/Christian/2006/01/) and I had to edit them plus I didn't have a decent piece of photo gallery software.

But now I do! It is called [p.hot.os](http://p.hot.os.luijten.org/) and it is going to be my photo gallery page, so here it is, the post on Edinburgh.

Actually, *p.hot.os* is nothing more than [webgen](http://webgen.rubyforge.org/) with a custom gallery layouter (and you can even do without that one). So, basically, I now use webgen for my gallery needs! Please tell me what you think of it, I still have some ideas to implement (for instance something with Google Maps, like most of you probably already saw in my Coppermine gallery).

So, back to Edinburgh… Uhm, I'm back *from* Edinburgh and it was a great weekend. We saw a lot of the city and got a hint of its surroundings, [took a lot of pictures](http://photos.luijten.org/Christian/2006/01/), but didn't get on the 41 however. I'd like to thank you, Paul, for your hospitality and hope to see you soon (in a few minutes, since you're coming home at the moment I'm writing this).
