---
layout: post
title: "Boten Anna"
categories: [web-stuff]
date: 2006-12-22 12:07:56 +0100
---
![]({% link /image/boten-anna.png %}) Nee, het gaat niet om een Zweeds liedje wat te vaak is gecoverd, maar om de website van IKEA (ook Zweeds). Je kan [Anna namelijk allerlei vragen stellen](http://193.108.42.79/cgi-nl/IKEA_NL.cgi) over IKEA. Heel leuk, maar nu vraag ik me wel af, wie heeft nu wie geïspireerd en kan ze je ook zo hard verbannen ("och hon kann banna, banna dig så hård")?

Zo, de eerste post hier sinds ik weer terugben, met meer in het vooruitzicht.
