---
layout: post
title: "Vijf minuutjes IJsland #02: Aurora Borealis op Snæfellsnes"
categories: [ijsland]
date: 2006-10-27 01:08:37 +0200
tags: [youtube, podcast, vlog]
---
Wel, zo ziet het eruit als je alle foto's achter elkaar zet!
 	
{% youtube "http://www.youtube.com/v/jp8yAyrD-5U" %}

Ik heb ook een [grote versie (640×480, h.264)](http://photos.luijten.org/Christian/Iceland/Aurora%20Borealis.m4v) online staan voor de [podcast-volgers](http://ijsland.luijten.org/category/podcast/) (volgens mij niemand).
