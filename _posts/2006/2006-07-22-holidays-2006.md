---
layout: post
title: "Holidays 2006"
date: 2006-07-22 09:41:51 +0200
---
Heh, I just read [my holiday 2005 planning]({% post_url 2005/2005-07-06-holidays-2005 %}) and apparantly I didn't do a very good job last year. Most of the projects still aren't finished :-)

However, something of a little more importance has come between them since I'll be [going to Iceland](http://ijsland.luijten.org/) in a few days. So, no History of Computing, no Hypermedia, no Linux, no LEDR, no Miriapodo for a while.
