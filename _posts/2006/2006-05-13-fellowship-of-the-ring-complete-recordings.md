---
layout: post
title: "Fellowship of the Ring: Complete Recordings"
categories: [muziek]
date: 2006-05-13 22:51:33 +0200
---
Extreme bargain alert! I got this 3CD plus a DVD with the complete recordings of The Fellowship of the Ring at a ridiculous price of € 5,95 at Makro… Something of a 90% discount off the Amazon-prize. Someone really must have messed things up since I've seen it at said Makro for something around € 40,- not too long ago.

![]({% link /image/photos/fotr-complete-recordings.png %})

Contents:

* A booklet with information on the various themes in the music, certainly an interesting read.
* 3CDs with the complete musical sound track of the first of the Lord of the Rings movies.
* 1DVD which contains the same music, only now in four formats: standard Dolby Digital Stereo and Surround and in 48kHz 24bit Advanced Resolution Surround and Stereo Sound. Anyone who can play DVD-Audio?

It seems that software to play the DVD-Audio portion (it's what goes in the AUDIO_TS directory on a DVD) simply doesn't exist. One has to have a hardware player for it, but now the same hardware players are dropping support for it in favour of HD content.

I really enjoy this "Extended Edition" of the soundtrack, it's fun to picture the complete film along with the music :)
