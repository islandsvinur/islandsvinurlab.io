---
layout: post
title: "Schumacher Alt"
date: 2006-04-06 23:49:17 +0200
---
Hmmmmm, nothing's better (for a drink I mean) than a nice cold [Altbier](https://de.wikipedia.org/wiki/Altbier), especially when it is from Düsseldorf.

![]({% link /image/photos/schumacher-alt.png %})  
[*Schumacher Alt*](http://de.wikipedia.org/wiki/Brauerei_Schumacher)
{: .center}

The drinking of Altbier is during carnaval often accompanied by the singing of the Altbier song: "...Ja, sind wir im Wald hier? Wo bleibt unser Altbier? Wir habben in Düsseldorf die längste Theke der Welt…"
