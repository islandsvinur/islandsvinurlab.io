---
layout: post
title: "Iceland Blog moved off Planet"
categories: [admin]
date: 2006-08-26 18:41:58 +0200
---
Because it was decided that Pluto no longer is a planet, but rather a dwarf planet, I on my part decided to take the Iceland blog off Planet Luon.

Really? Well… No. The actual reason is that Planet destroys the markup of my (admittedly invalid) RSS feed. Apparantly, the HTML that can be encoded inside the RSS feed, is not just normal HTML, but some other form where the object and iframe tags are illegal. My own feed reader however renders them well, because it is actually a web browser, right? So, why would you disallow those things? I want to do hip new things in my feed! Don't hold me back!

So I ask of you, readers subscribed via Planet Luon, add [http://ijsland.luijten.org/feed/](http://ijsland.luijten.org/feed/) to your readers. The site is now removed from Planet Luon.
