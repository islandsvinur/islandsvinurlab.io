---
date: 2020-10-09 20:48:28 +0200
title: Python in dronevlucht
layout: post
tags: [youtube, video, efteling, drone]
---

Ik had deze even gemist op het Efteling YouTube-kanaal, maar na de in mijn ogen wat [tegenvallende drone-video](https://www.youtube.com/watch?v=yDXb5o-bO5g) van afgelopen april hebben ze eind augustus een *hele mooie video van de Python in dronevlucht* neergezet!

{% youtube "https://www.youtube-nocookie.com/watch?v=83DvAOw7jJE" %}
