---
date: 2020-12-03 20:50:43
title: Stop motion met LEGO
layout: post
tags: [youtube, video, lego, stop motion]
---

Jaren geleden of beter gezegd, in de vorige eeuw, speelde ik veel met LEGO. Ik
maakte hele verhalen van straten met huizen en mensen en autootjes. We hadden
thuis ook een [Super 8 camera](https://en.wikipedia.org/wiki/Super_8_film), die 
ook beeldje voor beeldje kon belichten. Het zijn de ingrediënten voor een
fantastische stop-motion blockbuster.

Helaas waren de Super 8 films niet goedkoop en het ontwikkelen werd begin jaren
90 ook al lastiger. Die filmcarrière is dus aan mij voorbij gegaan.

Zo fijn dat de huidige generatie zich daar geen zorgen meer over hoeft te maken
en gewoon lekker kan creëren met alle digitale hulpmiddelen in één enkel
apparaat. Ontzettend leuk!

## De Spokende School

Jasper heeft deze film helemaal zelf gemaakt:

{% youtube "https://www.youtube.com/watch?v=bIQKIPZoKZU" %}

## Het Spooklab

En ik heb Noud wat geholpen met deze:

{% youtube "https://www.youtube.com/watch?v=K1ETjBqDn9s" %}
