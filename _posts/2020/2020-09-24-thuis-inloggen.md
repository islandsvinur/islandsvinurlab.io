---
date: 2020-09-24 09:07:28 +0200
title: Thuis inloggen
layout: post
---

Ik heb thuis een NAS van Synology, die deels bereikbaar is vanaf het internet. 
Soms wil ik een van de apparaten op mijn thuisnetwerk bereiken die dat niet 
zijn. Ze kunnen zelf wel verbinding maken met het internet, maar andersom is 
niet direct mogelijk.

Een van de oplossingen die je vooral in zakelijke netwerken ziet is de VPN. Je 
logt in op de VPN server van je bedrijf en vervolgens kun je gebruikmaken van
alle diensten alsof je direct op kantoor met het interne netwerk bent verbonden.

Voor de thuisgebruiker is dat niet altijd een optie. Niet alle (routers van) 
providers laten de benodigde protocollen zomaar door en de configuratie is ook 
redelijk complex.

Gelukkig is er een "eenvoudige" ad-hoc methode die voor nu-en-dan-gebruik
meer dan volstaat. Door gebruik te maken van een van de features van OpenSSH, 
wat je wellicht al gebruikt om in te loggen op je NAS of je router.

Het is mogelijk om een SSH-verbinding te openen en over die verbinding een
TCP-verbinding te tunnelen. Je geeft een poortnummer op voor je lokale
machine en het poortnummer waarmee je wil verbinden. Vervolgens maak je
verbinding op de lokale poort, waardoor al het verkeer door de tunnel gaat en
er aan de andere kant weer uit komt.

Dit is allemaal een beetje abstract, dus laten we het concreter maken. 

Zoals gezegd heb ik een NAS van Synology, die bereikbaar is via 
`luijten.mysyno.info`. De enige bereikbare poort is 22 voor SSH; hiervoor heb 
ik in mijn router een port-forward ingesteld.

### Verbinden met een HTTP server op je NAS

Op mijn NAS staat ook een installatie van OpenHAB - een home automation systeem. 
Dit heeft een HTTP user interface op poort 8080 (dus `localhost:8080` vanaf de 
NAS gezien). Deze poort is vanaf het internet (gelukkig!) niet bereikbaar.

Stel ik ben op kantoor, dan kan ik die OpenHAB dus niet bereiken en dus ook 
niet het licht uitzetten als ik dat vergeten was. Omdat ik echter wel kan 
verbinden met SSH, zet ik een *lokale* tunnel (`-L`) op van een vrije poort op 
mijn machine, zoals `8000` naar `localhost:8080` op het andere eindpunt (bezien 
*vanaf dat eindpunt*!).

```
ssh -L 8000:localhost:8080 islandsvinur@luijten.mysyno.info
```

Nu open ik mijn browser naar <http://localhost:8000/> en krijg ik de interface 
van OpenHAB thuis te zien! Niemand anders dan mijn machine kan die verbinding 
maken en zodra ik het commando afsluit is de tunnel ook verdwenen en werkt 
bovenstaande link meteen niet meer. Dit is dus een veel veiligere manier om een 
dienst beschikbaar te maken dan om de poort permanent en voor iedereen te 
forwarden in je router.

### Het scherm van een computer in je netwerk overnemen

In het vorige voorbeeld staat OpenHAB op dezelfde machine als de SSH server, 
namelijk de NAS. Het is echter ook mogelijk om te verbinden met andere machines 
in het netwerk, bijvoorbeeld met VNC, Screen Sharing of Remote Desktop naar je 
andere computer.

Ik heb een laptop genaamd [Skaftafell](https://nl.wikipedia.org/wiki/Skaftafell) 
en een desktop genaamd [Þingvellir](https://nl.wikipedia.org/wiki/Þingvellir). Die 
namen zijn in te stellen in de "Sharing" Preferences of "Delen" Voorkeuren van 
macOS, zie hieronder. Een soortgelijk scherm is er ook in Windows.  

![]({% link /image/2019-12-29-sharing-macos.png %})

Onder de computernaam zie je ook de "hostname" van de computer in het netwerk. 
Die gaan we zometeen nodig hebben. 

In dit scherm kan je ook de Schermdeling inschakelen, zodat je verbinding kan 
maken met Apple's Remote Desktop of met een algemene VNC tool.

Ik start weer een tunnel, dit keer op de lokale poort `5900` naar 
`skaftafell.local:5900`. Waarom 5900? Dat is de poort waarop VNC 
normaalgesproken luistert.  

```
ssh -L 5900:skaftafell.local:5900 islandsvinur@luijten.mysyno.info
```

Nu kan ik verbinden met <vnc://localhost> en voila, de verbinding met 
Skaftafell wordt opgebouwd. Vervang het doel van de tunnel:

```
ssh -L 5900:thingvellir.local:5900 islandsvinur@luijten.mysyno.info
```

En nu gaat <vnc://localhost> opeens naar Þingvellir.

## Conclusie

Voor nu en dan inloggen op je thuisnetwerk is een VPN server overkill en kan 
je gebruikmaken van de tools die je al op je apparaten hebt staan, of die vrij
beschikbaar zijn.

Deze post is vooral bedoeld als een aantekening voor mezelf, en een 
samenvatting van [een antwoord op Unix & Linux Stack Exchange](https://unix.stackexchange.com/a/115906).
