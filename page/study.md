---
layout: page
title: "Study"
date: 2005-10-22 09:37:31 +0200
---
This page contains links to various study related topics, internally and externally. The codes between parentheses are the [course codes](http://owinfo.tue.nl/) used at TU Eindhoven.

# Unpublished papers

## Concepts of Programming Languages

For the course of Concepts of Programming Languages (2M240), we had to write a paper on the delegate as introduced by Microsoft in their version of Java, Visual J++, and in the C# language.

Delegates are the object oriented equivalent of function pointers as they are known from programming languages like C. Their introduction lead to discussions whether they were a good thing or not. Especially Sun, who were advised already in 1996 by Borland <em>not</em> to implement delegates in Java, due to bad experiences with trying the same in Delphi.

The [full paper]({% link /files/papers/concepts of programming languages - C Sharp delegates.pdf %}) can be downloaded here. It was graded with an 8.

## Information Security

Among the assignments for the course of Information Security (2IF30) were two papers; an essay and a case study.

The subject of the essay is "firewalls and intrusion detection". It is in my view a very bad essay, but nonetheless it was graded 7. The [full essay]({% link /files/papers/information security - firewalls and intrusion detection.pdf %}) can be downloaded here.

The subject of the case study is "the Royal Dodge Airlines' <span class="caps">RFID</span> programme". The <span class="caps">RDA</span> wants to implement a customer relationship programme with the use of <span class="caps">RFID</span> chips to identify them. This paper studies the different ways of implementing, along with their (dis)advantages. The [full case study]({% link /files/papers/information security - royal dodge airlines rfid case study.pdf %}) can be downloaded here. It was graded 8.

##  Parallel Programming

A very interesting assignment we got for the course of Parallel Programming (2IN10). Apart from the normal assignment to do some grain-size experiment on a <span class="caps">LAM</span>/MPI cluster, we were to create our own cluster. Very motivated, we jumped on the assignment and within no time we got our own cluster running, with 4 machines at Spacelabs, one at Bas' home.

The [full report]({% link /files/papers/parallel programming - assignment report.pdf %}) can be downloaded here. It was graded 10 for our engineering spirit.

## Immersion and Presence in Virtual Environments

For the course of Interactive Virtual Environments (2IV50) the examination was based upon a literature study concerning the two terms "immersion" and "presence". Two terms with apparantly clear meaning, but with a great discussion around their definitions.

The paper mainly deals with two measures of presence and immersion and summarizes a few more.

The [paper]({% link /files/papers/interactive virtual environments - immersion and presence.pdf %}) can be downloaded here.

## Advanced Algorithms

For the course of <a href="http://www.win.tue.nl/~speckman/2IL40.html" title="2IL40">Advanced Algorithms</a> the examination was partly based upon a literature study concerning the term "Fixed-Parameter Tractability".

The [paper]({% link /files/papers/advanced algorithms - fixed-parameter tractability.pdf %}) can be downloaded here.

## Geometric Algorithms

For the course of <a href="http://www.win.tue.nl/~speckman/2I240.html" title="2IL20">Geometric Algorithms</a> the examination was partly based upon a literature study concerning the term "Construction Sequences".

The [paper]({% link /files/papers/geometric algorithms - construction sequences.pdf %}) can be downloaded here.

# Software

* [Feeding Frenzy](http://ffrenzy.luon.net/) - a game for OGO(Ontwerp Gericht Onderwijs: Design Oriented Education) 2.3 (2R680)
* GOAT - Goal-based Object-oriented Adaptive Teacher for the course of Intelligent Systems (2II40)
* Trains - a model railroad web control system for Software Engineering Project (2IP40)
* Ambient Earth - an ambient semantic web monitor, internship at Reykjavík University (2IM00)
* [Vökvar](https://github.com/islandsvinur/vokvar) - a fluids simulation visualization application for Visualization (2IV40)

