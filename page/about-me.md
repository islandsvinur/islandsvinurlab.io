---
layout: page
title: "Over mezelf"
site_header: "Over mezelf"
date: 2005-08-06 11:41:42 +0200
---

![]({% link image/photos/self-portrait-small.jpg %}){: .left} Mijn naam
is Christian Luijten ([bijgenaamd
Íslandsvinur]({% link page/about-islandsvinur.md %})).  

Ik werd in 1982 op een mooie zomerdag in Breda geboren en groeide daar ook op. 
Al in 1987 werd ik besmet met het computervirus dat me tot de dag van vandaag 
bezighoudt. Als iets programmeerbaar was, wilde ik ermee rommelen.

Begin 2005 raakte ik gefascineerd door
[IJsland]({% category_link ijsland %}) en na veel papierwerk is het me
gelukt om van augustus tot en met november 2006 een [taalcursus en aansluitende
stage](https://ijsland.luijten.org/) te regelen. Nog in de ban van de cultuur
van het noorden schreef ik me begin 2007 in voor een [studiereis naar
Scandinavië]({% link _holidays/kalmar/reisverslag.md %}) met
de [studievereniging GEWIS](https://www.gewis.nl/), die in februari 2008 zou
plaatsvinden. 

Daar, in het hoge noorden van Europa heb ik mijn vriendin leren kennen 
waarmee ik later getrouwd ben en nu [drie kinderen]({% category_link kinderen %}) mee heb. Dit was weer een 
hele nieuwe uitdaging, want kinderen blijken zich niet zo makkelijk te laten
programmeren!

De fascinatie voor koude oorden en in het bijzonder voor ijs laat zich ook op
sportief gebied merken: Sinds 2005 ben ik actief schaatser bij de [Eindhovense
studentenschaatsvereniging Isis](https://www.essvisis.nl/). Van mei 2007 tot mei
2008 was ik secretaris van de vereniging en ook buiten het bestuurswerk was ik
actief in de internetcommissie, de fotocommissie en ook in de redactie van het
verenigingsblad de Crisis. Met het eind van de studie kwam ook een eind aan de 
Isis-tijd. Daarna bleven de ijzers jarenlang in het vet, maar een paar jaar 
geleden begon het weer te kriebelen en rijd ik nu weer mijn rondjes in Grefrath 
bij de [Recreatieve Schaatsvereniging Noord-Limburg (RSNL)](https://www.rsnl.nl/)!

In 2009 ben ik als informatica-ingenieur afgestudeerd aan de faculteit [Wiskunde
en Informatica](https://www.win.tue.nl/) van de [Technische Universiteit
Eindhoven](https://www.tue.nl/). Voor mijn afstudeerproject bij de vakgroepen
Visualizatie en Formele Methoden ontwikkelde ik mee aan de educationele tool
CoffeeDregs die het werken met Java voor beginnende programmeurs moet
vereenvoudigen. Ik begon daarna als Java Programmeur bij internet- en
softwarebureau [ISAAC](https://www.isaac.nl/). Bijna twee jaar later zocht ik 
het groter en ging bij [Océ-Technologies](https://www.oce.com/) aan de slag met 
grote printers. Weer vijf jaar later ging ik terug naar mijn roots op het web 
bij [GX Software](https://gxsoftware.com/), om uiteindelijk te landen in 
Overloon bij [IXON Cloud](https://www.ixon.cloud/).

* [Studiegerelateerde schrijfsels]({% link page/study.md %})

Reisverslagen:

* [2005: Leukerbad, Zwitzerland]({% link _holidays/leukerbad/intro.md %}) journal report (Engels)
* [2006: Reykjavík, IJsland](https://ijsland.luijten.org/) vakantie en stage aan de [Universiteit van Reykjavík](https://www.ru.is/)
* [2008: Trondheim, Stockholm en Helsinki]({% link _holidays/kalmar/reisverslag.md %}) studiereis
