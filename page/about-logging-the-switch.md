---
layout: page
title: "Over dit weblog"
site_header: "Over dit weblog"
date: 2006-01-05 23:41:47 +0100
---
**Logging the Switch** is een weblog van [Christian
Luijten](/page/about-me.html), die in 2004 besloot dat hij zou stoppen met PCs
en _switchen_ naar de Mac. In eerste instantie was het plan om te switchen van
Linux op de PC naar Linux op de Mac. Het werd uiteindelijk een switch naar Mac
OS X naar opperste tevredenheid.

De naam **Logging the Switch** komt van Apple's beroemde _Switch_-campagne (De
originele pagina's staan niet meer online, op de [Way Back
Machine](http://web.archive.org/web/*/http://apple.com/switch/ads) is nog het
een en ander te vinden) waar normale mensen voor de camera vertellen over hun
ervaringen bij de switch naar het Mac platform.

{% youtube "https://www.youtube.com/watch?v=c72d4-LpilM" %}

Intussen is de auteur een beetje van het thema afgezwaaid en schrijft
onregelmatig over van alles en nog wat en soms komt daar de Mac bij kijken.

En natuurlijk geldt voor bloggen net als voor snijden: [altijd van je
áf](http://nl.wikipedia.org/wiki/Kreatief_met_Kurk)! Daarnaast is het
verstandig om het [Helvetica](http://www.helveticafilm.com/) lettertype te
hebben geïnstalleerd op je computer, want dan ziet alles er wat mooier uit
hier.
