---
layout: aframe
title: A-Frame panorama
date: 2023-07-20
---

<a-scene>
    <a-assets>
        <!--img id="schaffhausen" src="{% link photos/panorama/DJI_20220611_105057_502_pano.jpg %}"-->
        <!--img id="fort-saint-elme" src="{% link photos/panorama/DJI_20210821_151605_558_pano.jpg %}"-->
        <img id="pont-du-gard" src="{% link photos/panorama/DJI_20230815_121300_609_pano.jpg %}">
        <img id="city" src="https://cdn.aframe.io/a-painter/images/sky.jpg">
        <img id="floor" src="https://cdn.aframe.io/a-painter/images/floor.jpg">
    </a-assets>
    
    <a-sky src="#city"></a-sky>
    <a-curvedimage src="#pont-du-gard" 
                position="0 1.5 0" height="2" radius="2" 
                theta-start="60" theta-length="240"></a-curvedimage>

    <a-cylinder id="ground" src="#floor" radius="32" height="0.1"></a-cylinder>

    <a-entity laser-controls="hand: right"></a-entity>

</a-scene>
