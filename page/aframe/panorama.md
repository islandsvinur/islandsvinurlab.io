---
layout: aframe
title: A-Frame panorama
date: 2023-07-20
---

<script>
const imageCount = 8;
let curr = 0;

function nextImage(step = 1) {
    curr = (imageCount + curr + step) % imageCount;

    const prev = document.querySelector('#prev-image');
    prev.setAttribute('material', 'src', document.querySelector(`#image-${(imageCount + curr - 1) % imageCount}`));
    const panorama = document.querySelector('#panorama');
    panorama.setAttribute('material', 'src', document.querySelector(`#image-${curr % imageCount}`));
    const next = document.querySelector('#next-image');
    next.setAttribute('material', 'src', document.querySelector(`#image-${(curr + 1) % imageCount}`));
    const label = document.querySelector('#image-label');
    label.setAttribute('value', `${(curr % imageCount) + 1}`);
}

AFRAME.registerComponent('cursor-listener', {
    schema: {
        direction: {type: 'string', default: 'next'}
    },

    init: function() {
        this.el.addEventListener('click', (e) => {
            switch (this.data.direction) {
                case 'prev':
                    nextImage(-1); break;
                default:
                    nextImage(1); break;
            }
        });
    }
});

AFRAME.registerComponent('b-button-listener', {
    init: function() {
        const el = this.el;
        el.addEventListener('bbuttondown', function(e) {
            const sceneEl = document.querySelector('a-scene');
            if (sceneEl.is('vr-mode')) {
                sceneEl.exitVR();
            }
        });
    }
});
</script>

<a-scene>
    <a-assets>
        <img id="image-0" src="{% link photos/panorama/DJI_20230815_121300_609_pano.jpg %}">
        <img id="image-1" src="{% link photos/panorama/DJI_20210821_151605_558_pano.jpg %}">
        <img id="image-2" src="{% link photos/panorama/DJI_20220611_105057_502_pano.jpg %}">
        <img id="image-3" src="{% link photos/panorama/DJI_20210821_145939_386_pano.jpg %}">
        <img id="image-4" src="{% link photos/panorama/DJI_20210821_150510_548_pano.jpg %}">
        <img id="image-5" src="{% link photos/panorama/IMG_2698.jpg %}">
        <img id="image-6" src="{% link photos/panorama/IMG_8041.jpeg %}">
        <img id="image-7" src="{% link photos/panorama/IMG_8046.jpeg %}">

        <img id="city" src="https://cdn.aframe.io/a-painter/images/sky.jpg">
        <img id="floor" src="https://cdn.aframe.io/a-painter/images/floor.jpg">
    </a-assets>

    <a-sky src="#city"></a-sky>
    <a-curvedimage id="panorama" src="#image-0" 
                position="0 2 0" height="2.5" radius="2" 
                theta-start="60" theta-length="240"></a-curvedimage>

    <a-image id="prev-image" src="#image-2" 
                animation__mouseenter="property: scale; startEvents: mouseenter; easing: easeInCubic; dur: 500; from: 1 1 1; to: 1.1 1.1 1.1"
                animation__mouseleave="property: scale; startEvents: mouseleave; easing: easeInCubic; dur: 500; to: 1 1 1"
                position="-1 0.5 -1.5" rotation="-45 30 0"
                cursor-listener="direction: prev"
                width="1" height="0.33"></a-image>
    <a-text id="image-label" value="1" align="center"
                position="0 0.5 -2" rotation="-45 0 0"></a-text>
    <a-image id="next-image" src="#image-1" 
                animation__mouseenter="property: scale; startEvents: mouseenter; easing: easeInCubic; dur: 500; from: 1 1 1; to: 1.1 1.1 1.1"
                animation__mouseleave="property: scale; startEvents: mouseleave; easing: easeInCubic; dur: 500; to: 1 1 1"
                position="1 0.5 -1.5" rotation="-45 -30 0" 
                cursor-listener="direction: next"
                width="1" height="0.33"></a-image>

    <a-cylinder id="ground" src="#floor" radius="32" height="0.1"></a-cylinder>

    <a-entity oculus-touch-controls="hand: right" 
                laser-controls b-button-listener></a-entity>

</a-scene>
