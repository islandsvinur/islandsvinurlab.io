---
layout: page
title: "Who is @islandsvinur@mastodon.nl?"
---
Hey, hi! You've landed on my Mastodon introduction page! Thanks for clicking the link
in [Mastodon](https://mastodon.nl/@islandsvinur){: rel="me"}.

On this page I'll introduce you to my website, which is mostly in Dutch, so that's why I'll switch languages now.

Je zou je kunnen afvragen [wie ik ben]({% link page/about-me.md %}), daarover is nog lang niet alles geschreven, maar
wat er wel is, is een mooie pagina waar een klein beetje wordt toegelicht.

Dit weblog heet "Logging the Switch" met een reden. In 2004 deed ik de switch van PC naar Mac. In eerste instantie
bedoeld als een switch van Linux naar Linux, uiteindelijk van Linux naar Mac OS X. Wat meer informatie vind je op
de [pagina over dit weblog]({% link page/about-logging-the-switch.md %}).

Aangezien je via Mastodon op deze pagina kwam, heb je al een manier om mij te bereiken ontdekt. Andere manieren vind je
op [de contact-pagina]({% link page/contact.md %}).

Mijn Mastodon-naam is íslandsvinur, dit is IJslands en staat voor "vriend van IJsland". Ik ben er in 2006 geweest voor
een stage aan de Universiteit van Reykjavík en ook daar [heb ik een weblog bijgehouden](http://ijsland.luijten.org/.)
Het was een mooie tijd waar ik met veel plezier aan terugdenk...
