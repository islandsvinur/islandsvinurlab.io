
Jekyll::Hooks.register :site, :pre_render do |site|
  site.pages.each do |page|
    if page.instance_of? Jekyll::ArchivePage
      page.data['subtitle'] = 'Blogpost gemist?'
      page.data['lead_image'] = '/image/unsplash/c-m-X_j3b4rqnlk-unsplash.jpg'
      page.data['lead_image_credit'] = {
        'unsplash' => {
          'username' => 'ubahnverleih',
          'name' => 'C M',
        },
      }
    end
  end
end
