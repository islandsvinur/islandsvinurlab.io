
module Jekyll
  module FilesizeFilter
    def filesize(input)
      site = @context.registers[:site]
      file = site.static_files.find {|f| f.path.end_with?(input) }

      File.size(file.path)
    end
  end
end

Liquid::Template.register_filter(Jekyll::FilesizeFilter)
