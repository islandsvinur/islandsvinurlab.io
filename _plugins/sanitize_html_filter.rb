require 'sanitize'

module Jekyll
  module SanitizeHtmlFilter
    def sanitize_html(input)
      Sanitize.clean(input, Sanitize::Config::BASIC)
    end
  end
end

Liquid::Template.register_filter(Jekyll::SanitizeHtmlFilter)
